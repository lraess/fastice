function  A  = read_data(var_name,run_id,it,nx,ny,PRECIS)

    fid  = fopen([int2str(run_id) '.' int2str(it) '.' var_name],'rb');
    A1D  = fread(fid, PRECIS, 'n');  
    fclose(fid);

    for i=1:nx
        for j=1:ny
                ijk=i +  (j-1)*nx;
                A(i,j)=A1D(ijk);
        end
    end

end

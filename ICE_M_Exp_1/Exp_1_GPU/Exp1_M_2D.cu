/* FastICE: Thermomechanical ice deformation models 2D GPU

Copyright (C) 2019  Ludovic Raess, Aleksandar Licul et al.
 
FastICE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
FastICE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with FastICE. If not, see <http://www.gnu.org/licenses/>. */

// -------------------------------------------------------------------------
// Compile as: nvcc Exp1_M_2D.cu -arch=sm_XX --ptxas-options=-v
// arch=sm_XX: TITAN Black=sm_35, TITAN X=sm_52, TITAN Xp=sm_61, Tesla V100=sm_70
// Run as: ./a.out
// -------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define GPU_ID 0
#define USE_SINGLE_PRECISION   

#ifdef USE_SINGLE_PRECISION
#define DAT     float
#define PRECIS  4
const DAT eps_stop  = 2e-3;
#else
#define DAT     double
#define PRECIS  8
const DAT eps_stop  = 1e-8;
#endif

#define BLOCK_X            32
#define BLOCK_Y            32
#define NB_THREADS         (BLOCK_X*BLOCK_Y)
#define GRID_X             64
#define GRID_Y             16
#define NB_BLOCKS          (GRID_X*GRID_Y)
#define NB_MPS_OF_GPU      24
#define MAX_X_OVERLAP      1
#define MAX_Y_OVERLAP      1

////// global variables //////   
#define PI 3.141592653589793
// (numerics)
const int nx    = GRID_X*BLOCK_X - MAX_X_OVERLAP; 
const int ny    = GRID_Y*BLOCK_Y - MAX_Y_OVERLAP;        

#include "macros_2D.h"
						
__global__ void initialize(DAT* xc,DAT* xn,DAT* yc,DAT* yn,const DAT dx, const DAT dy,const int nx, const int ny,const DAT Lx){  
  def_sizes(xc    , nx   , 1    );
  def_sizes(xn    , nx+1 , 1    );
  def_sizes(yc    , 1    , ny   );
  def_sizes(yn    , 1    , ny+1 );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    
  if (participate_a(xc))    all(xc)    =  (DAT) ix*dx;
  if (participate_a(xn))    all(xn)    = - dx/(DAT)2.0 + ((DAT)ix)*dx;
  if (participate_a(yc))    all(yc)    =  (DAT) iy*dy;
  if (participate_a(yn))    all(yn)    = - dy/(DAT)2.0 + ((DAT)iy)*dy;
} 

__global__ void timesteps(DAT* etan,DAT* dtVx,DAT* dtVy,DAT* dtP,const DAT relV,const DAT relP,const DAT eta_b,const DAT nxy,const DAT dxy2,const int nx, const int ny){
  def_sizes(etan  , nx   , ny   );
  def_sizes(dtVx  , nx-1 , ny-2 );
  def_sizes(dtVy  , nx-2 , ny-1 );
  def_sizes(dtP   , nx   , ny   );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
  int ixi = ix+1; 
  int iyi = iy+1;

  if (participate_a(dtP))    all(dtP)   = relP*(DAT)4.1/nxy*all(etan)*((DAT)1.0+eta_b);
  if (participate_a(dtVx))   all(dtVx)  = relV*dxy2/av_xi(etan)/((DAT)1.0+eta_b)/(DAT)4.1;
  if (participate_a(dtVy))   all(dtVy)  = relV*dxy2/av_yi(etan)/((DAT)1.0+eta_b)/(DAT)4.1;
}

__global__ void strain_rates_P(DAT* P,DAT* Vx,DAT* Vy,DAT* divV,DAT* Exx,DAT* Eyy,DAT* Exy,DAT* dtP, const DAT DX, const DAT DY,const int nx, const int ny){
  def_sizes(P     , nx   , ny   );
  def_sizes(Vx    , nx+1 , ny   );
  def_sizes(Vy    , nx   , ny+1 );
  def_sizes(divV  , nx   , ny   );
  def_sizes(Exx   , nx   , ny   );
  def_sizes(Eyy   , nx   , ny   );
  def_sizes(Exy   , nx-1 , ny-1 );
  def_sizes(dtP   , nx   , ny   );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
  int ixi = ix+1; 
  int iyi = iy+1;

  if (participate_a(divV))  all(divV) = DX*d_xa(Vx) + DY*d_ya(Vy);
  if (participate_a(Exx))   all(Exx)  = DX*d_xa(Vx) - (DAT)0.5*( DX*d_xa(Vx) + DY*d_ya(Vy) );
  if (participate_a(Eyy))   all(Eyy)  = DY*d_ya(Vy) - (DAT)0.5*( DX*d_xa(Vx) + DY*d_ya(Vy) );
  if (participate_a(Exy))   all(Exy)  = (DAT)0.5*( DY*d_yi(Vx) + DX*d_xi(Vy) );

  if (participate_a(P))     all(P)    = all(P)  -  all(divV)*all(dtP); 
}

__global__ void stresses(DAT* etan,DAT* etas,DAT* divV,DAT* Exx,DAT* Eyy,DAT* Exy,DAT* txx,DAT* tyy,DAT* txy,DAT* Exyn, const DAT eta_b,const int nx, const int ny){
  def_sizes(etan  , nx   , ny   );
  def_sizes(etas  , nx-1 , ny-1 );
  def_sizes(divV  , nx   , ny   );
  def_sizes(Exx   , nx   , ny   );
  def_sizes(Eyy   , nx   , ny   );
  def_sizes(Exy   , nx-1 , ny-1 );
  def_sizes(txx   , nx   , ny   );
  def_sizes(tyy   , nx   , ny   );
  def_sizes(txy   , nx-1 , ny-1 );
  def_sizes(Exyn  , nx   , ny   );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
  int ixi = ix+1; 
  int iyi = iy+1;

  if (participate_a(txx))   all(txx)  = (DAT)2.0*all(etan)*(all(Exx) + eta_b*all(divV));
  if (participate_a(tyy))   all(tyy)  = (DAT)2.0*all(etan)*(all(Eyy) + eta_b*all(divV));
  if (participate_a(txy))   all(txy)  = (DAT)2.0*av_xya(etan)*all(Exy);

  if (participate_a(etas))  all(etas) = av_xya(etan);
  if (participate_i(Exyn))  inn(Exyn) = av_xya(Exy);
}

// BC for Exyn

__global__ void ResV_etait(DAT* P,DAT* dVxdt,DAT* dVydt,DAT* Exx,DAT* Eyy,DAT* Exyn,DAT* txx,DAT* tyy,DAT* txy,DAT* EII2,DAT* etait,const DAT dampX,const DAT dampY,const DAT alph,const DAT mpow,const DAT DX,const DAT DY,const int nx, const int ny){
  def_sizes(P     , nx   , ny   );
  def_sizes(dVxdt , nx-1 , ny-2 );
  def_sizes(dVydt , nx-2 , ny-1 );
  def_sizes(Exx   , nx   , ny   );
  def_sizes(Eyy   , nx   , ny   );
  def_sizes(Exyn  , nx   , ny   );
  def_sizes(txx   , nx   , ny   );
  def_sizes(tyy   , nx   , ny   );
  def_sizes(txy   , nx-1 , ny-1 );
  def_sizes(EII2  , nx   , ny   );
  def_sizes(etait , nx   , ny   );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
  int ixi = ix+1; 
  int iyi = iy+1;

  if (participate_a(dVxdt))  all(dVxdt) = all(dVxdt)*dampX - DX*d_xi(P) + DX*d_xi(txx) + DY*d_ya(txy) + (DAT)1.0; 
  if (participate_a(dVydt))  all(dVydt) = all(dVydt)*dampY - DY*d_yi(P) + DX*d_xa(txy) + DY*d_yi(tyy) - cos(alph)/sin(alph);

  if (participate_a(EII2))   all(EII2)  = (DAT)0.5*(all(Exx)*all(Exx) + all(Eyy)*all(Eyy)) + all(Exyn)*all(Exyn);
  if (participate_a(etait))  all(etait) = pow(all(EII2),mpow);
}

__global__ void update_V_eta(DAT* Vx,DAT* Vy,DAT* dVxdt,DAT* dVydt,DAT* dtVx,DAT* dtVy,DAT* etait,DAT* etan,const DAT eta0,const DAT rele, const int nx, const int ny){
  def_sizes(Vx    , nx+1 , ny   );
  def_sizes(Vy    , nx   , ny+1 );
  def_sizes(dVxdt , nx-1 , ny-2 );
  def_sizes(dVydt , nx-2 , ny-1 );
  def_sizes(dtVx  , nx-1 , ny-2 );
  def_sizes(dtVy  , nx-2 , ny-1 );
  def_sizes(etait , nx   , ny   );
  def_sizes(etan  , nx   , ny   );  

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
  int ixi = ix+1; 
  int iyi = iy+1;

  if (participate_i(Vx))  inn(Vx) = inn(Vx) +  all(dVxdt)*all(dtVx); 
  if (participate_i(Vy))  inn(Vy) = inn(Vy) +  all(dVydt)*all(dtVy);
  
  if (participate_a(etan))  all(etan) = min(exp(rele*log(all(etait)) + ((DAT)1.0 - rele)*log(all(etan)) ),eta0); 

}

__global__ void BC_Exyn(DAT* Exyn, const int nx, const int ny){
  def_sizes(Exyn , nx   , ny   );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;

  if (iy==0 && ix<size(Exyn,1))  Exyn[ix] =  Exyn[ix + size(Exyn,1)];   
}

__global__ void BC_Visc_1(DAT* etan,const int nx, const int ny){
  def_sizes(etan , nx   , ny );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    
  if (iy==0 && ix<size(etan,1))                 etan[ix] = etan[ix + size(etan,1)];
  if (iy==(size(etan,2)-1) && ix<size(etan,1))  etan[ix + (size(etan,2)-1)*size(etan,1)] = etan[ix + (size(etan,2)-2)*size(etan,1)];
}

__global__ void BC_Visc_2(DAT* etan,const int nx, const int ny){
  def_sizes(etan , nx   , ny );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;

  if (ix==0 && iy< size(etan,2))                etan[iy*size(etan,1)] = etan[1 + iy*size(etan,1)];
  if (ix==(size(etan,1)-1) && iy< size(etan,2)) etan[size(etan,1)-1 + iy*size(etan,1)] = etan[size(etan,1)-2 + iy*size(etan,1)];
}

__global__ void BC_V_1(DAT* Vx,DAT* Vy,const int nx, const int ny){
  def_sizes(Vx , nx+1 , ny   );
  def_sizes(Vy , nx   , ny+1 );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
  // BC left
  if (ix==0 && iy>0 && iy<(size(Vx,2)-1))  Vx[iy*size(Vx,1)] = - Vx[1 + iy*size(Vx,1)];
  if (ix==0 && iy>0 && iy<(size(Vy,2)-1))  Vy[iy*size(Vy,1)] =   Vy[1 + iy*size(Vy,1)];
  // BC right
  if (ix==(size(Vx,1)-1) && iy>0 && iy<(size(Vx,2)-1)) Vx[size(Vx,1)-1 + iy*size(Vx,1)] =  - Vx[size(Vx,1)-2 + iy*size(Vx,1)];
  if (ix==(size(Vy,1)-1) && iy>0 && iy<(size(Vy,2)-1)) Vy[size(Vy,1)-1 + iy*size(Vy,1)] =    Vy[size(Vy,1)-2 + iy*size(Vy,1)];
}

 __global__ void BC_V_2(DAT* Vx,DAT* Vy,DAT* P,DAT* divV,DAT* txy,DAT* etan,DAT* etas,const DAT p_dy2,const DAT p_dy3,const DAT p_dydx,const DAT p_dyeta,const DAT dx,const DAT dy,const int nx, const int ny){
  def_sizes(Vx    , nx+1 , ny   );
  def_sizes(Vy    , nx   , ny+1 );
  def_sizes(P     , nx   , ny   );
  def_sizes(etan  , nx   , ny   );
  def_sizes(etas  , nx-1 , ny-1 );
  def_sizes(txy   , nx-1 , ny-1 );
  def_sizes(divV     , nx   , ny   );

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
  int iy  = blockIdx.y*blockDim.y + threadIdx.y;
  // BC bottom
  if (iy==0 && ix<size(Vx,1))  Vx[ix] =    (DAT) 0.0;
  if (iy==0 && ix<size(Vy,1))  Vy[ix] =  - Vy[ix + size(Vy,1)];
  // BC top
  if (iy==(size(Vy,2)-1) && ix<size(Vy,1) )             Vy[ix + iy*size(Vy,1)] = Vy[ix + (iy-1)*size(Vy,1)] + p_dy2*(P[ix + (size(P,2)-1)*size(P,1)]/etan[ix + (size(etan,2)-1)*size(etan,1)]) + p_dyeta*divV[ix + (size(divV,2)-1)*size(divV,1)];
  if (iy==(size(Vx,2)-1) && ix>0 && ix<(size(Vx,1)-1) ) Vx[ix + iy*size(Vx,1)] = Vx[ix + (iy-1)*size(Vx,1)] - p_dydx*(Vy[ix + (size(Vy,2)-2)*size(Vy,1)]-Vy[ix-1 + (size(Vy,2)-2)*size(Vy,1)]) + p_dy3*txy[ix-1 + (size(txy,2)-2)*size(txy,1)]/etas[ix-1 + (size(etas,2)-1)*size(etas,1)] ;
}

int main(){	
  int run_id,iter;
  size_t i;
  
  double mem = (double)1e-9*(double)nx*(double)ny*sizeof(DAT);
  DAT nIO    = 8.0;
  run_id     = 10;

  DAT  alph_deg = 10; 
  DAT  Ly       = 1.0;
  DAT  npow     = 3.0;   
  DAT  aspect   = 10.0;
  // preproc     
  DAT Lx    = Ly*aspect;
  DAT alph  = alph_deg*PI/(DAT)180.0;
  DAT dx    = Lx/(nx-1);
  DAT dy    = Ly/(ny-1);
  DAT DX    = 1.0/dx;
  DAT DY    = 1.0/dy;
  DAT mpow  = -(1.0-1.0/npow)/2.0;
  DAT dxy2  =  pow(min(dx,dy),2);
  DAT nxy   =  max(nx,ny);

  DAT iterror     = (DAT)0.0;
  DAT device_NORM = (DAT)0.0;
  DAT err_Vx_NORM = (DAT)0.0;
  DAT err_Vy_NORM = (DAT)0.0;
  DAT err_P_NORM  = (DAT)0.0;

  // numerics
  DAT eta0  = 1e3;
  DAT damp  = 2.0;
  DAT relV  = 1.0/1;  
  DAT relP  = 1.0/4.0; 
  DAT eta_b = 1.0/4.0;
  DAT rele  = 1e-2;
  int niter = 5e6;
  DAT dampX = ((DAT)1.0 -damp/((DAT)nx));
  DAT dampY = ((DAT)1.0 -damp/((DAT)ny));
  DAT p_dy2   = dy/(DAT)2.0;
  DAT p_dydx  = dy/dx;
  DAT p_dy3   = dy/(DAT)3.0;
  DAT p_dyeta = dy*((DAT)1.0/(DAT)2.0 - eta_b);

  set_up_gpu();  
  printf("Local size: %dx%d, %d iterations ...\n", nx, ny, niter);
  printf("Launching (%dx%d) grid of (%dx%d) blocks.\n\n", grid.x, grid.y, block.x, block.y);

  zeros(xc    , nx   , 1    );
  zeros(yc    , 1    , ny   );
  zeros(xn    , nx+1 , 1    );
  zeros(yn    , 1    , ny+1 );
  zeros(P     , nx   , ny   );
  zeros(Exyn  , nx   , ny   );
  zeros(etas  , nx-1 , ny-1 ); 
  zeros(etait , nx   , ny   );
  zeros(dVxdt , nx-1 , ny-2 );
  zeros(dVydt , nx-2 , ny-1 );
  zeros(Vx    , nx+1 , ny   );
  zeros(Vy    , nx   , ny+1 );
  zeros(dtP   , nx   , ny   );
  zeros(dtVx  , nx-1 , ny-2 );
  zeros(dtVy  , nx-2 , ny-1 ); 
  zeros(divV  , nx   , ny   );
  zeros(EII2  , nx   , ny   );
  zeros(Exx   , nx   , ny   );
  zeros(Eyy   , nx   , ny   );
  zeros(Exy   , nx-1 , ny-1 );
  zeros(txx   , nx   , ny   );
  zeros(tyy   , nx   , ny   );
  zeros(txy   , nx-1 , ny-1 );
  ones(etan   , nx   , ny   );
  zeros(__device_normval,grid.x,grid.y);

  // action
  initialize <<<grid,block>>>(xc_d,xn_d,yc_d,yn_d,dx,dy,nx,ny,Lx); cudaDeviceSynchronize();
  SaveArray(xc,"xc"); SaveArray(xn,"xn");
  // action
  for (iter=0; iter<niter; iter++){
    if (iter==3){ tic(); }// start time counter
    timesteps     <<<grid,block>>>(etan_d, dtVx_d, dtVy_d, dtP_d, relV, relP, eta_b, nxy, dxy2, nx, ny); cudaDeviceSynchronize();
    strain_rates_P<<<grid,block>>>(P_d, Vx_d, Vy_d, divV_d, Exx_d, Eyy_d, Exy_d, dtP_d, DX, DY, nx, ny); cudaDeviceSynchronize();
    stresses      <<<grid,block>>>(etan_d, etas_d, divV_d, Exx_d, Eyy_d, Exy_d, txx_d, tyy_d, txy_d, Exyn_d, eta_b, nx, ny); cudaDeviceSynchronize();
    
    BC_Exyn       <<<grid,block>>>(Exyn_d, nx, ny); cudaDeviceSynchronize(); // BC

    ResV_etait    <<<grid,block>>>(P_d, dVxdt_d, dVydt_d,Exx_d, Eyy_d, Exyn_d, txx_d, tyy_d, txy_d, EII2_d, etait_d, dampX, dampY, alph, mpow, DX, DY, nx, ny); cudaDeviceSynchronize();
    update_V_eta  <<<grid,block>>>(Vx_d, Vy_d, dVxdt_d, dVydt_d, dtVx_d, dtVy_d, etait_d, etan_d, eta0, rele, nx, ny); cudaDeviceSynchronize();
     
    BC_Visc_1     <<<grid,block>>>(etan_d, nx, ny);     cudaDeviceSynchronize(); // BC
    BC_Visc_2     <<<grid,block>>>(etan_d, nx, ny);     cudaDeviceSynchronize(); // BC
    BC_V_1        <<<grid,block>>>(Vx_d, Vy_d, nx, ny); cudaDeviceSynchronize(); // BC
    BC_V_2        <<<grid,block>>>(Vx_d, Vy_d, P_d, divV_d, txy_d, etan_d, etas_d,p_dy2, p_dy3, p_dydx, p_dyeta, dx, dy, nx, ny); cudaDeviceSynchronize(); // BC
    if (iter%100==1 && iter>2){
      __MPI_norm(dVxdt);   err_Vx_NORM = device_NORM;
      __MPI_norm(dVydt);   err_Vy_NORM = device_NORM;
      __MPI_norm(divV);    err_P_NORM  = device_NORM;
      iterror = max(err_Vx_NORM,max(err_Vy_NORM,err_P_NORM));
      if (iterror<eps_stop && iter>1) {break;}
    }
    if (iter%5000==0){ printf(">> iter=%4.d , err=%1.3e \n", iter, iterror);}
  }//end iter
  tim("> Performance", mem*(iter-3)*nIO); // timer test
  printf("> Tot iters=%d \n",iter);

  SaveArray(Vx,"Vx"); 
  SaveArray(Vy,"Vy"); 
  SaveArray(P,"P"); 
  SaveArray(txx,"txx"); 
  SaveArray(tyy,"tyy"); 
  SaveArray(txy,"txy"); 

  // clear host memory & clear device memory
  free_all(xc);
  free_all(yc);
  free_all(xn);
  free_all(yn);
  free_all(P);
  free_all(Exyn);
  free_all(etas);
  free_all(etait);
  free_all(dVxdt);
  free_all(dVydt);
  free_all(Vx);
  free_all(Vy);
  free_all(dtP);
  free_all(dtVx);
  free_all(dtVy);
  free_all(divV);
  free_all(EII2);
  free_all(Exx);
  free_all(Eyy);
  free_all(Exy);
  free_all(txx);
  free_all(tyy);
  free_all(txy);
  free_all(etan);
  free_all(__device_normval);
  
  clean_cuda();
  return 0;
}

function [] = vizme_3D(v_sc,nx,ny,nz,iter_plot_count,iterror_M,Vx,Vy,Vz,P,txx,tyy,tzz,txy,txz,tyz,EII2,etan,divV)
     figure(1)
     clf,colormap jet   
     subplot(321),semilogy(iter_plot_count,iterror_M,'k'),axis tight,title('errors vs iterations')
     xlabel(max(v_sc*Vx(:)))
     subplot(322),imagesc(flipud(squeeze(P(:,round(ny/4),:))')),colorbar,title('p')
     subplot(323),imagesc(flipud(log10(squeeze(etan(:,round(ny/4),:)))')),colorbar,title('etan')
     subplot(324),imagesc(flipud(squeeze(Vx(:,round(ny/4),:))')),colorbar,title('u')
     subplot(325),imagesc(flipud(squeeze(Vy(:,round(ny/4),:))')),colorbar,title('v')
     subplot(326),imagesc(flipud(squeeze(Vz(:,round(ny/4),:))')),colorbar,title('w')
     drawnow
     figure(2)
     clf,colormap jet   
     subplot(331),imagesc(flipud(squeeze(txx(:,round(ny/4),:))')),colorbar,title('txx')
     subplot(332),imagesc(flipud(squeeze(tyy(:,round(ny/4),:))')),colorbar,title('tyy')
     subplot(333),imagesc(flipud(squeeze(tzz(:,round(ny/4),:))')),colorbar,title('tzz')
     subplot(334),imagesc(flipud(squeeze(txy(:,round(ny/4),:))')),colorbar,title('txy')
     subplot(335),imagesc(flipud(squeeze(txz(:,round(ny/4),:))')),colorbar,title('txz')
     subplot(336),imagesc(flipud(squeeze(tyz(:,round(ny/4),:))')),colorbar,title('tyz')
     %subplot(337),imagesc(flipud(squeeze(divV(:,round(ny/4),:)'))),colorbar,title('divV')
     %subplot(338),imagesc(flipud(log10(squeeze(EII2(:,round(ny/4),:)))')),colorbar,title('EII2')
     subplot(337),imagesc(flipud(squeeze(Vx(:,:,end))')),colorbar,title('Vx top')
     subplot(338),imagesc(flipud(squeeze(Vy(:,:,end))')),colorbar,title('Vy top')
     subplot(339),imagesc(flipud(squeeze(Vz(:,:,end))')),colorbar,title('Vz top')
     drawnow
end


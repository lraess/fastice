# FastICE

## Thermomechanical ice deformation models

Copyright (C) 2020  Ludovic Raess, Aleksandar Licul, Frederic Herman, Yury Y. Podladchikov and Jenny Suckale.

FastICE is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FastICE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FastICE. If not, see <http://www.gnu.org/licenses/>.

### Current version of FastICE can be found at:

https://bitbucket.org/lraess/fastice/

http://www.unil.ch/geocomputing/software/

### FastICE was released in the Journal of Geoscientific Model Development:

Reference:
```tex
@Article{raess_fastice_gmd_2020,
	AUTHOR = {R\"ass, L. and Licul, A. and Herman, F. and Podladchikov, Y. Y. and Suckale, J.},
	TITLE = {Modelling thermomechanical ice deformation using an implicit pseudo-transient method (FastICE v1.0) based on graphical processing units (GPUs)},
	JOURNAL = {Geoscientific Model Development},
	VOLUME = {13},
	YEAR = {2020},
	NUMBER = {3},
	PAGES = {955--976},
	URL = {https://www.geosci-model-dev.net/13/955/2020/},
	DOI = {10.5194/gmd-13-955-2020}
}
```

### Distributed software, directory content:

ICE_M_Exp_1	| Experiment 1 MATLAB and CUDA C codes to reproduce the results from the manuscript

ICE_M_Exp_2	| Experiment 2 MATLAB and CUDA C codes to reproduce the results from the manuscript

ICE_TM_2D_GPU | 2D Thermomechanic ice flow CUDA C code for single GPU (analoguous to Exp. 3 - with/without melt)

ICE_TM_2D_MATLAB | 2D Thermomechanic ice flow MATLAB code (analoguous to Exp. 3 - with/without melt)	

ICE_TM_3D_GPU_MPI | 3D Thermomechanic ice flow CUDA C MPI (CUDA-aware MPI) code (analoguous to Exp. 3 - with/without melt)	

### QUICK START:

MATLAB > select the routine and enjoy!

CUDA > compile the ICE_GPU_XX.cu routine on a system hosting an
Nvidia GPU using the compilation line displayed on the top line
of the .cu file. Run it (./a.out) and use the MATLAB visualisation
script to plot the output. 

CUDA MPI > follow the instruction in the folder or modify the supplied bash file

### Contact: ludovic.rass@gmail.com

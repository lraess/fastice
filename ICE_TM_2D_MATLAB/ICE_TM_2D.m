% FastICE: Thermomechanical ice deformation models 2D
%
% Copyright (C) 2019  Ludovic Raess, Aleksandar Licul et al.
%
% FastICE is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% FastICE is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with FastICE. If not, see <http://www.gnu.org/licenses/>.
clear
% Physics 
s2y    =  3600*24*365.25;
T_K    =  273;
R      =  8.314;
g      =  9.8; 
rho    =  900; 
kappa  =  2.51;
cp     =  2096.9;
Q0     =  6*1e4;
a0     =  8.75*1e-13;
eta0   =  1.5e7; % cut-off viscosity
% Input values
Lz     = 300;
alpha  = 10;
T0     = -10;
q0     = 0;
npow   = 3;
MELT   = 1.0;  % Melting if MELT = 1.0, none if =0
Tm     = -0.2; % defined metling temperature °C
% scales
T0     = T0 + T_K;
T_sc   = npow*R*T0^2/Q0;
tau_sc = rho*cp*T_sc;
t_sc   = 2^(-npow)*a0^(-1)*tau_sc^(-npow)*exp(Q0/R/T0);
L_sc   = sqrt(kappa/rho/cp)*sqrt(t_sc);
% non-dimensional values
Lz     = Lz/L_sc;
F      = rho*g*L_sc*sind(alpha)/tau_sc;
theta  = T0/T_sc;
q      = q0/kappa*L_sc/T_sc;
t_t    = 10*300*1e6;
dt     = 5e6;
aspect = 10;
% Numerics
nx     = 255;
nz     = 63;
epsi   = 1e-15;  
CN     = 0.5;
damp   = 2; 
relV   = 1/1;
relP   = 1/2;
eta_b  = 1/2;
rele   = 1e-1;
niter  = 5e6;
nouti  = 1000;
nout   = 10;   % nout for physical timesteps
% Pre-processing
Lx     = aspect*Lz;
mpow   = -(1-1/npow)/2;
dx     = Lx/(nx-1); 
dz     = Lz/(nz-1);
xn     = -dx/2:dx:Lx+dx/2; 
zn     = -dz/2:dz:Lz+dz/2;
xc     = 0:dx:Lx;
zc     = 0:dz:Lz;
[xc2,zc2] = ndgrid(xc,zc);
etan   = 1e4*ones(nx,nz);
T      = q*(max(zc)-zc2)*0; 
Vx     = zeros(nx+1,nz  );
Vz     = zeros(nx  ,nz+1);
P      = zeros(nx  ,nz  );
Exzn   = zeros(nx  ,nz  );
etas   = zeros(nx-1,nz-1);
dVxdt  = zeros(nx-1,nz-2);
dVzdt  = zeros(nx-2,nz-1); 
dTdt   = zeros(nx  ,nz  ); 
Hs     = zeros(nx  ,nz  );
qxT    = zeros(nx+1,nz  );
qzT    = zeros(nx  ,nz+1);
T_init = T;
q_arr  = zeros(nx  ,nz-1);
q_arr(1:fix(nx/2),1) = 0; q_arr(fix(nx/2):end,1) = q;
nt     = round(t_t/dt)+1;
time   = 0;
Pt = linspace(cotd(alpha),0,nz); Pt = repmat(Pt,nx,1);
% load restart2
% Action
for it=1:nt
   T_o    = T;
   dTdt_o = dTdt;
    tic
    for iter = 1:niter % Pseudo-Transient cycles
        % Timesteps - GPU KERNEL 1
        etax     =  0.5*(etan(2:end  ,2:end-1) + etan(1:end-1,2:end-1));
        etaz     =  0.5*(etan(2:end-1,2:end  ) + etan(2:end-1,1:end-1));
        dtP      = relP*4.1/max(nx,nz)*etan*(1+eta_b);
        dtVx     = relV*min(dx,dz)^2./etax/(1+eta_b)/4.1;
        dtVz     = relV*min(dx,dz)^2./etaz/(1+eta_b)/4.1;
        dtau     = 1/(4.1/min(dx,dz)^2 + 1/dt);
        % Pressure update and strain rates - GPU KERNEL 2
        divV     = diff(Vx,1,1)/dx + diff(Vz,1,2)/dz;
        P        = P - divV.*dtP;
        Exx      = diff(Vx,1,1)/dx - 1/2*divV;
        Ezz      = diff(Vz,1,2)/dz - 1/2*divV;
        Exz      = 0.5*(diff(Vx(2:end-1,:),1,2)/dz+diff(Vz(:,2:end-1),1,1)/dx);                  
        % Stresses - GPU KERNEL 3
        txx      =  2*etan.*(Exx + eta_b*divV);
        tzz      =  2*etan.*(Ezz + eta_b*divV);
        txz      =  2*etas.*Exz;  
        etas     = 0.25*(etan(1:end-1,1:end-1)+etan(2:end  ,1:end-1)+etan(1:end-1,2:end  ) + etan(2:end  ,2:end  ));
        Exzn(2:end-1,2:end-1) = 0.25*(Exz(1:end-1,1:end-1)+Exz(2:end,1:end-1)+Exz(1:end-1,2:end  )+Exz(2:end,2:end  ));
        % GPU KERNEL 4
        Exzn(:,1) = Exzn(:,2);
        % GPU KERNEL 5
        ResVx    = diff(txz,1,2)/dz - diff(P(:,2:end-1),1,1)/dx + diff(txx(:,2:end-1),1,1)/dx + F;
        ResVz    = diff(txz,1,1)/dx - diff(P(2:end-1,:),1,2)/dz + diff(tzz(2:end-1,:),1,2)/dz - F*cotd(alpha);
        dVxdt    = dVxdt.*(1-damp/nx) + ResVx;
        dVzdt    = dVzdt.*(1-damp/nz) + ResVz;   
        EII2     = (Exx.^2 + Ezz.^2)/2 + Exzn.^2;
        qxT(2:end-1,:) = -diff(T,1,1)/dx;
        qzT(:,2:end-1) = -diff(T,1,2)/dz;
        Hs       = 4*etan.*EII2;
        %%% Melting
        T2       = T.*T_sc + (T0-T_K);
        H        = 1-tanh(-(T2-Tm)./0.5);
        dTdt     = - ( diff(qxT,1,1)/dx + diff(qzT,1,2)/dz ) + Hs.*(1-H*MELT);
        %%% TEST
        T_res    = -(T-T_o)/dt + (1-CN).*dTdt + CN*dTdt_o; % Crank Nicolson
        etait    = 1.0./(1.0./((EII2).^mpow.*exp( -T./(1+T/theta) )) + 1.0/eta0);
        % GPU KERNEL 6
        Vx(2:end-1,2:end-1) = Vx(2:end-1,2:end-1) + dVxdt.*dtVx;
        Vz(2:end-1,2:end-1) = Vz(2:end-1,2:end-1) + dVzdt.*dtVz;
        T        = T + dtau*T_res;
        etan     = exp(rele*log(etait) + (1-rele)*log(etan));
        % GPU KERNEL 7
        etan(:,[1 end]) = etan(:,[2 end-1]);
        % GPU KERNEL 8
        etan([1 end],:) = etan([2 end-1],:);
        % GPU KERNEL 9
        % Boundary conditions
        % BC left
        Vx(1,2:end-1)   = -Vx(2,2:end-1);
        Vz(1,2:end-1)   =  Vz(2,2:end-1);
        % BC right
        Vx(end,2:end-1) = -Vx(end-1,2:end-1);
        Vz(end,2:end-1) =  Vz(end-1,2:end-1);
        % GPU KERNEL 10
        % BC bottom
        Vx(:,1)   =  0.0;
        Vz(:,1)   = -Vz(:,2);
        qzT(:,1)  = q_arr(:,1);
        % BC top             
        Vz(:,end) = Vz(:,end-1) + dz*P(:,end)./etan(:,end)/2 + dz*(1/2-eta_b)*divV(:,end);
        Vx(2:end-1,end) = Vx(2:end-1,end-1) - dz/dx*(Vz(2:end,end-1)-Vz(1:end-1,end-1)) + dz/3*txz(:,end-1)./etas(:,end); 
        %Vx([1 end],end) = 0.0;
        T(:,end)        = 0;
        %% errors
        norm_X  = sqrt(sum(dVxdt(:).*dVxdt(:)))/(nx*nz);
        norm_Z  = sqrt(sum(dVzdt(:).*dVzdt(:)))/(nx*nz);
        norm_P  = sqrt(sum(divV(:).*divV(:)))/(nx*nz);
        %norm_T  = sqrt(sum(T_res(:).*T_res(:)))/(nx*nz);
        % Monitoring
        if (mod(iter,100)==1) 
        iterror              = max(norm_X,max(norm_Z,norm_P));
        if ((iterror < epsi) && (iter > 2)),break; end
        end
        if (mod(iter,nouti)==1) 
            fprintf('it=%d, iter=%d, err=%1.3e \n',it,iter,iterror)
            figure(1),clf,colormap jet
            subplot(3,1,1),imagesc(flipud(Vx'*L_sc/t_sc*s2y)),colorbar
            subplot(3,1,2),imagesc(flipud(T'*T_sc)+(T0-T_K)),colorbar
            subplot(3,1,3),imagesc(flipud(H')),caxis([0 1]),colorbar%imagesc(flipud(log10(etan'*tau_sc/t_sc))),colorbar
            drawnow
        end
    end
    if mod(it,20)==1,eval(['save run_TM2D_' int2str(it)]); end
    CPU_time=toc;
    MTP_eff = (nx*nz)*iter*(2*4+2)*8/1e9/CPU_time;
    fprintf('it=%d, iter=%d, err=%1.3e \n',it,iter,iterror)
    fprintf('time=%f, MTP_eff=%f \n',CPU_time,MTP_eff)
    if (max(T(:))>1 || (iter>(niter-nouti)));disp('END');break;end  
    time        = time + dt; 
end

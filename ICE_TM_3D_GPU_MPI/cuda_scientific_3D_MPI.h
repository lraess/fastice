/* FastICE: cuda_scientific include file

Copyright (C) 2019  Ludovic Raess, Aleksandar Licul et al.
 
FastICE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
FastICE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with FastICE. If not, see <http://www.gnu.org/licenses/>. */

// cuda_scientific include file, 3D MPI version: GPU_PoroVEP3D, 06.10.2018  -  © L.Räss, S.Omlin, Y.Podladchikov  -  Unil
#include <math.h>
#define _min(a,b)               ({ __typeof__ (a) _a = (a);  __typeof__ (b) _b = (b);  _a < _b ? _a : _b; })
#define _max(a,b)               ({ __typeof__ (a) _a = (a);  __typeof__ (b) _b = (b);  _a > _b ? _a : _b; })
#define mod(a,b)               (a % b)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Definition of basic macros
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define NB_THREADS             (BLOCKS_X*BLOCKS_Y*BLOCKS_Z)
#define NB_BLOCKS              (GRID_X*GRID_Y*GRID_Z)
#define def_sizes(A,nx,ny,nz)  const int sizes_##A[] = {nx,ny,nz};                            
#define      _size(A,dim)       (sizes_##A[dim-1])
#define     numel(A)           (_size(A,1)*_size(A,2)*_size(A,3))
#define       _end(A,dim)       (_size(A,dim)-1)
#define   zeros_h(A,nx,ny,nz)  def_sizes(A,nx,ny,nz);                                  \
                               DAT *A##_h; A##_h = (DAT*)malloc(numel(A)*sizeof(DAT)); \
                               for(i=0; i < (nx)*(ny)*(nz); i++){ A##_h[i]=(DAT)0.0; }
#define   zeros_d(A,n1,n2)     DAT *A##_d; cudaMalloc(&A##_d,((n1)*(n2))*sizeof(DAT));
#define     zeros(A,nx,ny,nz)  def_sizes(A,nx,ny,nz);                                         \
                               DAT *A##_d,*A##_h; A##_h = (DAT*)malloc(numel(A)*sizeof(DAT)); \
                               for(i=0; i < (nx)*(ny)*(nz); i++){ A##_h[i]=(DAT)0.0; }        \
                               cudaMalloc(&A##_d      ,numel(A)*sizeof(DAT));                 \
                               cudaMemcpy( A##_d,A##_h,numel(A)*sizeof(DAT),cudaMemcpyHostToDevice);
#define      ones(A,nx,ny,nz)  def_sizes(A,nx,ny,nz);                                         \
                               DAT *A##_d,*A##_h; A##_h = (DAT*)malloc(numel(A)*sizeof(DAT)); \
                               for(i=0; i < (nx)*(ny)*(nz); i++){ A##_h[i]=(DAT)1.0; }        \
                               cudaMalloc(&A##_d      ,numel(A)*sizeof(DAT));                 \
                               cudaMemcpy( A##_d,A##_h,numel(A)*sizeof(DAT),cudaMemcpyHostToDevice);
#define gather(A)              cudaMemcpy( A##_h,A##_d,numel(A)*sizeof(DAT),cudaMemcpyDeviceToHost);
#define free_all(A)            free(A##_h);cudaFree(A##_d);
// #define  swap(A,B,tmp)         DAT *tmp; tmp = A##_d; A##_d = B##_d; B##_d = tmp;

#define   d_xa(A)  ( A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)] - A[(ix) +  iy *_size(A,1) +  iz *_size(A,1)*_size(A,2)] )
#define   d_ya(A)  ( A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)] - A[ ix  + (iy)*_size(A,1) +  iz *_size(A,1)*_size(A,2)] )
#define   d_za(A)  ( A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] - A[ ix  +  iy *_size(A,1) + (iz)*_size(A,1)*_size(A,2)] )
#define   d_xi(A)  ( A[(ix+1) +  iyi  *_size(A,1) +  izi  *_size(A,1)*_size(A,2)] - A[(ix) +  iyi*_size(A,1) +  izi*_size(A,1)*_size(A,2)] )
#define   d_yi(A)  ( A[ ixi   + (iy+1)*_size(A,1) +  izi  *_size(A,1)*_size(A,2)] - A[ ixi + (iy)*_size(A,1) +  izi*_size(A,1)*_size(A,2)] )
#define   d_zi(A)  ( A[ ixi   +  iyi  *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] - A[ ixi +  iyi*_size(A,1) + (iz)*_size(A,1)*_size(A,2)] )

#define  d2_xi(A)  ( A[(ixi+1) +  iyi   *_size(A,1) +  izi   *_size(A,1)*_size(A,2)] - (DAT)2.0*A[ixi + iyi*_size(A,1) +  izi*_size(A,1)*_size(A,2)] + A[(ixi-1) +  iyi   *_size(A,1) +  izi   *_size(A,1)*_size(A,2)] )
#define  d2_yi(A)  ( A[ ixi    + (iyi+1)*_size(A,1) +  izi   *_size(A,1)*_size(A,2)] - (DAT)2.0*A[ixi + iyi*_size(A,1) +  izi*_size(A,1)*_size(A,2)] + A[ ixi    + (iyi-1)*_size(A,1) +  izi   *_size(A,1)*_size(A,2)] )
#define  d2_zi(A)  ( A[ ixi    +  iyi   *_size(A,1) + (izi+1)*_size(A,1)*_size(A,2)] - (DAT)2.0*A[ixi + iyi*_size(A,1) +  izi*_size(A,1)*_size(A,2)] + A[ ixi    +  iyi   *_size(A,1) + (izi-1)*_size(A,1)*_size(A,2)] )

#define   d_xiy(A) ( A[(ix+1) +  iyi  *_size(A,1) +  iz   *_size(A,1)*_size(A,2)] - A[(ix) +  iyi*_size(A,1) +  iz *_size(A,1)*_size(A,2)] )
#define   d_xiz(A) ( A[(ix+1) +  iy   *_size(A,1) +  izi  *_size(A,1)*_size(A,2)] - A[(ix) +  iy *_size(A,1) +  izi*_size(A,1)*_size(A,2)] )
#define   d_yix(A) ( A[ ixi   + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)] - A[ ixi + (iy)*_size(A,1) +  iz *_size(A,1)*_size(A,2)] )
#define   d_yiz(A) ( A[ ix    + (iy+1)*_size(A,1) +  izi  *_size(A,1)*_size(A,2)] - A[ ix  + (iy)*_size(A,1) +  izi*_size(A,1)*_size(A,2)] )
#define   d_zix(A) ( A[ ixi   +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] - A[ ixi +  iy *_size(A,1) + (iz)*_size(A,1)*_size(A,2)] )
#define   d_ziy(A) ( A[ ix    +  iyi  *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] - A[ ix  +  iyi*_size(A,1) + (iz)*_size(A,1)*_size(A,2)] )
#define    all(A)  ( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)] )
#define    inn(A)  ( A[ ixi   +  iyi  *_size(A,1) +  izi  *_size(A,1)*_size(A,2)] )
#define   in_x(A)  ( A[ ixi   +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)] )
#define   in_y(A)  ( A[ ix    +  iyi  *_size(A,1) +  iz   *_size(A,1)*_size(A,2)] )
#define   in_z(A)  ( A[ ix    +  iy   *_size(A,1) +  izi  *_size(A,1)*_size(A,2)] )
#define  in_yz(A)  ( A[ ix    +  iyi  *_size(A,1) +  izi  *_size(A,1)*_size(A,2)] )
#define  in_xz(A)  ( A[ ixi   +  iy   *_size(A,1) +  izi  *_size(A,1)*_size(A,2)] )
#define  in_xy(A)  ( A[ ixi   +  iyi  *_size(A,1) +  iz   *_size(A,1)*_size(A,2)] )
#define av_xyi(A) (( A[ ix    +  iy   *_size(A,1) +  izi  *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iy   *_size(A,1) +  izi  *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    + (iy+1)*_size(A,1) +  izi  *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) + (iy+1)*_size(A,1) +  izi  *_size(A,1)*_size(A,2)] )*(DAT)0.25)
#define av_xzi(A) (( A[ ix    +  iyi  *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iyi  *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    +  iyi  *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iyi  *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.25)
#define av_yzi(A) (( A[ ixi   +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ixi   + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ixi   +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                 A[ ixi   + (iy+1)*_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.25)
#define av_xyz(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                 A[ ix    + (iy+1)*_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) + (iy+1)*_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.125)
#define av_xya(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)] )*(DAT)0.25)
#define av_xza(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.25)
#define av_yza(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                 A[ ix    + (iy+1)*_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.25)
#define  av_xa(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)] )*(DAT)0.5)
#define  av_ya(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)] )*(DAT)0.5)
#define  av_za(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.5)
#define  av_xi(A) (( A[ ix    +  iyi  *_size(A,1) +  izi  *_size(A,1)*_size(A,2)]  \
   +                 A[(ix+1) +  iyi  *_size(A,1) +  izi  *_size(A,1)*_size(A,2)] )*(DAT)0.5)
#define  av_yi(A) (( A[ ixi   +  iy   *_size(A,1) +  izi  *_size(A,1)*_size(A,2)]  \
   +                 A[ ixi   + (iy+1)*_size(A,1) +  izi  *_size(A,1)*_size(A,2)] )*(DAT)0.5)
#define  av_zi(A) (( A[ ixi   +  iyi  *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                 A[ ixi   +  iyi  *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.5)
// Anton's invariant definition (not cancelling the closest neighbours - averaging the squared values)
#define av_xya2(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]*A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                  A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]*A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                  A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]*A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                  A[(ix+1) + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]*A[(ix+1) + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)] )*(DAT)0.25)
#define av_xza2(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]*A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                  A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]*A[(ix+1) +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                  A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]*A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                  A[(ix+1) +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]*A[(ix+1) +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.25)
#define av_yza2(A) (( A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]*A[ ix    +  iy   *_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                  A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]*A[ ix    + (iy+1)*_size(A,1) +  iz   *_size(A,1)*_size(A,2)]  \
   +                  A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]*A[ ix    +  iy   *_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]  \
   +                  A[ ix    + (iy+1)*_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)]*A[ ix    + (iy+1)*_size(A,1) + (iz+1)*_size(A,1)*_size(A,2)] )*(DAT)0.25)

#define select(A,ix,iy,iz) ( A[ix + iy*_size(A,1) + iz*_size(A,1)*_size(A,2)] )
// participate_a: Test if the thread (ix,iy,iz) has to participate for the following computation of (all) A.
// participate_i: Test if the thread (ix,iy,iz) has to participate for the following computation of (inn) A.
#define participate_a(A)   (ix< _size(A,1)         && iy< _size(A,2)         && iz< _size(A,3)        )
#define participate_i(A)   (ix<(_size(A,1)-(int)2) && iy<(_size(A,2)-(int)2) && iz<(_size(A,3)-(int)2))
#define participate_ix(A)  (ix<(_size(A,1)-(int)2) && iy< _size(A,2)         && iz< _size(A,3)        )
#define participate_iy(A)  (ix< _size(A,1)         && iy<(_size(A,2)-(int)2) && iz< _size(A,3)        )
#define participate_iz(A)  (ix< _size(A,1)         && iy< _size(A,2)         && iz<(_size(A,3)-(int)2))
#define participate_ixy(A) (ix<(_size(A,1)-(int)2) && iy<(_size(A,2)-(int)2) && iz< _size(A,3)        )
#define participate_ixz(A) (ix<(_size(A,1)-(int)2) && iy< _size(A,2)         && iz<(_size(A,3)-(int)2))
#define participate_iyz(A) (ix< _size(A,1)         && iy<(_size(A,2)-(int)2) && iz<(_size(A,3)-(int)2))
#define partic_shift_x(A)  (ix<(_size(A,1)-(int)1) && iy< _size(A,2)         && iz< _size(A,3)        )
#define partic_shift_y(A)  (ix< _size(A,1)         && iy<(_size(A,2)-(int)1) && iz< _size(A,3)        )
#define partic_shift_z(A)  (ix< _size(A,1)         && iy< _size(A,2)         && iz<(_size(A,3)-(int)1))
// update inner from boundaries
#define bc_no_dx0(A) if (ix==0)        select(A,0       ,iy      ,iz      ) = select(A,1           ,iy          ,iz          )
#define bc_no_dxE(A) if (ix==_end(A,1)) select(A,_end(A,1),iy      ,iz      ) = select(A,(_end(A,1)-1),iy          ,iz          )
#define bc_no_dy0(A) if (iy==0)        select(A,ix      ,0       ,iz      ) = select(A,ix          ,1           ,iz          )
#define bc_no_dyE(A) if (iy==_end(A,2)) select(A,ix      ,_end(A,2),iz      ) = select(A,ix          ,(_end(A,2)-1),iz          )
#define bc_no_dz0(A) if (iz==0)        select(A,ix      ,iy      ,0       ) = select(A,ix          ,iy          ,1           )
#define bc_no_dzE(A) if (iz==_end(A,3)) select(A,ix      ,iy      ,_end(A,3)) = select(A,ix          ,iy          ,(_end(A,3)-1))
// copy boundary from A to B (A & B must be same _size !)
#define cp_bc_dx0(A,B) if (ix==0)        select(B,0       ,iy      ,iz      ) = select(A,0       ,iy      ,iz      )
#define cp_bc_dxE(A,B) if (ix==_end(A,1)) select(B,_end(A,1),iy      ,iz      ) = select(A,_end(A,1),iy      ,iz      )
#define cp_bc_dy0(A,B) if (iy==0)        select(B,ix      ,0       ,iz      ) = select(A,ix      ,0       ,iz      )
#define cp_bc_dyE(A,B) if (iy==_end(A,2)) select(B,ix      ,_end(A,2),iz      ) = select(A,ix      ,_end(A,2),iz      )
#define cp_bc_dz0(A,B) if (iz==0)        select(B,ix      ,iy      ,0       ) = select(A,ix      ,iy      ,0       )
#define cp_bc_dzE(A,B) if (iz==_end(A,3)) select(B,ix      ,iy      ,_end(A,3)) = select(A,ix      ,iy      ,_end(A,3))
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables for cuda and Multi-GPU applications
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int nprocs=-1, me=-1, gpu_id=-1;
dim3 grid, block;
int coords_h[3]={0,0,0};
int* coords_d=NULL;
int* dims_d=NULL;
#ifdef USE_MPI
#include "mpi.h"
int dims[3]={DIMS_X,DIMS_Y,DIMS_Z};
#define neighbours(dim,nr)  __neighbours[nr + dim*2]
int __neighbours[2*NDIMS]={-1}; // DEBUG neighbours(DIM.nr) macro in my case oposite to Sam's
int reqnr=0, tag=0;
int periods[NDIMS]={0};
int reorder=1;
dim3 grid_mpi0, block_mpi0, grid_mpi1, block_mpi1, grid_mpi2, block_mpi2;
#define NREQS1  (2*2)
#define NREQS   (2*2*3)
// #define NREQS  (2*2*NDIMS*3)
MPI_Comm    topo_comm=MPI_COMM_NULL;
MPI_Request req1[NREQS1]={MPI_REQUEST_NULL};
MPI_Request req[NREQS]={MPI_REQUEST_NULL};
// CommOverlap
cudaStream_t  streams[2];
int repeat = 2;
#define CommOverlap() if ( istep==0 &&   ix>=BOUNDARY_WIDTH_X && ix<=(nx  )-1-BOUNDARY_WIDTH_X &&           \
                                         iy>=BOUNDARY_WIDTH_Y && iy<=(ny  )-1-BOUNDARY_WIDTH_Y &&           \
                                         iz>=BOUNDARY_WIDTH_Z && iz<=(nz  )-1-BOUNDARY_WIDTH_Z    ) return; \
                      if ( istep==1 && ( ix< BOUNDARY_WIDTH_X || ix> (nx  )-1-BOUNDARY_WIDTH_X ||           \
                                         iy< BOUNDARY_WIDTH_Y || iy> (ny  )-1-BOUNDARY_WIDTH_Y ||           \
                                         iz< BOUNDARY_WIDTH_Z || iz> (nz  )-1-BOUNDARY_WIDTH_Z  ) ) return;
#else
int dims[3]={1,1,1};
cudaStream_t  streams[1];
int repeat = 1;
#define CommOverlap() 
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CUDA Bc_no_XYZ kernels & Bc_no_Flux_X,Y,Z Free Surface stuff
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__global__ void Bc_no_X_G(int*coords, int*dims, DAT*bc_X, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific  
  def_sizes(bc_X ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (coords[0]==0          ){ if (participate_a(bc_X)){ bc_no_dx0(bc_X); } }
  if (coords[0]==(dims[0]-1)){ if (participate_a(bc_X)){ bc_no_dxE(bc_X); } }
}
__global__ void Bc_no_Y_G(int*coords, int*dims, DAT*bc_Y, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific  
  def_sizes(bc_Y ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (coords[1]==0          ){ if (participate_a(bc_Y)){ bc_no_dy0(bc_Y); } }
  if (coords[1]==(dims[1]-1)){ if (participate_a(bc_Y)){ bc_no_dyE(bc_Y); } }
}
__global__ void Bc_no_Z_G(int*coords, int*dims, DAT*bc_Z, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific  
  def_sizes(bc_Z ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (coords[2]==0          ){ if (participate_a(bc_Z)){ bc_no_dz0(bc_Z); } }
  if (coords[2]==(dims[2]-1)){ if (participate_a(bc_Z)){ bc_no_dzE(bc_Z); } }
}
#define __Bc_no_XYZ_G(A)  Bc_no_X_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                          Bc_no_Y_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                          Bc_no_Z_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_Vx_G(A)   Bc_no_Y_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                          Bc_no_Z_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_Vy_G(A)   Bc_no_X_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                          Bc_no_Z_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_Vz_G(A)   Bc_no_X_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                          Bc_no_Y_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_X_G(A)    Bc_no_X_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_Y_G(A)    Bc_no_Y_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_Z_G(A)    Bc_no_Z_G<<<grid, block>>>(coords_d, dims_d, A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
__global__ void Bc_no_X(DAT*bc_X, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific  
  def_sizes(bc_X ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (participate_a(bc_X)){ bc_no_dx0(bc_X);
                            bc_no_dxE(bc_X); }
}
__global__ void Bc_no_Y(DAT*bc_Y, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific  
  def_sizes(bc_Y ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (participate_a(bc_Y)){ bc_no_dy0(bc_Y);
                            bc_no_dyE(bc_Y); }
}
__global__ void Bc_no_Z(DAT*bc_Z, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific  
  def_sizes(bc_Z ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (participate_a(bc_Z)){ bc_no_dz0(bc_Z);
                            bc_no_dzE(bc_Z); }
}
#define __Bc_no_XYZ(A)  Bc_no_X<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                        Bc_no_Y<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                        Bc_no_Z<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_Vx(A)   Bc_no_Y<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                        Bc_no_Z<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_Vy(A)   Bc_no_X<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                        Bc_no_Z<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

#define __Bc_no_Vz(A)   Bc_no_X<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize(); \
                        Bc_no_Y<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3)); cudaDeviceSynchronize();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CUDA __MPI_max __MPI_min __MPI_mean & __MPI_norm subroutine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define params_write()  for (int i=0; i<NB_PARAMS; i++){  params_evol_h[i+evol_count*_size(params_evol,1)] = params_h[i];  } \
                        evol_count++;

#define blockId        (blockIdx.x  +  blockIdx.y *gridDim.x +  blockIdx.z *gridDim.x *gridDim.y)
#define threadId       (threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y)
#define isBlockMaster  (threadIdx.x==0 && threadIdx.y==0 && threadIdx.z==0)
// maxval //
#define block_max_init() DAT __thread_maxval=0.0;
#define __thread_max(A)  if (participate_a(A)){ __thread_maxval = _max(fabs(__thread_maxval),fabs(select(A,ix ,iy ,iz))); } 

__shared__ volatile DAT __block_maxval;
#define __block_max(A)    __thread_max(A);                                                            \
                          if (isBlockMaster){ __block_maxval=0; }                                     \
                          __syncthreads();                                                            \
                          for (int i=0; i < (NB_THREADS); i++){                                       \
                            if (i==threadId){ __block_maxval = _max(__block_maxval,__thread_maxval); } \
                            __syncthreads();                                                          \
                          }

__global__ void __device_max_d(DAT*A, const int nx_A,const int ny_A,const int nz_A, DAT*__device_maxval){
  // CUDA specific
  def_sizes(A,nx_A,ny_A,nz_A);
  block_max_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
  // find the maxval for each block
  __block_max(A);
  __device_maxval[blockId] = __block_maxval;
}
// minval //
#define block_min_init() DAT __thread_minval=999999.9;
#define __thread_min(A)  if (participate_a(A)){ __thread_minval = _min(fabs(__thread_minval),fabs(select(A,ix ,iy ,iz))); }

__shared__ volatile DAT __block_minval;
#define __block_min(A)    __thread_min(A);                                                            \
                          if (isBlockMaster){ __block_minval=999999.9; }                              \
                          __syncthreads();                                                            \
                          for (int i=0; i < (NB_THREADS); i++){                                       \
                            if (i==threadId){ __block_minval = _min(__block_minval,__thread_minval); } \
                            __syncthreads();                                                          \
                          }

__global__ void __device_min_d(DAT*A, const int nx_A,const int ny_A,const int nz_A, DAT*__device_minval){
  // CUDA specific
  def_sizes(A,nx_A,ny_A,nz_A);
  block_min_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
  // find the minval for each block
  __block_min(A);
  __device_minval[blockId] = __block_minval;
}
// meanval //
#define block_mean_init() DAT __thread_meanval=0.0;
#define __thread_mean(A)  if (participate_a(A)){ __thread_meanval = select(A,ix ,iy ,iz); }

__shared__ volatile DAT __block_meanval;
#define __block_mean(A)    __thread_mean(A);                                                                                     \
                           if (isBlockMaster){ __block_meanval=0.0; }                                                            \
                           __syncthreads();                                                                                      \
                           for (int i=0; i < (NB_THREADS); i++){                                                                 \
                             if (i==threadId){ __block_meanval = __block_meanval + ((__thread_meanval) / ((DAT)(NB_THREADS))); } \
                             __syncthreads();                                                                                    \
                           }

__global__ void __device_mean_d(DAT*A, const int nx_A,const int ny_A,const int nz_A, DAT*__device_meanval){
  // CUDA specific
  def_sizes(A,nx_A,ny_A,nz_A);
  block_mean_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
  // find the meaval for each block
  __block_mean(A);
  __device_meanval[blockId] = __block_meanval;
}
// normL2 //
#define block_norm_init() DAT __thread_normval=0.0;
#define __thread_norm(A)  if (participate_a(A)){ __thread_normval = (select(A,ix ,iy ,iz)*select(A,ix ,iy ,iz)); }

__shared__ volatile DAT __block_normval;
#define __block_norm(A)    __thread_norm(A);                                                           \
                           if (isBlockMaster){ __block_normval=0.0; }                                  \
                           __syncthreads();                                                            \
                           for (int i=0; i < (NB_THREADS); i++){                                       \
                             if (i==threadId){ __block_normval = __block_normval + __thread_normval; } \
                             __syncthreads();                                                          \
                           }

__global__ void __device_norm_d(DAT*A, const int nx_A,const int ny_A,const int nz_A, DAT*__device_normval){
  // CUDA specific
  def_sizes(A,nx_A,ny_A,nz_A);
  block_norm_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
  // find the normval for each block
  __block_norm(A);
  __device_normval[blockId] = __block_normval;
}
////////// __MPI_max , __MPI_min & __MPI_mean Macros //////////
#ifdef USE_MPI
#define __MPI_max(A)  __device_max_d<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3), __device_maxval_d); \
                      gather(__device_maxval); device_MAX=(DAT)0.0;                                             \
                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                                           \
                        device_MAX = _max(device_MAX,__device_maxval_h[i]);                                      \
                      }                                                                                         \
                      MPI_Allreduce(&device_MAX,&global_MAX,1,MPI_DAT,MPI_MAX,topo_comm);                       \
                      params_h[A##_MAX] = (global_MAX);
#define __MPI_min(A)  __device_min_d<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3), __device_minval_d); \
                      gather(__device_minval); device_MIN=(DAT)999999;                                          \
                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                                           \
                        device_MIN = _min(device_MIN,__device_minval_h[i]);                                      \
                      }                                                                                         \
                      MPI_Allreduce(&device_MIN,&global_MIN,1,MPI_DAT,MPI_MIN,topo_comm);                       \
                      params_h[A##_MIN] = (global_MIN);
#define __MPI_mean(A) __device_mean_d<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3), __device_meanval_d); \
                      gather(__device_meanval); device_MEAN=(DAT)0.0;                                             \
                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                                             \
                        device_MEAN = device_MEAN + ((__device_meanval_h[i]) / ((DAT)(grid.x*grid.y*grid.z)) / ((DAT)(nprocs))); \
                      }                                                                                           \
                      MPI_Allreduce(&device_MEAN,&global_MEAN,1,MPI_DAT,MPI_SUM,topo_comm);                       \
                      params_h[A##_MEAN] = (global_MEAN);
#define __MPI_norm(A) __device_norm_d<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3), __device_normval_d); \
                      gather(__device_normval); device_NORM=(DAT)0.0;                                             \
                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                                             \
                        device_NORM = device_NORM + __device_normval_h[i];                                        \
                      }                                                                                           \
                      MPI_Allreduce(&device_NORM,&global_NORM,1,MPI_DAT,MPI_SUM,topo_comm);                       \
                      global_NORM = (DAT)1.0/((DAT)N)*sqrt(global_NORM);                                          \
                      params_h[A##_NORM] = (global_NORM);
#else
// DEBUG: changed name of __device_max(A) & __device_max_iter(A) to __MPI_max in order to switch btw change the function call in the timeloop
#define __MPI_max(A)  __device_max_d<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3), __device_maxval_d); \
                      gather(__device_maxval); device_MAX=(DAT)0.0;                                             \
                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                                           \
                         device_MAX = _max(device_MAX,__device_maxval_h[i]);                                     \
                      }                                                                                         \
                      params_h[A##_MAX] = (device_MAX);
#define __MPI_min(A)  __device_min_d<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3), __device_minval_d); \
                      gather(__device_minval); device_MIN=(DAT)999999;                                          \
                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                                           \
                         device_MIN = _min(device_MIN,__device_minval_h[i]);                                     \
                      }                                                                                         \
                      params_h[A##_MIN] = (device_MIN);
#define __MPI_mean(A) __device_mean_d<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3), __device_meanval_d); \
                      gather(__device_meanval); device_MEAN=(DAT)0.0;                                             \
                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                                             \
                        device_MEAN = device_MEAN + ((__device_meanval_h[i]) / ((DAT)(grid.x*grid.y*grid.z)));    \
                      }                                                                                           \
                      params_h[A##_MEAN] = (device_MEAN);
#define __MPI_norm(A) __device_norm_d<<<grid, block>>>(A##_d, _size(A,1),_size(A,2),_size(A,3), __device_normval_d); \
                      gather(__device_normval); device_NORM=(DAT)0.0;                                             \
                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                                             \
                        device_NORM = device_NORM + __device_normval_h[i];                                        \
                      }                                                                                           \
                      device_NORM = (DAT)1.0/((DAT)N)*sqrt(device_NORM);                                          \
                      params_h[A##_NORM] = (device_NORM);
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions (host code)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void __set_up_gpu(int argc, char *argv[]){
  block.x = BLOCKS_X; grid.x = GRID_X;
  block.y = BLOCKS_Y; grid.y = GRID_Y;
  block.z = BLOCKS_Z; grid.z = GRID_Z;
  #ifdef USE_MPI
  block_mpi0.x = 1;        grid_mpi0.x = 1; 
  block_mpi0.y = BLOCKS_Y; grid_mpi0.y = GRID_Y;
  block_mpi0.z = BLOCKS_Z; grid_mpi0.z = GRID_Z;
  block_mpi1.x = BLOCKS_X; grid_mpi1.x = GRID_X;
  block_mpi1.y = 1;        grid_mpi1.y = 1;
  block_mpi1.z = BLOCKS_Z; grid_mpi1.z = GRID_Z;
  block_mpi2.x = BLOCKS_X; grid_mpi2.x = GRID_X;
  block_mpi2.y = BLOCKS_Y; grid_mpi2.y = GRID_Y;
  block_mpi2.z = 1;        grid_mpi2.z = 1;
  int me_loc=-1;
  // cudaSetDeviceFlags(cudaDeviceMapHost); // DEBUG: needs to be set before context creation !
  const char* me_str     = getenv("OMPI_COMM_WORLD_RANK");
  const char* me_loc_str = getenv("OMPI_COMM_WORLD_LOCAL_RANK");
  // const char* me_str     = getenv("MV2_COMM_WORLD_RANK");
  // const char* me_loc_str = getenv("MV2_COMM_WORLD_LOCAL_RANK");
  me     = atoi(me_str);
  me_loc = atoi(me_loc_str);
  gpu_id = me_loc; // GPU_ID
  #else
  gpu_id = GPU_ID;
  nprocs=1, me=0;
  #endif
  cudaSetDevice(gpu_id); cudaGetDevice(&gpu_id);
  // cudaDeviceReset(); 
  cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);  // set L1 to prefered
  #ifdef USE_MPI
  // CommOverlap
  int leastPriority=-1, greatestPriority=-1;
  cudaDeviceGetStreamPriorityRange(&leastPriority, &greatestPriority);
  cudaStreamCreateWithPriority(&streams[0], cudaStreamNonBlocking, greatestPriority);
  cudaStreamCreateWithPriority(&streams[1], cudaStreamNonBlocking, leastPriority);
  #endif
}
#define set_up_gpu()  __set_up_gpu(argc, argv);

#ifdef USE_MPI
#define BARRIER()  ( MPI_Barrier(MPI_COMM_WORLD) )
void __set_up_parallelisation(int argc, char *argv[]){
  // MPI STUFF
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  MPI_Dims_create(nprocs, NDIMS, dims);
  MPI_Cart_create(MPI_COMM_WORLD, NDIMS, dims, periods, reorder, &topo_comm);
  MPI_Comm_rank(topo_comm, &me);
  MPI_Cart_coords(topo_comm, me, NDIMS, coords_h);
  cudaMalloc(&coords_d,3*sizeof(int)); cudaMemcpy(coords_d ,coords_h,3*sizeof(int),cudaMemcpyHostToDevice);
  cudaMalloc(&dims_d  ,3*sizeof(int)); cudaMemcpy(dims_d   ,dims    ,3*sizeof(int),cudaMemcpyHostToDevice);
  for (int i=0; i<NDIMS; i++){ MPI_Cart_shift(topo_comm, i, 1, &(neighbours(i,0)), &(neighbours(i,1))); }
  if (me==0){ printf("\n  ------------------------------  ");
              printf("\n  |     MPI - GPU_ICE_TM_3D    |  ");
              printf("\n  ------------------------------  \n");
              printf("\nLaunching %d GPU MPI processes: dims(1)=%d, dims(2)=%d, dims(3)=%d ...\n", nprocs,dims[0],dims[1],dims[2]); }
}
#else
#define BARRIER()
void __set_up_parallelisation(int argc, char *argv[]){
  cudaMalloc(&coords_d,3*sizeof(int)); cudaMemcpy(coords_d ,coords_h,3*sizeof(int),cudaMemcpyHostToDevice);
  cudaMalloc(&dims_d  ,3*sizeof(int)); cudaMemcpy(dims_d   ,dims    ,3*sizeof(int),cudaMemcpyHostToDevice);
  if (me==0){ printf("\n   -------------------------  ");
              printf("\n   |      GPU_ICE_TM_3D    |  ");
              printf("\n   -------------------------  \n\n"); }
}
#endif
#define set_up_parallelisation()  __set_up_parallelisation(argc, argv);

void  clean_cuda(){ 
  cudaError_t ce = cudaGetLastError();
  if(ce != cudaSuccess){ printf("ERROR launching GPU C-CUDA program: %s\n", cudaGetErrorString(ce)); cudaDeviceReset();}
  // CommOverlap
  cudaStreamDestroy(*streams);
}

// Timer // to add for mpi in each tic and toc at the beginning: MPI_Barrier(MPI_COMM_WORLD);
#include "sys/time.h"
double timer_start = 0;
double cpu_sec(){ struct timeval tp; gettimeofday(&tp,NULL); return tp.tv_sec+1e-6*tp.tv_usec; }
void   tic(){ timer_start = cpu_sec(); }
double toc(){ return cpu_sec()-timer_start; }
void   tim(const char *what, double n){ double s=toc();if(me==0){ printf("%s: %8.3f seconds",what,s);if(n>0)printf(", %8.3f GB/s", n/s); printf("\n"); } }

////////// ========== Save & Read Data functions ========== //////////
void save_info(){
  FILE* fid;
  if (me==0){ fid=fopen("0_nxyz.inf"     , "w"); fprintf(fid,"%d %d %d %d" ,PRECIS,(nx-2)*dims[0]+2,(ny-2)*dims[1]+2,(nz-2)*dims[2]+2);   fclose(fid); }
  if (me==0){ fid=fopen("0_nxyz_l.inf"   , "w"); fprintf(fid,"%d %d %d"    ,nx,ny,nz);                                                    fclose(fid); }
  if (me==0){ fid=fopen("0_dims.inf"     , "w"); fprintf(fid,"%d %d %d %d" ,nprocs,dims[0],dims[1],dims[2]);                              fclose(fid); }
  if (me==0){ fid=fopen("0_evol.inf"     , "w"); fprintf(fid,"%d %d"       ,NB_PARAMS,nt);                                                fclose(fid); }
}

void save_coords(){
  char* fname; FILE* fid;
  asprintf(&fname, "%d_co.inf", me); 
  fid=fopen(fname, "w"); fprintf(fid,"%d %d %d",coords_h[0],coords_h[1],coords_h[2]); fclose(fid); free(fname);
}

void save_array(DAT* A, size_t nb_elems, const char A_name[], int isave){
  char* fname; FILE* fid; asprintf(&fname, "%d_%d_%s.res" ,isave, me, A_name); 
  fid=fopen(fname, "wb"); fwrite(A, PRECIS, nb_elems, fid); fclose(fid);  free(fname);
}
#define SaveArray(A,A_name)  gather(A); save_array(A##_h, numel(A), A_name, isave);

void read_data(DAT* A_h, DAT* A_d, int nx,int ny,int nz, const char A_name[], const char B_name[],int isave){
  char* bname; FILE* fid; size_t nb_elems = nx*ny*nz;
  asprintf(&bname, "%d_%d_%s.%s", isave, me, A_name, B_name);
  fid=fopen(bname, "rb"); // Open file
  if (!fid){ fprintf(stderr, "\nUnable to open file %s \n", bname); return; }
  fread(A_h, PRECIS, nb_elems, fid); fclose(fid);
  cudaMemcpy(A_d, A_h, nb_elems*sizeof(DAT), cudaMemcpyHostToDevice);
  if (me==0){ printf("Read data: %d files %s.%s loaded (_size = %dx%dx%d) \n", nprocs,A_name,B_name,nx,ny,nz); } free(bname);
}

void read_data_h(DAT* A_h, int nx,int ny,int nz, const char A_name[], const char B_name[],int isave){
  char* bname; FILE* fid; size_t nb_elems = nx*ny*nz;
  asprintf(&bname, "%d_%d_%s.%s", isave, me, A_name, B_name);
  fid=fopen(bname, "rb"); // Open file
  if (!fid){ fprintf(stderr, "\nUnable to open file %s \n", bname); return; }
  fread(A_h, PRECIS, nb_elems, fid); fclose(fid);
  if (me==0){ printf("Read data: %d files %s.%s loaded (_size = %dx%dx%d) \n", nprocs,A_name,B_name,nx,ny,nz); } free(bname);
}

void read_val_h(DAT* val, const char A_name[], const char B_name[],int isave){
  char* bname; FILE* fid; size_t nb_elems = 1; int me_loc = 0;
  asprintf(&bname, "%d_%d_%s.%s", isave, me_loc, A_name, B_name);
  fid=fopen(bname, "rb"); // Open file
  if (!fid){ fprintf(stderr, "\nUnable to open file %s \n", bname); return; }
  fread(val, PRECIS, nb_elems, fid); fclose(fid);
  if (me==0){ printf("Read value: %s.%s loaded (val=%1.4f) \n", A_name,B_name,*val); } free(bname);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MPI subroutines
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef USE_MPI
// MPI buffer init
__global__ void write_to_mpi_sendbuffer_00(DAT* A_send_00,DAT* A, const int nx_A, const int ny_A, const int nz_A, const int nx){
    int ix;
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    ix =          ( (2+((nx_A)-nx))-1 );
    if (iz<(nz_A) && iy<(ny_A)){
        A_send_00[iy + iz*(ny_A)] = A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)]; }
}
__global__ void write_to_mpi_sendbuffer_01(DAT* A_send_01,DAT* A, const int nx_A, const int ny_A, const int nz_A, const int nx){
    int ix;
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    ix = (nx_A)-1-( (2+((nx_A)-nx))-1 );
    if (iz<(nz_A) && iy<(ny_A)){
        A_send_01[iy + iz*(ny_A)] = A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)]; }
}
__global__ void write_to_mpi_sendbuffer_10(DAT* A_send_10,DAT* A, const int nx_A, const int ny_A, const int nz_A, const int ny){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy;
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    iy =          ( (2+((ny_A)-ny))-1 );
    if (iz<(nz_A) && ix<(nx_A)){
        A_send_10[ix + iz*(nx_A)] = A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)]; }
}
__global__ void write_to_mpi_sendbuffer_11(DAT* A_send_11,DAT* A, const int nx_A, const int ny_A, const int nz_A, const int ny){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy;
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    iy = (ny_A)-1-( (2+((ny_A)-ny))-1 );
    if (iz<(nz_A) && ix<(nx_A)){
        A_send_11[ix + iz*(nx_A)] = A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)]; }
}
__global__ void write_to_mpi_sendbuffer_20(DAT* A_send_20,DAT* A, const int nx_A, const int ny_A, const int nz_A, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz;
    iz =          ( (2+((nz_A)-nz))-1 );
    if (iy<(ny_A) && ix<(nx_A)){
        A_send_20[ix + iy*(nx_A)] = A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)]; }
}
__global__ void write_to_mpi_sendbuffer_21(DAT* A_send_21,DAT* A, const int nx_A, const int ny_A, const int nz_A, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz;
    iz = (nz_A)-1-( (2+((nz_A)-nz))-1 );
    if (iy<(ny_A) && ix<(nx_A)){
        A_send_21[ix + iy*(nx_A)] = A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)]; }
}
__global__ void read_from_mpi_recvbuffer_00(DAT* A,DAT* A_recv_00, const int nx_A, const int ny_A, const int nz_A){
    int ix;
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    ix = 0;
    if (iz<(nz_A) && iy<(ny_A)){
        A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)] = A_recv_00[iy + iz*(ny_A)]; }
}
__global__ void read_from_mpi_recvbuffer_01(DAT* A,DAT* A_recv_01, const int nx_A, const int ny_A, const int nz_A){
    int ix;
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    ix = (nx_A)-1;
    if (iz<(nz_A) && iy<(ny_A)){
        A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)] = A_recv_01[iy + iz*(ny_A)]; }
}
__global__ void read_from_mpi_recvbuffer_10(DAT* A,DAT* A_recv_10, const int nx_A, const int ny_A, const int nz_A){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy;
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    iy = 0;
    if (iz<(nz_A) && ix<(nx_A)){
        A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)] = A_recv_10[ix + iz*(nx_A)]; }
}
__global__ void read_from_mpi_recvbuffer_11(DAT* A,DAT* A_recv_11, const int nx_A, const int ny_A, const int nz_A){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy;
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    iy = (ny_A)-1;
    if (iz<(nz_A) && ix<(nx_A)){
        A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)] = A_recv_11[ix + iz*(nx_A)]; }
}
__global__ void read_from_mpi_recvbuffer_20(DAT* A,DAT* A_recv_20, const int nx_A, const int ny_A, const int nz_A){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz;
    iz = 0;
    if (iy<(ny_A) && ix<(nx_A)){
        A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)] = A_recv_20[ix + iy*(nx_A)]; }
}
__global__ void read_from_mpi_recvbuffer_21(DAT* A,DAT* A_recv_21, const int nx_A, const int ny_A, const int nz_A){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz;
    iz = (nz_A)-1;
    if (iy<(ny_A) && ix<(nx_A)){
        A[ix + iy*(nx_A) + iz*(nx_A)*(ny_A)] = A_recv_21[ix + iy*(nx_A)]; }
}
#define update_sides(A)  __update_sides(A,_size(A,1),_size(A,2),_size(A,3));
#define __update_sides(A,nx_A,ny_A,nz_A)  if (istep==0){ cudaDeviceSynchronize(); } else if (istep==1){ \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_01<<<grid_mpi0,block_mpi0,0,streams[0]>>>(A##_send_01_d, A##_d, nx_A, ny_A, nz_A, nx); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_00_d, (ny_A)*(nz_A), MPI_DAT, neighbours(0,0), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      if (neighbours(0,1) != MPI_PROC_NULL){  MPI_Isend(A##_send_01_d, (ny_A)*(nz_A), MPI_DAT, neighbours(0,1), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_00<<<grid_mpi0,block_mpi0,0,streams[0]>>>(A##_send_00_d, A##_d, nx_A, ny_A, nz_A, nx); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_01_d, (ny_A)*(nz_A), MPI_DAT, neighbours(0,1), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      if (neighbours(0,0) != MPI_PROC_NULL){  MPI_Isend(A##_send_00_d, (ny_A)*(nz_A), MPI_DAT, neighbours(0,0), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      MPI_Waitall(reqnr,req1,MPI_STATUSES_IGNORE);  reqnr=0;  for (int j=0; j<NREQS1; j++){ req1[j]=MPI_REQUEST_NULL; }; \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_00<<<grid_mpi0,block_mpi0,0,streams[0]>>>(A##_d, A##_recv_00_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_01<<<grid_mpi0,block_mpi0,0,streams[0]>>>(A##_d, A##_recv_01_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_11<<<grid_mpi1,block_mpi1,0,streams[0]>>>(A##_send_11_d, A##_d, nx_A, ny_A, nz_A, ny); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_10_d, (nx_A)*(nz_A), MPI_DAT, neighbours(1,0), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      if (neighbours(1,1) != MPI_PROC_NULL){  MPI_Isend(A##_send_11_d, (nx_A)*(nz_A), MPI_DAT, neighbours(1,1), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_10<<<grid_mpi1,block_mpi1,0,streams[0]>>>(A##_send_10_d, A##_d, nx_A, ny_A, nz_A, ny); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_11_d, (nx_A)*(nz_A), MPI_DAT, neighbours(1,1), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      if (neighbours(1,0) != MPI_PROC_NULL){  MPI_Isend(A##_send_10_d, (nx_A)*(nz_A), MPI_DAT, neighbours(1,0), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      MPI_Waitall(reqnr,req1,MPI_STATUSES_IGNORE);  reqnr=0;  for (int j=0; j<NREQS1; j++){ req1[j]=MPI_REQUEST_NULL; }; \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_10<<<grid_mpi1,block_mpi1,0,streams[0]>>>(A##_d, A##_recv_10_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_11<<<grid_mpi1,block_mpi1,0,streams[0]>>>(A##_d, A##_recv_11_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_21<<<grid_mpi2,block_mpi2,0,streams[0]>>>(A##_send_21_d, A##_d, nx_A, ny_A, nz_A, nz); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_20_d, (nx_A)*(ny_A), MPI_DAT, neighbours(2,0), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      if (neighbours(2,1) != MPI_PROC_NULL){  MPI_Isend(A##_send_21_d, (nx_A)*(ny_A), MPI_DAT, neighbours(2,1), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_20<<<grid_mpi2,block_mpi2,0,streams[0]>>>(A##_send_20_d, A##_d, nx_A, ny_A, nz_A, nz); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_21_d, (nx_A)*(ny_A), MPI_DAT, neighbours(2,1), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      if (neighbours(2,0) != MPI_PROC_NULL){  MPI_Isend(A##_send_20_d, (nx_A)*(ny_A), MPI_DAT, neighbours(2,0), tag, topo_comm, &(req1[reqnr]));  reqnr++;  } \
                      MPI_Waitall(reqnr,req1,MPI_STATUSES_IGNORE);  reqnr=0;  for (int j=0; j<NREQS1; j++){ req1[j]=MPI_REQUEST_NULL; }; \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_20<<<grid_mpi2,block_mpi2,0,streams[0]>>>(A##_d, A##_recv_20_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_21<<<grid_mpi2,block_mpi2,0,streams[0]>>>(A##_d, A##_recv_21_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); }


#define update_sides3(A,B,C)  __update_sides3(A,_size(A,1),_size(A,2),_size(A,3),B,_size(B,1),_size(B,2),_size(B,3),C,_size(C,1),_size(C,2),_size(C,3));
#define __update_sides3(A,nx_A,ny_A,nz_A, B,nx_B,ny_B,nz_B, C,nx_C,ny_C,nz_C)  if (istep==0){ cudaDeviceSynchronize(); } else if (istep==1){ \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_01<<<grid_mpi0,block_mpi0,0,streams[0]>>>(A##_send_01_d, A##_d, nx_A, ny_A, nz_A, nx); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_01<<<grid_mpi0,block_mpi0,0,streams[0]>>>(B##_send_01_d, B##_d, nx_B, ny_B, nz_B, nx); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_01<<<grid_mpi0,block_mpi0,0,streams[0]>>>(C##_send_01_d, C##_d, nx_C, ny_C, nz_C, nx); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_00_d, (ny_A)*(nz_A), MPI_DAT, neighbours(0,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,0) != MPI_PROC_NULL){  MPI_Irecv(B##_recv_00_d, (ny_B)*(nz_B), MPI_DAT, neighbours(0,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,0) != MPI_PROC_NULL){  MPI_Irecv(C##_recv_00_d, (ny_C)*(nz_C), MPI_DAT, neighbours(0,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,1) != MPI_PROC_NULL){  MPI_Isend(A##_send_01_d, (ny_A)*(nz_A), MPI_DAT, neighbours(0,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,1) != MPI_PROC_NULL){  MPI_Isend(B##_send_01_d, (ny_B)*(nz_B), MPI_DAT, neighbours(0,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,1) != MPI_PROC_NULL){  MPI_Isend(C##_send_01_d, (ny_C)*(nz_C), MPI_DAT, neighbours(0,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_00<<<grid_mpi0,block_mpi0,0,streams[0]>>>(A##_send_00_d, A##_d, nx_A, ny_A, nz_A, nx); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_00<<<grid_mpi0,block_mpi0,0,streams[0]>>>(B##_send_00_d, B##_d, nx_B, ny_B, nz_B, nx); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_00<<<grid_mpi0,block_mpi0,0,streams[0]>>>(C##_send_00_d, C##_d, nx_C, ny_C, nz_C, nx); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_01_d, (ny_A)*(nz_A), MPI_DAT, neighbours(0,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,1) != MPI_PROC_NULL){  MPI_Irecv(B##_recv_01_d, (ny_B)*(nz_B), MPI_DAT, neighbours(0,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,1) != MPI_PROC_NULL){  MPI_Irecv(C##_recv_01_d, (ny_C)*(nz_C), MPI_DAT, neighbours(0,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,0) != MPI_PROC_NULL){  MPI_Isend(A##_send_00_d, (ny_A)*(nz_A), MPI_DAT, neighbours(0,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,0) != MPI_PROC_NULL){  MPI_Isend(B##_send_00_d, (ny_B)*(nz_B), MPI_DAT, neighbours(0,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(0,0) != MPI_PROC_NULL){  MPI_Isend(C##_send_00_d, (ny_C)*(nz_C), MPI_DAT, neighbours(0,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      MPI_Waitall(reqnr,req,MPI_STATUSES_IGNORE);  reqnr=0;  for (int j=0; j<NREQS; j++){ req[j]=MPI_REQUEST_NULL; }; \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_00<<<grid_mpi0,block_mpi0,0,streams[0]>>>(A##_d, A##_recv_00_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_00<<<grid_mpi0,block_mpi0,0,streams[0]>>>(B##_d, B##_recv_00_d, nx_B, ny_B, nz_B); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_00<<<grid_mpi0,block_mpi0,0,streams[0]>>>(C##_d, C##_recv_00_d, nx_C, ny_C, nz_C); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_01<<<grid_mpi0,block_mpi0,0,streams[0]>>>(A##_d, A##_recv_01_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_01<<<grid_mpi0,block_mpi0,0,streams[0]>>>(B##_d, B##_recv_01_d, nx_B, ny_B, nz_B); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(0,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_01<<<grid_mpi0,block_mpi0,0,streams[0]>>>(C##_d, C##_recv_01_d, nx_C, ny_C, nz_C); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_11<<<grid_mpi1,block_mpi1,0,streams[0]>>>(A##_send_11_d, A##_d, nx_A, ny_A, nz_A, ny); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_11<<<grid_mpi1,block_mpi1,0,streams[0]>>>(B##_send_11_d, B##_d, nx_B, ny_B, nz_B, ny); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_11<<<grid_mpi1,block_mpi1,0,streams[0]>>>(C##_send_11_d, C##_d, nx_C, ny_C, nz_C, ny); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_10_d, (nx_A)*(nz_A), MPI_DAT, neighbours(1,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,0) != MPI_PROC_NULL){  MPI_Irecv(B##_recv_10_d, (nx_B)*(nz_B), MPI_DAT, neighbours(1,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,0) != MPI_PROC_NULL){  MPI_Irecv(C##_recv_10_d, (nx_C)*(nz_C), MPI_DAT, neighbours(1,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,1) != MPI_PROC_NULL){  MPI_Isend(A##_send_11_d, (nx_A)*(nz_A), MPI_DAT, neighbours(1,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,1) != MPI_PROC_NULL){  MPI_Isend(B##_send_11_d, (nx_B)*(nz_B), MPI_DAT, neighbours(1,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,1) != MPI_PROC_NULL){  MPI_Isend(C##_send_11_d, (nx_C)*(nz_C), MPI_DAT, neighbours(1,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_10<<<grid_mpi1,block_mpi1,0,streams[0]>>>(A##_send_10_d, A##_d, nx_A, ny_A, nz_A, ny); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_10<<<grid_mpi1,block_mpi1,0,streams[0]>>>(B##_send_10_d, B##_d, nx_B, ny_B, nz_B, ny); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_10<<<grid_mpi1,block_mpi1,0,streams[0]>>>(C##_send_10_d, C##_d, nx_C, ny_C, nz_C, ny); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_11_d, (nx_A)*(nz_A), MPI_DAT, neighbours(1,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,1) != MPI_PROC_NULL){  MPI_Irecv(B##_recv_11_d, (nx_B)*(nz_B), MPI_DAT, neighbours(1,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,1) != MPI_PROC_NULL){  MPI_Irecv(C##_recv_11_d, (nx_C)*(nz_C), MPI_DAT, neighbours(1,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,0) != MPI_PROC_NULL){  MPI_Isend(A##_send_10_d, (nx_A)*(nz_A), MPI_DAT, neighbours(1,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,0) != MPI_PROC_NULL){  MPI_Isend(B##_send_10_d, (nx_B)*(nz_B), MPI_DAT, neighbours(1,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(1,0) != MPI_PROC_NULL){  MPI_Isend(C##_send_10_d, (nx_C)*(nz_C), MPI_DAT, neighbours(1,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      MPI_Waitall(reqnr,req,MPI_STATUSES_IGNORE);  reqnr=0;  for (int j=0; j<NREQS; j++){ req[j]=MPI_REQUEST_NULL; }; \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_10<<<grid_mpi1,block_mpi1,0,streams[0]>>>(A##_d, A##_recv_10_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_10<<<grid_mpi1,block_mpi1,0,streams[0]>>>(B##_d, B##_recv_10_d, nx_B, ny_B, nz_B); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_10<<<grid_mpi1,block_mpi1,0,streams[0]>>>(C##_d, C##_recv_10_d, nx_C, ny_C, nz_C); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_11<<<grid_mpi1,block_mpi1,0,streams[0]>>>(A##_d, A##_recv_11_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_11<<<grid_mpi1,block_mpi1,0,streams[0]>>>(B##_d, B##_recv_11_d, nx_B, ny_B, nz_B); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(1,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_11<<<grid_mpi1,block_mpi1,0,streams[0]>>>(C##_d, C##_recv_11_d, nx_C, ny_C, nz_C); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_21<<<grid_mpi2,block_mpi2,0,streams[0]>>>(A##_send_21_d, A##_d, nx_A, ny_A, nz_A, nz); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_21<<<grid_mpi2,block_mpi2,0,streams[0]>>>(B##_send_21_d, B##_d, nx_B, ny_B, nz_B, nz); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_21<<<grid_mpi2,block_mpi2,0,streams[0]>>>(C##_send_21_d, C##_d, nx_C, ny_C, nz_C, nz); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_20_d, (nx_A)*(ny_A), MPI_DAT, neighbours(2,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,0) != MPI_PROC_NULL){  MPI_Irecv(B##_recv_20_d, (nx_B)*(ny_B), MPI_DAT, neighbours(2,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,0) != MPI_PROC_NULL){  MPI_Irecv(C##_recv_20_d, (nx_C)*(ny_C), MPI_DAT, neighbours(2,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,1) != MPI_PROC_NULL){  MPI_Isend(A##_send_21_d, (nx_A)*(ny_A), MPI_DAT, neighbours(2,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,1) != MPI_PROC_NULL){  MPI_Isend(B##_send_21_d, (nx_B)*(ny_B), MPI_DAT, neighbours(2,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,1) != MPI_PROC_NULL){  MPI_Isend(C##_send_21_d, (nx_C)*(ny_C), MPI_DAT, neighbours(2,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_20<<<grid_mpi2,block_mpi2,0,streams[0]>>>(A##_send_20_d, A##_d, nx_A, ny_A, nz_A, nz); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_20<<<grid_mpi2,block_mpi2,0,streams[0]>>>(B##_send_20_d, B##_d, nx_B, ny_B, nz_B, nz); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL)    write_to_mpi_sendbuffer_20<<<grid_mpi2,block_mpi2,0,streams[0]>>>(C##_send_20_d, C##_d, nx_C, ny_C, nz_C, nz); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL){  MPI_Irecv(A##_recv_21_d, (nx_A)*(ny_A), MPI_DAT, neighbours(2,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,1) != MPI_PROC_NULL){  MPI_Irecv(B##_recv_21_d, (nx_B)*(ny_B), MPI_DAT, neighbours(2,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,1) != MPI_PROC_NULL){  MPI_Irecv(C##_recv_21_d, (nx_C)*(ny_C), MPI_DAT, neighbours(2,1), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,0) != MPI_PROC_NULL){  MPI_Isend(A##_send_20_d, (nx_A)*(ny_A), MPI_DAT, neighbours(2,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,0) != MPI_PROC_NULL){  MPI_Isend(B##_send_20_d, (nx_B)*(ny_B), MPI_DAT, neighbours(2,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      if (neighbours(2,0) != MPI_PROC_NULL){  MPI_Isend(C##_send_20_d, (nx_C)*(ny_C), MPI_DAT, neighbours(2,0), tag, topo_comm, &(req[reqnr]));  reqnr++;  } \
                      MPI_Waitall(reqnr,req,MPI_STATUSES_IGNORE);  reqnr=0;  for (int j=0; j<NREQS; j++){ req[j]=MPI_REQUEST_NULL; }; \
                      cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_20<<<grid_mpi2,block_mpi2,0,streams[0]>>>(A##_d, A##_recv_20_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_20<<<grid_mpi2,block_mpi2,0,streams[0]>>>(B##_d, B##_recv_20_d, nx_B, ny_B, nz_B); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,0) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_20<<<grid_mpi2,block_mpi2,0,streams[0]>>>(C##_d, C##_recv_20_d, nx_C, ny_C, nz_C); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_21<<<grid_mpi2,block_mpi2,0,streams[0]>>>(A##_d, A##_recv_21_d, nx_A, ny_A, nz_A); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_21<<<grid_mpi2,block_mpi2,0,streams[0]>>>(B##_d, B##_recv_21_d, nx_B, ny_B, nz_B); cudaStreamSynchronize(streams[0]); \
                      if (neighbours(2,1) != MPI_PROC_NULL)   read_from_mpi_recvbuffer_21<<<grid_mpi2,block_mpi2,0,streams[0]>>>(C##_d, C##_recv_21_d, nx_C, ny_C, nz_C); cudaStreamSynchronize(streams[0]); }

#define init_sides(A)  __init_sides(A,_size(A,1),_size(A,2),_size(A,3));
#define __init_sides(A,nx_A,ny_A,nz_A)   zeros_d(A##_send_00 ,ny_A,nz_A); zeros_d(A##_send_01 ,ny_A,nz_A); zeros_d(A##_send_10 ,nx_A,nz_A); zeros_d(A##_send_11 ,nx_A,nz_A); zeros_d(A##_send_20 ,nx_A,ny_A); zeros_d(A##_send_21 ,nx_A,ny_A); zeros_d(A##_recv_00 ,ny_A,nz_A); zeros_d(A##_recv_01 ,ny_A,nz_A); zeros_d(A##_recv_10 ,nx_A,nz_A); zeros_d(A##_recv_11 ,nx_A,nz_A); zeros_d(A##_recv_20 ,nx_A,ny_A); zeros_d(A##_recv_21 ,nx_A,ny_A);
#define free_sides(A)  cudaFree(A##_send_00_d); cudaFree(A##_send_01_d); cudaFree(A##_send_10_d); cudaFree(A##_send_11_d); cudaFree(A##_send_20_d); cudaFree(A##_send_21_d); cudaFree(A##_recv_00_d); cudaFree(A##_recv_01_d); cudaFree(A##_recv_10_d); cudaFree(A##_recv_11_d); cudaFree(A##_recv_20_d); cudaFree(A##_recv_21_d);
#else
#define update_sides(A)                
#define update_sides3(A,B,C)
#define init_sides(A)
#define free_sides(A)
#define MPI_Finalize()
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Staggering of timestep arrays (halo)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__global__ void stag_bcXmax(DAT*A,DAT*A2, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific
  def_sizes(A  ,nx_A,ny_A,nz_A);
  def_sizes(A2 ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (ix==0           && iy<ny_A && iz<nz_A)  select(A2 ,0          ,iy,iz) = _max(select(A ,0         ,iy,iz) , select(A ,1           ,iy,iz));
  if (ix==(_end(A2,1)) && iy<ny_A && iz<nz_A)  select(A2 ,(_end(A2,1)),iy,iz) = _max(select(A ,(_end(A,1)),iy,iz) , select(A ,(_end(A,1)-1),iy,iz));
}
__global__ void stag_bcYmax(DAT*A,DAT*A2, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific
  def_sizes(A  ,nx_A,ny_A,nz_A);
  def_sizes(A2 ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (ix<nx_A && iy==0           && iz<nz_A)  select(A2 ,ix,0          ,iz) = _max(select(A ,ix,0         ,iz) , select(A ,ix,1           ,iz));
  if (ix<nx_A && iy==(_end(A2,2)) && iz<nz_A)  select(A2 ,ix,(_end(A2,2)),iz) = _max(select(A ,ix,(_end(A,2)),iz) , select(A ,ix,(_end(A,2)-1),iz));
}
__global__ void stag_bcZmax(DAT*A,DAT*A2, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific
  def_sizes(A  ,nx_A,ny_A,nz_A);
  def_sizes(A2 ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (ix<nx_A && iy<ny_A && iz==0          )  select(A2 ,ix,iy,0          ) = _max(select(A ,ix,iy,0         ) , select(A ,ix,iy,1           ));
  if (ix<nx_A && iy<ny_A && iz==(_end(A2,3)))  select(A2 ,ix,iy,(_end(A2,3))) = _max(select(A ,ix,iy,(_end(A,3))) , select(A ,ix,iy,(_end(A,3)-1)));
}
__global__ void stag_Amax(int istep, DAT*A,DAT*A2, const int nx_A,const int ny_A,const int nz_A){
  // CUDA specific
  def_sizes(A  ,nx_A,ny_A,nz_A);
  def_sizes(A2 ,nx_A,ny_A,nz_A);

  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
  int ixm = ix-1;                                // shifted ix for computations with inner points of an array
  int ixp = ix+1;                                // shifted ix for computations with inner points of an array
  int iym = iy-1;                                // shifted ix for computations with inner points of an array
  int iyp = iy+1;                                // shifted ix for computations with inner points of an array
  int izm = iz-1;                                // shifted ix for computations with inner points of an array
  int izp = iz+1;                                // shifted ix for computations with inner points of an array

  CommOverlap();

  DAT xdir,ydir,zdir;
  if (ix>0 && ix<(nx_A-1) && iy<ny_A && iz<nz_A)  xdir = _max(select(A ,ix,iy,iz) , _max(select(A ,ixm,iy,iz) , select(A ,ixp,iy,iz)));
  if (ix<nx_A && iy>0 && iy<(ny_A-1) && iz<nz_A)  ydir = _max(select(A ,ix,iy,iz) , _max(select(A ,ix,iym,iz) , select(A ,ix,iyp,iz)));
  if (ix<nx_A && iy<ny_A && iz>0 && iz<(nz_A-1))  zdir = _max(select(A ,ix,iy,iz) , _max(select(A ,ix,iy,izm) , select(A ,ix,iy,izp)));

  if (ix>0 && ix<(nx_A-1) && iy<ny_A && iz<nz_A)  select(A2 ,ix,iy,iz) = _max(select(A  ,ix,iy,iz) , xdir);
  if (ix<nx_A && iy>0 && iy<(ny_A-1) && iz<nz_A)  select(A2 ,ix,iy,iz) = _max(select(A2 ,ix,iy,iz) , ydir);
  if (ix<nx_A && iy<ny_A && iz>0 && iz<(nz_A-1))  select(A2 ,ix,iy,iz) = _max(select(A2 ,ix,iy,iz) , zdir);
}
#define __stag_bcXYZmax(A,A2)  stag_bcXmax<<<grid, block>>>(A##_d,A2##_d, _size(A,1),_size(A,2),_size(A,3)); \
                               stag_bcYmax<<<grid, block>>>(A##_d,A2##_d, _size(A,1),_size(A,2),_size(A,3)); \
                               stag_bcZmax<<<grid, block>>>(A##_d,A2##_d, _size(A,1),_size(A,2),_size(A,3));

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Advection Upwind conservative
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CUDA interpolation kernel (for VTK visualization)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CUDA user defined interp macros
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


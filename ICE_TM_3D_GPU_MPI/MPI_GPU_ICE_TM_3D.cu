/* FastICE: Thermomechanical ice deformation models 3D GPU CUDA-aware MPI

Copyright (C) 2019  Ludovic Raess, Aleksandar Licul et al.
 
FastICE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
FastICE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with FastICE. If not, see <http://www.gnu.org/licenses/>. */

// -------------------------------------------------------------------------
// Compile as: nvcc MPI_GPU_ICE_TM_3D.cu -arch=sm_XX --ptxas-options=-v --compiler-bindir mpic++ -DUSE_MPI
// arch=sm_XX: TITAN Black=sm_35, TITAN X=sm_52, TITAN Xp=sm_61, Tesla V100=sm_70
// -------------------------------------------------------------------------

#define GPU_ID 0
// #define USE_SINGLE_PRECISION      /* Comment this line using "!" if you want to use double precision.  */

#ifdef USE_SINGLE_PRECISION
#define DAT     float
#define MPI_DAT MPI_REAL
#define PRECIS  4
#else
#define DAT     double
#define MPI_DAT MPI_DOUBLE_PRECISION
#define PRECIS  8
#endif
////////// ========== Simulation Initialisation ========== //////////
#define NDIMS 3
#define BLOCKS_X     32
#define BLOCKS_Y     16
#define BLOCKS_Z     2
#define GRID_X       16
#define GRID_Y       16
#define GRID_Z       64
// MPI dims initialization:
#define DIMS_X       DIMX
#define DIMS_Y       DIMY
#define DIMS_Z       DIMZ
// maximum overlap in x, y, z direction. x : Vx is nx+1, so it is 1; y: Vy is ny+1, so it is 1; z: Vz is nz+1, so it is 1.
#define MAX_OVERLENGTH_X 1
#define MAX_OVERLENGTH_Y 1
#define MAX_OVERLENGTH_Z 1
#define BOUNDARY_WIDTH_X 8
#define BOUNDARY_WIDTH_Y 8
#define BOUNDARY_WIDTH_Z 8

#define PI 3.14159265358979323846
#ifdef USE_SINGLE_PRECISION
const DAT eps_stop  = 1e-10;
#else
const DAT eps_stop  = 1e-16;
#endif
const DAT nIO      = 12.0;
// Physics
const DAT alph_deg = 10; 
const DAT npow     = 3.0;   
const DAT F        = 2.797540079392592e-08;
const DAT Lz       = 3.026718296079047e+05;  
const DAT theta    = 9.146695619007199;      
const DAT gamma_b  = 0.0;
const DAT dt       = 5e5;
const DAT t_max    = 500*1e6;
// const DAT t_max    = 10*500*1e6;
const DAT aspect_x = 10.0;
const DAT aspect_y = 4.0;
const DAT eta0     = 1.5e7;
// add melting
const DAT MELT     = 0.0;      // set to 1.0 to include temperature buffering due to melting
const DAT Tm       = -0.2;     // defined metling temperature (°C)
// Numerics
const int nx       = GRID_X*BLOCKS_X - MAX_OVERLENGTH_X; 
const int ny       = GRID_Y*BLOCKS_Y - MAX_OVERLENGTH_Y; 
const int nz       = GRID_Z*BLOCKS_Z - MAX_OVERLENGTH_Z;
const DAT etani    = 1e4;
const DAT damp     = 2.0;
const DAT relV     = 1.0/1.0; 
const DAT relP     = 1.0/4.0; 
const DAT eta_b    = 1.0/4.0;
const DAT rele     = 1e-1;
const int niter    = 5e6;
const int nout     = 500;//100
const int nsave    = 5;//10;
// Preprocessing
const int nt       = 1;//10;//round(t_max/dt);
const DAT Lx       = Lz*aspect_x;
const DAT Ly       = Lz*aspect_y;
const DAT alph     = alph_deg*PI/(DAT)180.0;
const DAT mpow     = -(1.0-1.0/npow)/2.0;
const DAT T_sc     = 28.7536;  // npow*R*T0^2/Q0;
const DAT T0_K     = 263-273;  // T0-T_K 
/// Params to be saved for evol plot ///
#define NB_PARAMS      7
#define it_EVOL      0
#define iter_EVOL    1
#define time_EVOL    2
#define dVxdt_NORM   3
#define dVydt_NORM   4
#define dVzdt_NORM   5
#define divV_NORM    6

size_t Nix, Niy, Niz, N;
DAT    dx,  dy,  dz;

#include "cuda_scientific_3D_MPI.h"

////////// ========================================  Physics  __CUDAkernels  ======================================== //////////
__global__ void initialize(DAT* xc,DAT* xn,DAT* yc,DAT* yn,DAT* zc,DAT* zn,DAT* etan,DAT* T, int* coords, const DAT etani, const DAT gamma_b, const DAT dx, const DAT dy,const DAT dz,const int nx, const int ny,const int nz,const DAT Lz){
    def_sizes(xc   , nx  , 1   , 1   );
    def_sizes(xn   , nx+1, 1   , 1   );
    def_sizes(yc   , 1   , ny  , 1   );
    def_sizes(yn   , 1   , ny+1, 1   );
    def_sizes(zc   , 1   , 1   , nz  );
    def_sizes(zn   , 1   , 1   , nz+1);
    def_sizes(T    , nx  , ny  , nz  );
    def_sizes(etan , nx  , ny  , nz  );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    
    DAT xcoord = ((DAT)(coords[0]*(nx-2)) + (DAT)ix)*dx;
    DAT ycoord = ((DAT)(coords[1]*(ny-2)) + (DAT)iy)*dy;
    DAT zcoord = ((DAT)(coords[2]*(nz-2)) + (DAT)iz)*dz;

    if (participate_a(xc))    all(xc)    =  xcoord;
    if (participate_a(xn))    all(xn)    =  xcoord - (DAT)0.5*dx;
    if (participate_a(yc))    all(yc)    =  ycoord;
    if (participate_a(yn))    all(yn)    =  ycoord - (DAT)0.5*dy;
    if (participate_a(zc))    all(zc)    =  zcoord;
    if (participate_a(zn))    all(zn)    =  zcoord - (DAT)0.5*dz;
    if (participate_a(T))     all(T)     =  gamma_b*(Lz-zcoord);
    if (participate_a(etan))  all(etan)  =  etani; 
}

__global__ void timesteps(DAT* etan,DAT* dtVx,DAT* dtVy,DAT* dtVz,DAT* dtP,const DAT relV,const DAT relP,const DAT eta_b,const DAT nxyz,const DAT dxyz2,const int nx, const int ny, const int nz){
    def_sizes(dtVx , nx-1, ny-2, nz-2);
    def_sizes(dtVy , nx-2, ny-1, nz-2);
    def_sizes(dtVz , nx-2, ny-2, nz-1);
    def_sizes(dtP  , nx  , ny  , nz  );
    def_sizes(etan , nx  , ny  , nz  );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_a(dtP))    all(dtP)   = relP*(DAT)6.1/nxyz*all(etan)*((DAT)1.0+eta_b);
    if (participate_a(dtVx))   all(dtVx)  = relV*dxyz2/av_xi(etan)/((DAT)1.0+eta_b)/(DAT)6.1;
    if (participate_a(dtVy))   all(dtVy)  = relV*dxyz2/av_yi(etan)/((DAT)1.0+eta_b)/(DAT)6.1;
    if (participate_a(dtVz))   all(dtVz)  = relV*dxyz2/av_zi(etan)/((DAT)1.0+eta_b)/(DAT)6.1;
}

__global__ void strain_rates_P(DAT* P,DAT* Vx,DAT* Vy, DAT* Vz,DAT* divV,DAT* Exx,DAT* Eyy,DAT* Ezz,DAT* Exy,DAT* Exz,DAT* Eyz,DAT* dtP, const DAT pxyz, const DAT DX, const DAT DY, const DAT DZ,const int nx, const int ny, const int nz){
    def_sizes(P    , nx  , ny  , nz  );
    def_sizes(Vx   , nx+1, ny  , nz  );
    def_sizes(Vy   , nx  , ny+1, nz  ); 
    def_sizes(Vz   , nx  , ny  , nz+1);
    def_sizes(divV , nx  , ny  , nz  );
    def_sizes(Exx  , nx  , ny  , nz  );
    def_sizes(Eyy  , nx  , ny  , nz  );
    def_sizes(Ezz  , nx  , ny  , nz  );
    def_sizes(Exy  , nx-1, ny-1, nz-2);
    def_sizes(Exz  , nx-1, ny-2, nz-1);
    def_sizes(Eyz  , nx-2, ny-1, nz-1);
    def_sizes(dtP  , nx  , ny  , nz  );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_a(divV))  all(divV) = DX*d_xa(Vx) + DY*d_ya(Vy) + DZ*d_za(Vz);
    if (participate_a(Exx))   all(Exx)  = DX*d_xa(Vx) - pxyz*(DX*d_xa(Vx) + DY*d_ya(Vy) + DZ*d_za(Vz));
    if (participate_a(Eyy))   all(Eyy)  = DY*d_ya(Vy) - pxyz*(DX*d_xa(Vx) + DY*d_ya(Vy) + DZ*d_za(Vz));
    if (participate_a(Ezz))   all(Ezz)  = DZ*d_za(Vz) - pxyz*(DX*d_xa(Vx) + DY*d_ya(Vy) + DZ*d_za(Vz));
    if (participate_a(Exy))   all(Exy)  = (DAT)0.5*( DY*d_yi(Vx) + DX*d_xi(Vy) );
    if (participate_a(Exz))   all(Exz)  = (DAT)0.5*( DZ*d_zi(Vx) + DX*d_xi(Vz) );
    if (participate_a(Eyz))   all(Eyz)  = (DAT)0.5*( DZ*d_zi(Vy) + DY*d_yi(Vz) );
    if (participate_a(P))     all(P)    = all(P)  -  all(divV)*all(dtP); 
}

__global__ void stresses(DAT* T, DAT* qxT,DAT* qyT,DAT* qzT, DAT* etan,DAT* eta_xz,DAT* eta_yz,DAT* divV,DAT* Exx,DAT* Eyy,DAT* Ezz,DAT* Exy,DAT* Exz,DAT* Eyz,DAT* txx,DAT* tyy,DAT* tzz,DAT* txy,DAT* txz,DAT* tyz,DAT* Exyn,DAT* Exzn,DAT* Eyzn, const DAT DX, const DAT DY, const DAT DZ, const DAT eta_b,const int nx, const int ny, const int nz){
    def_sizes(T      , nx  , ny  , nz  );
    def_sizes(qxT    , nx+1, ny  , nz  );
    def_sizes(qyT    , nx  , ny+1, nz  );
    def_sizes(qzT    , nx  , ny  , nz+1);
    def_sizes(Exx    , nx  , ny  , nz  );
    def_sizes(Eyy    , nx  , ny  , nz  );
    def_sizes(Ezz    , nx  , ny  , nz  );
    def_sizes(Exy    , nx-1, ny-1, nz-2);
    def_sizes(Exz    , nx-1, ny-2, nz-1);
    def_sizes(Eyz    , nx-2, ny-1, nz-1);
    def_sizes(txx    , nx  , ny  , nz  );
    def_sizes(tyy    , nx  , ny  , nz  );
    def_sizes(tzz    , nx  , ny  , nz  );
    def_sizes(txy    , nx-1, ny-1, nz-2);
    def_sizes(txz    , nx-1, ny-2, nz-1);
    def_sizes(tyz    , nx-2, ny-1, nz-1);
    def_sizes(etan   , nx  , ny  , nz  );
    def_sizes(divV   , nx  , ny  , nz  );
    def_sizes(eta_xz , nx-1, ny-2, nz-1);
    def_sizes(eta_yz , nx-2, ny-1, nz-1);
    def_sizes(Exyn   , nx  , ny  , nz  );
    def_sizes(Exzn   , nx  , ny  , nz  );
    def_sizes(Eyzn   , nx  , ny  , nz  );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_a(txx))   all(txx)  = (DAT)2.0*all(etan)*(all(Exx) + eta_b*all(divV));
    if (participate_a(tyy))   all(tyy)  = (DAT)2.0*all(etan)*(all(Eyy) + eta_b*all(divV));
    if (participate_a(txx))   all(tzz)  = (DAT)2.0*all(etan)*(all(Ezz) + eta_b*all(divV));

    if (participate_a(txy))   all(txy)  = (DAT)2.0*av_xyi(etan)*all(Exy);
    if (participate_a(txz))   all(txz)  = (DAT)2.0*av_xzi(etan)*all(Exz);
    if (participate_a(tyz))   all(tyz)  = (DAT)2.0*av_yzi(etan)*all(Eyz);

    if (participate_a(eta_xz))  all(eta_xz)  = av_xzi(etan);
    if (participate_a(eta_yz))  all(eta_yz)  = av_yzi(etan);

    if (participate_i(Exyn))  inn(Exyn)  = av_xya(Exy);
    if (participate_i(Exzn))  inn(Exzn)  = av_xza(Exz);
    if (participate_i(Eyzn))  inn(Eyzn)  = av_yza(Eyz);

    if (participate_ix(qxT))   in_x(qxT)  = -DX*d_xa(T);
    if (participate_iy(qyT))   in_y(qyT)  = -DY*d_ya(T);
    if (participate_iz(qzT))   in_z(qzT)  = -DZ*d_za(T);
}


__global__ void BC_En(DAT* Exyn, DAT* Exzn, DAT* Eyzn, int* coords, const int nx, const int ny, const int nz){
    def_sizes(Exyn , nx, ny, nz);
    def_sizes(Exzn , nx, ny, nz);
    def_sizes(Eyzn , nx, ny, nz);

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (coords[2]==0){ if (iz==0 && ix<_size(Exyn,1) && iy<_size(Exyn,2)){ Exyn[ix + iy*_size(Exyn,1)] =  Exyn[ix + iy*_size(Exyn,1) + _size(Exyn,1)*_size(Exyn,2)]; } }
    if (coords[2]==0){ if (iz==0 && ix<_size(Exzn,1) && iy<_size(Exzn,2)){ Exzn[ix + iy*_size(Exzn,1)] =  Exzn[ix + iy*_size(Exzn,1) + _size(Exzn,1)*_size(Exzn,2)]; } }
    if (coords[2]==0){ if (iz==0 && ix<_size(Eyzn,1) && iy<_size(Eyzn,2)){ Eyzn[ix + iy*_size(Eyzn,1)] =  Eyzn[ix + iy*_size(Eyzn,1) + _size(Eyzn,1)*_size(Eyzn,2)]; } }
}
                                                                                        
__global__ void ResV_etait(DAT* etan,DAT* T,DAT* T_old,DAT* Tres, DAT* qxT,DAT* qyT,DAT* qzT,DAT* dVxdt,DAT* dVydt,DAT* dVzdt,DAT* P,DAT* EII2, DAT* etait,DAT* Exx,DAT* Eyy,DAT* Ezz, DAT* Exyn, DAT* Exzn, DAT* Eyzn, DAT* txx,DAT* tyy,DAT* tzz,DAT* txy,DAT* txz,DAT* tyz,const DAT MELT,const DAT Tm,const DAT T_sc,const DAT T0_K,const DAT F,const DAT DT,const DAT theta,const DAT mpow,const DAT alph, const DAT dampX, const DAT dampY, const DAT dampZ, const DAT DX, const DAT DY, const DAT DZ, const int nx, const int ny, const int nz){  
    def_sizes(Exx   , nx  , ny  , nz  );
    def_sizes(Eyy   , nx  , ny  , nz  );
    def_sizes(Ezz   , nx  , ny  , nz  );
    def_sizes(Exyn  , nx  , ny  , nz  );
    def_sizes(Exzn  , nx  , ny  , nz  );
    def_sizes(Eyzn  , nx  , ny  , nz  );
    def_sizes(txx   , nx  , ny  , nz  );
    def_sizes(tyy   , nx  , ny  , nz  );
    def_sizes(tzz   , nx  , ny  , nz  );
    def_sizes(txy   , nx-1, ny-1, nz-2);
    def_sizes(txz   , nx-1, ny-2, nz-1);
    def_sizes(tyz   , nx-2, ny-1, nz-1);
    def_sizes(P     , nx  , ny  , nz  );
    def_sizes(EII2  , nx  , ny  , nz  );
    def_sizes(etan  , nx  , ny  , nz  );
    def_sizes(etait , nx  , ny  , nz  );
    def_sizes(dVxdt , nx-1, ny-2, nz-2);
    def_sizes(dVydt , nx-2, ny-1, nz-2);
    def_sizes(dVzdt , nx-2, ny-2, nz-1);
    def_sizes(T     , nx  , ny  , nz  );
    def_sizes(T_old , nx  , ny  , nz  );
    def_sizes(Tres  , nx  , ny  , nz  );
    def_sizes(qxT   , nx+1, ny  , nz  );
    def_sizes(qyT   , nx  , ny+1, nz  );
    def_sizes(qzT   , nx  , ny  , nz+1);

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_a(dVxdt))  all(dVxdt) = all(dVxdt)*dampX - DX*d_xi(P)   + DX*d_xi(txx) + DY*d_ya(txy) + DZ*d_za(txz) + F; 
    if (participate_a(dVydt))  all(dVydt) = all(dVydt)*dampY + DX*d_xa(txy) - DY*d_yi(P)   + DY*d_yi(tyy) + DZ*d_za(tyz);
    if (participate_a(dVzdt))  all(dVzdt) = all(dVzdt)*dampZ + DX*d_xa(txz) + DY*d_ya(tyz) - DZ*d_zi(P)   + DZ*d_zi(tzz) - F*cos(alph)/sin(alph);

    if (participate_a(EII2))   all(EII2)  = (DAT)0.5*(all(Exx)*all(Exx) + all(Eyy)*all(Eyy) + all(Ezz)*all(Ezz)) + all(Exyn)*all(Exyn) + all(Exzn)*all(Exzn) + all(Eyzn)*all(Eyzn);
    // Added melt water generation to balance shear heating
    if (participate_a(Tres))   all(Tres)  = -DT*(all(T)-all(T_old)) - (DX*d_xa(qxT) + DY*d_ya(qyT) + DZ*d_za(qzT)) + (DAT)4.0*all(etan)*all(EII2)*( (DAT)1.0 - MELT*((DAT)1.0 - tanh(-(DAT)2.0*(all(T)*T_sc+(T0_K)-Tm))) );
    
    if (participate_a(etait))  all(etait) = pow(all(EII2),mpow)*exp(-all(T)/((DAT)1.0 + all(T)/theta));
    #undef MELT
}
    
__global__ void update_V_eta(int istep, DAT* T,DAT* Tres,DAT* Vx,DAT* Vy,DAT* Vz,DAT* dVxdt,DAT* dVydt,DAT* dVzdt,DAT* dtVx,DAT* dtVy,DAT* dtVz,DAT* etait,DAT* etan,const DAT dtau,const DAT eta0,const DAT rele,const int nx,const int ny,const int nz,const int it){
    def_sizes(Vx    , nx+1, ny  , nz  );
    def_sizes(Vy    , nx  , ny+1, nz  );
    def_sizes(Vz    , nx  , ny  , nz+1);
    def_sizes(dVxdt , nx-1, ny-2, nz-2);
    def_sizes(dVydt , nx-2, ny-1, nz-2);
    def_sizes(dVzdt , nx-2, ny-2, nz-1);
    def_sizes(dtVx  , nx-1, ny-2, nz-2);
    def_sizes(dtVy  , nx-2, ny-1, nz-2);
    def_sizes(dtVz  , nx-2, ny-2, nz-1);
    def_sizes(etan  , nx  , ny  , nz  );
    def_sizes(etait , nx  , ny  , nz  );
    def_sizes(T     , nx  , ny  , nz  );
    def_sizes(Tres  , nx  , ny  , nz  );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    CommOverlap();

    if (participate_i(Vx))  inn(Vx) = inn(Vx) +  all(dVxdt)*all(dtVx); 
    if (participate_i(Vy))  inn(Vy) = inn(Vy) +  all(dVydt)*all(dtVy);
    if (participate_i(Vz))  inn(Vz) = inn(Vz) +  all(dVzdt)*all(dtVz);
    if (it>0){ if (participate_a(T)){ all(T)  = all(T)  +  dtau*all(Tres); } }

    // if (participate_a(etan))  all(etan) = min(exp(rele*log(all(etait)) + ((DAT)1.0 - rele)*log(all(etan)) ),eta0); 
    if (participate_a(etait))  all(etait) = (DAT)1.0/( (DAT)1.0/all(etait) + (DAT)1.0/eta0 );
    if (participate_a(etan))   all(etan)  = exp(rele*log(all(etait)) + ((DAT)1.0 - rele)*log(all(etan))); 
}

__global__ void BC_V_1(DAT* Vx,DAT* Vy,DAT* Vz, int* coords, const int nx, const int ny, const int nz){
    def_sizes(Vx , nx+1, ny  , nz  );
    def_sizes(Vy , nx  , ny+1, nz  );
    def_sizes(Vz , nx  , ny  , nz+1);

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (coords[0]==0){ if (ix==0 && iy>0 && iy< (_size(Vx,2)-1) && iz>0 && iz< (_size(Vx,3)-1)){ Vx[iy*_size(Vx,1) + iz*_size(Vx,1)*_size(Vx,2)] = -Vx[1 + iy*_size(Vx,1) + iz*_size(Vx,1)*_size(Vx,2)]; } }
    if (coords[0]==0){ if (ix==0 && iy>0 && iy< (_size(Vy,2)-1) && iz>0 && iz< (_size(Vy,3)-1)){ Vy[iy*_size(Vy,1) + iz*_size(Vy,1)*_size(Vy,2)] = (DAT) 0.0; } }
    if (coords[0]==0){ if (ix==0 && iy>0 && iy< (_size(Vz,2)-1) && iz>0 && iz< (_size(Vz,3)-1)){ Vz[iy*_size(Vz,1) + iz*_size(Vz,1)*_size(Vz,2)] =  Vz[1 + iy*_size(Vz,1) + iz*_size(Vz,1)*_size(Vz,2)]; } }
}

__global__ void BC_V_2(DAT* Vx,DAT* Vy,DAT* Vz, int* coords, int* dims, const int nx, const int ny, const int nz){
    def_sizes(Vx , nx+1, ny  , nz  );
    def_sizes(Vy , nx  , ny+1, nz  );
    def_sizes(Vz , nx  , ny  , nz+1);

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (coords[0]==(dims[0]-1)){ if (ix==(_size(Vx,1)-1) && iy>0 && iy< (_size(Vx,2)-1) && iz>0 && iz< (_size(Vx,3)-1)){ Vx[(_size(Vx,1)-1) + iy*_size(Vx,1) + iz*_size(Vx,1)*_size(Vx,2)] = -Vx[(_size(Vx,1)-2) + iy*_size(Vx,1) + iz*_size(Vx,1)*_size(Vx,2)]; } }
    if (coords[0]==(dims[0]-1)){ if (ix==(_size(Vy,1)-1) && iy>0 && iy< (_size(Vy,2)-1) && iz>0 && iz< (_size(Vy,3)-1)){ Vy[(_size(Vy,1)-1) + iy*_size(Vy,1) + iz*_size(Vy,1)*_size(Vy,2)] = (DAT) 0.0; } }
    if (coords[0]==(dims[0]-1)){ if (ix==(_size(Vz,1)-1) && iy>0 && iy< (_size(Vz,2)-1) && iz>0 && iz< (_size(Vz,3)-1)){ Vz[(_size(Vz,1)-1) + iy*_size(Vz,1) + iz*_size(Vz,1)*_size(Vz,2)] = Vz[(_size(Vz,1)-2) + iy*_size(Vz,1) + iz*_size(Vz,1)*_size(Vz,2)]; } } 
}

__global__ void BC_V_3(DAT* Vx,DAT* Vy,DAT* Vz, int* coords, const int nx, const int ny, const int nz){
    def_sizes(Vx , nx+1, ny  , nz  );
    def_sizes(Vy , nx  , ny+1, nz  );
    def_sizes(Vz , nx  , ny  , nz+1);

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (coords[1]==0){ if (iy==0 && ix>0 && ix< (_size(Vx,1)-1) && iz>0 && iz< (_size(Vx,3)-1)){ Vx[ix + iz*_size(Vx,1)*_size(Vx,2)] = (DAT) 0.0; } }
    if (coords[1]==0){ if (iy==0 && ix>0 && ix< (_size(Vy,1)-1) && iz>0 && iz< (_size(Vy,3)-1)){ Vy[ix + iz*_size(Vy,1)*_size(Vy,2)] = -Vy[ix + _size(Vy,1) + iz*_size(Vy,1)*_size(Vy,2)]; } }
    if (coords[1]==0){ if (iy==0 && ix>0 && ix< (_size(Vz,1)-1) && iz>0 && iz< (_size(Vz,3)-1)){ Vz[ix + iz*_size(Vz,1)*_size(Vz,2)] =  Vz[ix + _size(Vz,1) + iz*_size(Vz,1)*_size(Vz,2)]; } }
}

__global__ void BC_V_4(DAT* Vx,DAT* Vy,DAT* Vz, int* coords, int* dims, const int nx, const int ny, const int nz){
    def_sizes(Vx , nx+1, ny  , nz  );
    def_sizes(Vy , nx  , ny+1, nz  );
    def_sizes(Vz , nx  , ny  , nz+1);

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (coords[1]==(dims[1]-1)){ if (iy==(_size(Vx,2)-1) && ix>0 && ix< (_size(Vx,1)-1) && iz>0 && iz< (_size(Vx,3)-1)){ Vx[ix + (_size(Vx,2)-1)*_size(Vx,1) + iz*_size(Vx,1)*_size(Vx,2)] = (DAT) 0.0; } }
    if (coords[1]==(dims[1]-1)){ if (iy==(_size(Vy,2)-1) && ix>0 && ix< (_size(Vy,1)-1) && iz>0 && iz< (_size(Vy,3)-1)){ Vy[ix + (_size(Vy,2)-1)*_size(Vy,1) + iz*_size(Vy,1)*_size(Vy,2)] = -Vy[ix + (_size(Vy,2)-2)*_size(Vy,1) + iz*_size(Vy,1)*_size(Vy,2)]; } }
    if (coords[1]==(dims[1]-1)){ if (iy==(_size(Vz,2)-1) && ix>0 && ix< (_size(Vz,1)-1) && iz>0 && iz< (_size(Vz,3)-1)){ Vz[ix + (_size(Vz,2)-1)*_size(Vz,1) + iz*_size(Vz,1)*_size(Vz,2)] =  Vz[ix + (_size(Vz,2)-2)*_size(Vz,1) + iz*_size(Vz,1)*_size(Vz,2)]; } }
}

__global__ void BC_V_5(DAT* qzT,DAT* Vx,DAT* Vy,DAT* Vz,const DAT gamma_b, int* coords, const int nx, const int ny, const int nz){
    def_sizes(Vx  , nx+1, ny  , nz  );
    def_sizes(Vy  , nx  , ny+1, nz  );
    def_sizes(Vz  , nx  , ny  , nz+1);
    def_sizes(qzT , nx  , ny  , nz+1);

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (coords[2]==0){ if (iz==0 && ix>0 && ix<(_size(Vx,1)-1) && iy>0 && iy< (_size(Vx,2)-1)){ Vx[ix + iy*_size(Vx,1)] = (DAT) 0.0; } }
    if (coords[2]==0){ if (iz==0 && ix>0 && ix<(_size(Vy,1)-1) && iy>0 && iy< (_size(Vy,2)-1)){ Vy[ix + iy*_size(Vy,1)] = (DAT) 0.0; } }
    if (coords[2]==0){ if (iz==0 && ix<(_size(Vz ,1)) && iy< (_size(Vz ,2))){  Vz[ix + iy*_size(Vz,1)]  = -Vz[ix + iy*_size(Vz,1) + _size(Vz,1)*_size(Vz,2)]; } }
    if (coords[2]==0){ if (iz==0 && ix<(_size(qzT,1)) && iy< (_size(qzT,2))){ qzT[ix + iy*_size(qzT,1)] = gamma_b; } }
}

__global__ void BC_V_6(DAT* T,DAT* Vx,DAT* Vy,DAT* Vz,DAT* P,DAT* divV,DAT* txz,DAT* tyz,DAT* etan,DAT* eta_xz,DAT* eta_yz, int* coords, int* dims, const DAT p_dz2,const DAT p_dz3,const DAT p_dzdx,const DAT p_dzdy,const DAT p_dzeta,const int nx, const int ny, const int nz){
    def_sizes(Vx     , nx+1, ny  , nz  );
    def_sizes(Vy     , nx  , ny+1, nz  );
    def_sizes(Vz     , nx  , ny  , nz+1);
    def_sizes(P      , nx  , ny  , nz  );
    def_sizes(divV   , nx  , ny  , nz  );
    def_sizes(etan   , nx  , ny  , nz  );
    def_sizes(txz    , nx-1, ny-2, nz-1);
    def_sizes(tyz    , nx-2, ny-1, nz-1);
    def_sizes(eta_xz , nx-1, ny-2, nz-1);
    def_sizes(eta_yz , nx-2, ny-1, nz-1);
    def_sizes(T      , nx  , ny  , nz  );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (coords[2]==(dims[2]-1)){ if (iz==(_size(Vz,3)-1) && ix< _size(Vz,1) && iy< _size(Vz,2)){                         Vz[ix + iy*_size(Vz,1) + (_size(Vz,3)-1)*_size(Vz,1)*_size(Vz,2)] = Vz[ix + iy*_size(Vz,1) + (_size(Vz,3)-2)*_size(Vz,1)*_size(Vz,2)] + p_dz2*(P[ix + iy*_size(P,1) + (_size(P,3)-1)*_size(P,1)*_size(P,2)]/etan[ix + iy*_size(etan,1) + (_size(etan,3)-1)*_size(etan,1)*_size(etan,2)]) + p_dzeta*divV[ix + iy*_size(divV,1) + (_size(divV,3)-1)*_size(divV,1)*_size(divV,2)]; } }
    if (coords[2]==(dims[2]-1)){ if (iz==(_size(Vx,3)-1) && ix>0 && ix< (_size(Vx,1)-1) && iy>0 && iy< (_size(Vx,2)-1)){ Vx[ix + iy*_size(Vx,1) + (_size(Vx,3)-1)*_size(Vx,1)*_size(Vx,2)] = Vx[ix + iy*_size(Vx,1) + (_size(Vx,3)-2)*_size(Vx,1)*_size(Vx,2)] - p_dzdx*(Vz[ix + iy*_size(Vz,1) + (_size(Vz,3)-2)*_size(Vz,1)*_size(Vz,2)] - Vz[ix-1 + iy*_size(Vz,1) + (_size(Vz,3)-2)*_size(Vz,1)*_size(Vz,2)])   + p_dz3*txz[ix-1 + (iy-1)*_size(txz,1) + (_size(txz,3)-2)*_size(txz,1)*_size(txz,2)]/eta_xz[ix-1 + (iy-1)*_size(eta_xz,1) + (_size(eta_xz,3)-1)*_size(eta_xz,1)*_size(eta_xz,2)]; } }
    if (coords[2]==(dims[2]-1)){ if (iz==(_size(Vy,3)-1) && ix>0 && ix< (_size(Vy,1)-1) && iy>0 && iy< (_size(Vy,2)-1)){ Vy[ix + iy*_size(Vy,1) + (_size(Vy,3)-1)*_size(Vy,1)*_size(Vy,2)] = Vy[ix + iy*_size(Vy,1) + (_size(Vy,3)-2)*_size(Vy,1)*_size(Vy,2)] - p_dzdy*(Vz[ix + iy*_size(Vz,1) + (_size(Vz,3)-2)*_size(Vz,1)*_size(Vz,2)] - Vz[ix + (iy-1)*_size(Vz,1) + (_size(Vz,3)-2)*_size(Vz,1)*_size(Vz,2)]) + p_dz3*tyz[ix-1 + (iy-1)*_size(tyz,1) + (_size(tyz,3)-2)*_size(tyz,1)*_size(tyz,2)]/eta_yz[ix-1 + (iy-1)*_size(eta_yz,1) + (_size(eta_yz,3)-1)*_size(eta_yz,1)*_size(eta_yz,2)]; } }
    if (coords[2]==(dims[2]-1)){ if (iz==(_size(T ,3)-1) && ix<(_size(T,1)) && iy<(_size(T,2))){ T[ix + iy*_size(T,1) + (_size(T,3)-1)*_size(T,1)*_size(T,2)] = (DAT) 0.0; } }
}

  __global__ void swapT(DAT* T,DAT* T_old,const int nx, const int ny, const int nz){
    def_sizes(T     , nx, ny, nz);
    def_sizes(T_old , nx, ny, nz);

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (participate_a(T_old))  all(T_old) = all(T);
}
////////// ========================================  MAIN  ======================================== //////////
int main(int argc, char *argv[]){
    size_t i, istep;
    
    set_up_gpu();
    set_up_parallelisation();
    
    Nix = ((nx-2)*dims[0])+2;  // debug: must be after set_up_process_grid() call because, dims is only defined there! 
    Niy = ((ny-2)*dims[1])+2;
    Niz = ((nz-2)*dims[2])+2;
    dx  = Lx/((DAT)Nix-1.0);
    dy  = Ly/((DAT)Niy-1.0);
    dz  = Lz/((DAT)Niz-1.0);
    N   = Nix*Niy*Niz; double mem = (double)1e-9*(double)N*sizeof(DAT);

    if (me==0) printf("Local _size: %dx%dx%d (%1.4f GB) %d iterations ...\n", nx, ny, nz, mem*32.0, niter);
    if (me==0) printf("Launching (%dx%dx%d) grid of (%dx%dx%d) blocks.\n\n", grid.x, grid.y, grid.z, block.x, block.y, block.z);
    // allocate memory (on host + device), initialize to 0, copy from host to device
    zeros(xc     , nx  , 1   , 1   );
    zeros(yc     , 1   , ny  , 1   );
    zeros(zc     , 1   , 1   , nz  );
    zeros(xn     , nx+1, 1   , 1   );
    zeros(yn     , 1   , ny+1, 1   );
    zeros(zn     , 1   , 1   , nz+1);
    zeros(dtP    , nx  , ny  , nz  );
    zeros(dtVx   , nx-1, ny-2, nz-2);
    zeros(dtVy   , nx-2, ny-1, nz-2);
    zeros(dtVz   , nx-2, ny-2, nz-1);
    ones( etan   , nx  , ny  , nz  );
    zeros(Vx     , nx+1, ny  , nz  );
    zeros(Vy     , nx  , ny+1, nz  );
    zeros(Vz     , nx  , ny  , nz+1);
    zeros(P      , nx  , ny  , nz  );
    zeros(divV   , nx  , ny  , nz  );
    zeros(Exx    , nx  , ny  , nz  );
    zeros(Eyy    , nx  , ny  , nz  );
    zeros(Ezz    , nx  , ny  , nz  );
    zeros(Exy    , nx-1, ny-1, nz-2);
    zeros(Exz    , nx-1, ny-2, nz-1);
    zeros(Eyz    , nx-2, ny-1, nz-1);
    zeros(txx    , nx  , ny  , nz  );
    zeros(tyy    , nx  , ny  , nz  );
    zeros(tzz    , nx  , ny  , nz  );
    zeros(txy    , nx-1, ny-1, nz-2);
    zeros(txz    , nx-1, ny-2, nz-1);
    zeros(tyz    , nx-2, ny-1, nz-1);
    zeros(Exyn   , nx  , ny  , nz  );
    zeros(Exzn   , nx  , ny  , nz  );
    zeros(Eyzn   , nx  , ny  , nz  );
    zeros(eta_xz , nx-1, ny-2, nz-1);
    zeros(eta_yz , nx-2, ny-1, nz-1);
    zeros(EII2   , nx  , ny  , nz  );
    zeros(etait  , nx  , ny  , nz  );
    zeros(dVxdt  , nx-1, ny-2, nz-2);
    zeros(dVydt  , nx-2, ny-1, nz-2);
    zeros(dVzdt  , nx-2, ny-2, nz-1);
    zeros(T      , nx  , ny  , nz  );
    zeros(T_old  , nx  , ny  , nz  );
    zeros(Tres   , nx  , ny  , nz  );
    zeros(qxT    , nx+1, ny  , nz  );
    zeros(qyT    , nx  , ny+1, nz  );
    zeros(qzT    , nx  , ny  , nz+1);

    init_sides(Vx);
    init_sides(Vy);
    init_sides(Vz);
    init_sides(T);
    init_sides(etan)

    int evol_count=0;
    zeros_h(params_evol,NB_PARAMS, nt,1);
    zeros_h(params     ,NB_PARAMS, 1 ,1);
    zeros(__device_maxval ,(int)grid.x,(int)grid.y,(int)grid.z);
    zeros(__device_minval ,(int)grid.x,(int)grid.y,(int)grid.z);
    zeros(__device_meanval,(int)grid.x,(int)grid.y,(int)grid.z);
    zeros(__device_normval,(int)grid.x,(int)grid.y,(int)grid.z);
    // DAT device_MAX=0.0,      global_MAX=0.0;
    // DAT device_MIN=999999.0, global_MIN=999999.0;
    // DAT device_MEAN=0.0,     global_MEAN=0.0;
    DAT device_NORM=0.0,     global_NORM=0.0;
    // Initial condition
    DAT iterror = (DAT)0.0;
    int it, iter, isave=0;
    ///////////================================================================================ INITIALISATION ====////
    const DAT dampX  = (DAT)1.0-damp/(DAT)Nix;
    const DAT dampY  = (DAT)1.0-damp/(DAT)Niy;
    const DAT dampZ  = (DAT)1.0-damp/(DAT)Niz;
    const DAT DX     = (DAT)1.0/dx;
    const DAT DY     = (DAT)1.0/dy;
    const DAT DZ     = (DAT)1.0/dz;
    const DAT DT     = (DAT)1.0/dt;

    const DAT dxyz2   = pow(min(dx,min(dy,dz)),2);
    const DAT nxyz    =  max(Nix,max(Niy,Niz));
    const DAT pxyz    = (DAT)1.0/(DAT)3.0;
    const DAT p_dz2   = dz/(DAT)2.0;
    const DAT p_dzdx  = dz/dx;
    const DAT p_dzdy  = dz/dy;
    const DAT p_dz3   = dz/(DAT)3.0;
    const DAT p_dzeta = dz*((DAT)1.0/(DAT)3.0 - eta_b);
          DAT time    = (DAT)0.0;
    const DAT dtau    = (DAT)1.0/((DAT)4.0/dxyz2 + (DAT)1.0/dt);
    if (me==0) printf(">> nt    = %d     \n", nt); 
    initialize<<<grid,block>>>(xc_d,xn_d,yc_d,yn_d,zc_d,zn_d,etan_d,T_d,coords_d,etani,gamma_b,dx,dy,dz,nx,ny,nz,Lz);  cudaDeviceSynchronize();
    SaveArray(xc,"xc"); SaveArray(xn,"xn");
    SaveArray(yc,"yc"); SaveArray(yn,"yn");
    SaveArray(zc,"zc"); SaveArray(zn,"zn");
    SaveArray(T,"T_init");
    ///////////================================================================================ ACTION - timeloop ====////
    BARRIER();
    for (it=0; it < nt; it++){
        swapT <<<grid,block>>>(T_d,T_old_d,nx,ny,nz); cudaDeviceSynchronize();

        for (iter=0; iter<niter; iter++){
            if (iter==3){ tic(); }// start time counter
            if ((iter%(int)5)==0){ BARRIER(); }
            timesteps<<<grid,block>>>(etan_d, dtVx_d, dtVy_d, dtVz_d, dtP_d, relV, relP, eta_b, nxyz, dxyz2, nx, ny, nz); cudaDeviceSynchronize();
            strain_rates_P<<<grid,block>>>(P_d, Vx_d, Vy_d, Vz_d, divV_d, Exx_d, Eyy_d,Ezz_d, Exy_d, Exz_d, Eyz_d, dtP_d, pxyz, DX, DY, DZ, nx, ny, nz); cudaDeviceSynchronize();
            stresses<<<grid,block>>>(T_d, qxT_d, qyT_d, qzT_d, etan_d, eta_xz_d, eta_yz_d, divV_d, Exx_d, Eyy_d, Ezz_d, Exy_d, Exz_d, Eyz_d, txx_d, tyy_d, tzz_d, txy_d, txz_d, tyz_d, Exyn_d, Exzn_d, Eyzn_d, DX, DY, DZ, eta_b, nx, ny, nz); cudaDeviceSynchronize();
            BC_En<<<grid,block>>>(Exyn_d,Exzn_d,Eyzn_d, coords_d, nx, ny, nz); cudaDeviceSynchronize(); // BC
            ResV_etait<<<grid,block>>>(etan_d, T_d, T_old_d, Tres_d, qxT_d, qyT_d, qzT_d,dVxdt_d,dVydt_d,dVzdt_d,P_d,EII2_d,etait_d,Exx_d,Eyy_d,Ezz_d,Exyn_d,Exzn_d,Eyzn_d,txx_d,tyy_d,tzz_d,txy_d,txz_d,tyz_d, MELT, Tm, T_sc, T0_K, F,DT,theta,mpow,alph,dampX,dampY,dampZ,DX,DY,DZ,nx,ny,nz); cudaDeviceSynchronize();
            
            for (istep=0; istep<repeat; istep++){
                update_V_eta<<<grid,block,0,streams[istep]>>>(istep, T_d, Tres_d,Vx_d,Vy_d,Vz_d,dVxdt_d,dVydt_d,dVzdt_d,dtVx_d,dtVy_d,dtVz_d,etait_d,etan_d,dtau,eta0,rele,nx,ny,nz,it);
                update_sides3(Vx,Vy,Vz);
                update_sides(T);
                update_sides(etan);
            }
            cudaDeviceSynchronize();
            __Bc_no_Z_G(etan); // BC_Visc_1 
            __Bc_no_X_G(etan); // BC_Visc_2 
            __Bc_no_Y_G(etan); // BC_Visc_3
            BC_V_1<<<grid,block>>>(Vx_d, Vy_d, Vz_d, coords_d, nx, ny, nz); cudaDeviceSynchronize();                // BC
            BC_V_2<<<grid,block>>>(Vx_d, Vy_d, Vz_d, coords_d, dims_d, nx, ny, nz); cudaDeviceSynchronize();        // BC
            BC_V_3<<<grid,block>>>(Vx_d, Vy_d, Vz_d, coords_d, nx, ny, nz); cudaDeviceSynchronize();                // BC
            BC_V_4<<<grid,block>>>(Vx_d, Vy_d, Vz_d, coords_d, dims_d, nx, ny, nz); cudaDeviceSynchronize();        // BC
            BC_V_5<<<grid,block>>>(qzT_d,Vx_d, Vy_d, Vz_d, gamma_b, coords_d, nx, ny, nz); cudaDeviceSynchronize(); // BC
            BC_V_6<<<grid,block>>>(T_d, Vx_d, Vy_d, Vz_d, P_d, divV_d, txz_d, tyz_d, etan_d, eta_xz_d, eta_yz_d, coords_d, dims_d, p_dz2, p_dz3, p_dzdx, p_dzdy, p_dzeta, nx, ny, nz); cudaDeviceSynchronize(); // BC
            // Check
            if ((iter%nout)==1 && iter>1){
                __MPI_norm(dVxdt);  __MPI_norm(dVydt);  __MPI_norm(dVzdt);  __MPI_norm(divV); __MPI_norm(divV);
                iterror = max(max(params_h[dVxdt_NORM] , params_h[dVydt_NORM]) , max(params_h[dVzdt_NORM] , params_h[divV_NORM]));
                if (me==0){ printf(">> it  = %d, iter=%4.d , err=%1.3e \n", it, iter, iterror); }
                if (iterror<eps_stop && iter>1){ break; }
            }// end nout
        }// end iter
        if (!(iterror>0 || iterror==0 || iterror<0)){ if (me==0){ printf("\n !! ERROR: resid=Nan, break (it=%d, iter=%d) !! \n\n", (it+1), (iter+1)); } break; }
        time = time + dt;
        params_h[it_EVOL]=it; params_h[iter_EVOL]=iter; params_h[time_EVOL]=time; params_write();

        tim("> Performance", mem*(iter-3)*nIO); // timer test
        
        if ( (it%nsave)==0 || (it==(nt-1)) ){
            save_info(); save_coords();  // Save simulation infos and coords (.inf files)
            SaveArray(Vx  ,"Vx"); 
            SaveArray(Vy  ,"Vy");
            SaveArray(Vz  ,"Vz"); 
            SaveArray(T   ,"T");
            SaveArray(P   ,"P"); 
            SaveArray(etan,"etan");
            SaveArray(EII2,"EII2");
            SaveArray(txx ,"txx"); 
            SaveArray(tyy ,"tyy");
            SaveArray(tzz ,"tzz"); 
            SaveArray(txy ,"txy"); 
            SaveArray(txz ,"txz");
            SaveArray(tyz ,"tyz");
            save_array(params_evol_h,numel(params_evol),"pa",isave);
            isave = isave+1;
        }
    }// end for (time loop)
    ///////////================================================================================ POSTPROCESS ====////
    // clear host memory & clear device memory
    free_all(xc); 
    free_all(xn); 
    free_all(yc); 
    free_all(yn); 
    free_all(zc); 
    free_all(zn);
    free_all(dtP); 
    free_all(dtVx); 
    free_all(dtVy); 
    free_all(dtVz); 
    free_all(etan); 
    free_all(Vx);
    free_all(Vy);
    free_all(Vz);
    free_all(P);
    free_all(divV);
    free_all(Exx);
    free_all(Eyy);
    free_all(Ezz);
    free_all(Exy);
    free_all(Exz);
    free_all(Eyz);
    free_all(txx);
    free_all(tyy);
    free_all(tzz);
    free_all(txy);
    free_all(txz);
    free_all(tyz);
    free_all(Exyn);
    free_all(Exzn);
    free_all(Eyzn);
    free_all(eta_xz);
    free_all(eta_yz);
    free_all(EII2);
    free_all(etait);
    free_all(dVxdt);
    free_all(dVydt);
    free_all(dVzdt);
    free_all(T);
    free_all(Tres);
    free_all(T_old);
    free_all(qxT);
    free_all(qyT);
    free_all(qzT);

    free_sides(Vx);
    free_sides(Vy);
    free_sides(Vz);
    free_sides(T);
    free_sides(etan);

    free_all(__device_maxval );
    free_all(__device_minval );
    free_all(__device_meanval);
    free_all(__device_normval);
    free(params_h);
    free(params_evol_h);
    clean_cuda();
    MPI_Finalize();
    return 0;
}

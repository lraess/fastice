#!/bin/bash

# runme.sh is a shell script for executing GPU MPI application
# -> to make it executable: chmod +x runme.sh or chmod 755 runme.sh
#------------------------------------------------------------------

# load the reqired modules:
module load cuda/11.4
module load openmpi/gcc83-314-c112

mpirun=$(which mpirun)

# remove what should be removed
rm a.out

# How many mpi procs should run
if [ $# -lt 4 ]; then
    echo $0: usage: runme.sh nprocs dimsx dimsy dimsz
    exit 1
fi
nprocs=$1
DX=$2
DY=$3
DZ=$4

# compile the code for Titan X
nvcc MPI_GPU_ICE_TM_3D.cu -arch=sm_52 -O3 --compiler-bindir mpic++ -DUSE_MPI -DDIMX=$DX -DDIMY=$DY -DDIMZ=$DZ

# compile the code for V100 (node40)
# nvcc MPI_GPU_ICE_TM_3D.cu -arch=sm_70 -O3 --compiler-bindir mpic++ -DUSE_MPI -DDIMX=$DX -DDIMY=$DY -DDIMZ=$DZ

# execute it
run_cmd="-np $nprocs -rf gpu_rankfile_128 --mca btl_openib_warn_default_gid_prefix 0 a.out"

echo $mpirun $run_cmd

$mpirun $run_cmd

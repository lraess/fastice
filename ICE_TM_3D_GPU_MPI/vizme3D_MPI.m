clear
machineformat='n';  skip=0; OVERLAP = 2; 
nxyz   = load('0_nxyz.inf'  );  PRECIS=nxyz(1); nx=nxyz(2); ny=nxyz(3); nz=nxyz(4);
dims   = load('0_dims.inf'  );  nprocs=dims(1); dims=dims(2:4);
nxyz_l = load('0_nxyz_l.inf');
evol   = load('0_evol.inf'  ); NB_PARAMS=evol(1); NB_EVOL=evol(2);
coords = zeros(nprocs,3);
for  ip = 1:nprocs
    coords(ip,:) = load([ num2str(ip-1) '_co.inf']);
end
if (PRECIS==8), DAT = 'double';  elseif (PRECIS==4), DAT = 'single';  end  

isave = 2;

name=[num2str(isave) '_0_pa.res'];id = fopen(name); evol = fread(id,DAT); fclose(id); evol = reshape(evol,NB_PARAMS,NB_EVOL,1);

figure(1),clf,colormap(jet),set(gcf,'color','white')
muc = load_field('etan', nxyz_l, nxyz_l, OVERLAP, nprocs, coords, DAT, isave, skip, machineformat);
muc_flat = squeeze(muc(:,fix(ny/2),:));
subplot(221),imagesc(log10(flipud(muc_flat'))), colorbar,axis image 

nxyz_Vx = nxyz_l + [1 0 0];
Vx = load_field('Vx', nxyz_Vx, nxyz_l, OVERLAP, nprocs, coords, DAT, isave, skip, machineformat);
Vzx_flat = squeeze(Vx(:,fix(ny/2),:));
subplot(222),imagesc(flipud(Vzx_flat')), colorbar,axis image 
 
Pt = load_field('P',nxyz_l, nxyz_l, OVERLAP, nprocs, coords, DAT, isave, skip, machineformat);
Pt_flat = squeeze(Pt(:,fix(ny/2),:));
subplot(223),imagesc(flipud(Pt_flat')), colorbar,axis image 
 
T = load_field('T',nxyz_l, nxyz_l, OVERLAP, nprocs, coords, DAT, isave, skip, machineformat);
T_flat = squeeze(T(:,fix(ny/2),:));
subplot(224),imagesc(flipud(T_flat')), colorbar,axis image 


[C,I] = min(T(:));
[I1,I2,I3] = ind2sub(size(T),I);
T(I1,I2,I3)



% figure(3),clf,set(gcf,'color','white')
% semilogy(evol(1,2:end),evol(2,2:end),'-r*',evol(1,2:end),evol(4,2:end),'b-',evol(1,2:end),evol(6,2:end),'bs',evol(1,2:end),evol(8,2:end),'ms'),xlabel('iter'),legend('||Rx||','||Ry||','||Rz||','||RPt||')
% drawnow



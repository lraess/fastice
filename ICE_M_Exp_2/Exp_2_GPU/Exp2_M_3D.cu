/* FastICE: Thermomechanical ice deformation models 3D GPU

Copyright (C) 2019  Ludovic Raess, Aleksandar Licul et al.
 
FastICE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
FastICE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with FastICE. If not, see <http://www.gnu.org/licenses/>. */

// -------------------------------------------------------------------------
// Compile as: nvcc Exp2_M_3D.cu -arch=sm_XX --ptxas-options=-v
// arch=sm_XX: TITAN Black=sm_35, TITAN X=sm_52, TITAN Xp=sm_61, Tesla V100=sm_70
// Run as: ./a.out
// -------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define GPU_ID 0
//#define USE_SINGLE_PRECISION   

#ifdef USE_SINGLE_PRECISION
#define DAT     float
#define PRECIS  4
const DAT eps_stop  = 3e-2;
//const DAT eps_stop  = 5e-14;
#else
#define DAT     double
#define PRECIS  8
const DAT eps_stop  = 5e-7;
#endif

#define BLOCK_X            32
#define BLOCK_Y            16
#define BLOCK_Z            2
#define NB_THREADS         (BLOCK_X*BLOCK_Y*BLOCK_Z)
#define GRID_X             8
#define GRID_Y             16
#define GRID_Z             32
#define NB_BLOCKS          (GRID_X*GRID_Y*GRID_Z)
#define NB_MPS_OF_GPU      24
#define MAX_X_OVERLAP      1
#define MAX_Y_OVERLAP      1
#define MAX_Z_OVERLAP      1

////// global variables //////   
#define PI 3.14159265358979323846
// (numerics)
const int nx    = GRID_X*BLOCK_X - MAX_X_OVERLAP; 
const int ny    = GRID_Y*BLOCK_Y - MAX_Y_OVERLAP;        
const int nz    = GRID_Z*BLOCK_Z - MAX_Z_OVERLAP; 

#include "macros_3D.h"

__global__ void initialize(DAT* xc,DAT* xn,DAT* yc,DAT* yn,DAT* zc,DAT* zn,DAT* beta_x,DAT* beta_y, const DAT beta_0,const DAT dx, const DAT dy,const DAT dz,const int nx, const int ny,const int nz,const DAT Lx,const DAT Ly)
  {  
    def_sizes(xc    , nx   , 1    , 1    );
    def_sizes(xn    , nx+1 , 1    , 1    );
    def_sizes(yc    , 1    , ny   , 1    );
    def_sizes(yn    , 1    , ny+1 , 1    );
    def_sizes(zc    , 1    , 1    , nz   );
    def_sizes(zn    , 1    , 1    , nz+1 );
    def_sizes(beta_x , nx-1 , ny   , 1    );
    def_sizes(beta_y , nx   , ny-1 , 1    );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    
    if (participate_a(xc))    all(xc)    =  (DAT) ix*dx;
    if (participate_a(xn))    all(xn)    = - dx/(DAT)2.0 + (DAT) ix*dx;
    if (participate_a(yc))    all(yc)    =  (DAT) iy*dy;
    if (participate_a(yn))    all(yn)    = - dy/(DAT)2.0 + (DAT) iy*dy;
    if (participate_a(zc))    all(zc)    =  (DAT) iz*dz;
    if (participate_a(zn))    all(zn)    = - dz/(DAT)2.0 + (DAT) iz*dz;
    if (participate_a(beta_x))  all(beta_x)  =  beta_0*((DAT)1.0+ sin((DAT)2.0*PI*((DAT)(ix+1)*dx-dx/(DAT)2.0)/Lx)*sin((DAT)2.0*PI*((DAT)iy*dy/Ly)));
    if (participate_a(beta_y))  all(beta_y)  =  beta_0*((DAT)1.0+ sin((DAT)2.0*PI*((DAT)ix*dx/Lx))*sin((DAT)2.0*PI*((DAT)(iy+1)*dy-dy/(DAT)2.0)/Ly));

  }

__global__ void timesteps(DAT* etan,DAT* dtVx,DAT* dtVy,DAT* dtVz,DAT* dtP,const DAT relV,const DAT relP,const DAT eta_b,const DAT nxyz,const DAT dxyz2,const int nx, const int ny, const int nz)
{

    def_sizes(dtVx  , nx-1 , ny-2 , nz-2 );
    def_sizes(dtVy  , nx-2 , ny-1 , nz-2 );
    def_sizes(dtVz  , nx-2 , ny-2 , nz-1 );
    def_sizes(dtP   , nx   , ny   , nz   );
    def_sizes(etan  , nx   , ny   , nz   );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_a(dtP))    all(dtP)   = relP*(DAT)4.1/nxyz*all(etan)*((DAT)1.0+eta_b)/(DAT)2.0;
    if (participate_a(dtVx))   all(dtVx)  = relV*dxyz2/av_xi(etan)/((DAT)1.0+eta_b)/(DAT)6.1;
    if (participate_a(dtVy))   all(dtVy)  = relV*dxyz2/av_yi(etan)/((DAT)1.0+eta_b)/(DAT)6.1;
    if (participate_a(dtVz))   all(dtVz)  = relV*dxyz2/av_zi(etan)/((DAT)1.0+eta_b)/(DAT)6.1;

}
                                                                             
__global__ void strain_rates_P(DAT* P,DAT* Vx,DAT* Vy, DAT* Vz,DAT* divV,DAT* Exx,DAT* Eyy,DAT* Ezz,DAT* Exy,DAT* Exz,DAT* Eyz,DAT* dtP, const DAT pxyz, const DAT DX, const DAT DY, const DAT DZ,const int nx, const int ny, const int nz)
{

    def_sizes(P    , nx   , ny   , nz   );
    def_sizes(Vx   , nx+1 , ny   , nz   );
    def_sizes(Vy   , nx   , ny+1 , nz   ); 
    def_sizes(Vz   , nx   , ny   , nz+1 );
    def_sizes(divV , nx   , ny   , nz   );
    def_sizes(Exx  , nx   , ny   , nz   );
    def_sizes(Eyy  , nx   , ny   , nz   );
    def_sizes(Ezz  , nx   , ny   , nz   );
    def_sizes(Exy  , nx-1 , ny-1 , nz-2 );
    def_sizes(Exz  , nx-1 , ny-2 , nz-1 );
    def_sizes(Eyz  , nx-2 , ny-1 , nz-1 );
    def_sizes(dtP  , nx   , ny   , nz   );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_a(divV))  all(divV) = DX*d_xa(Vx) + DY*d_ya(Vy) + DZ*d_za(Vz);
    if (participate_a(Exx))   all(Exx)  = DX*d_xa(Vx) - pxyz*(DX*d_xa(Vx) + DY*d_ya(Vy) + DZ*d_za(Vz));
    if (participate_a(Eyy))   all(Eyy)  = DY*d_ya(Vy) - pxyz*(DX*d_xa(Vx) + DY*d_ya(Vy) + DZ*d_za(Vz));
    if (participate_a(Ezz))   all(Ezz)  = DZ*d_za(Vz) - pxyz*(DX*d_xa(Vx) + DY*d_ya(Vy) + DZ*d_za(Vz));
    if (participate_a(Exy))   all(Exy)  = (DAT)0.5*( DY*d_yi(Vx) + DX*d_xi(Vy) );
    if (participate_a(Exz))   all(Exz)  = (DAT)0.5*( DZ*d_zi(Vx) + DX*d_xi(Vz) );
    if (participate_a(Eyz))   all(Eyz)  = (DAT)0.5*( DZ*d_zi(Vy) + DY*d_yi(Vz) );

    if (participate_a(P))     all(P)    = all(P)  -  all(divV)*all(dtP); 

}

__global__ void stresses(DAT* etan,DAT* eta_xz,DAT* eta_yz,DAT* divV,DAT* Exx,DAT* Eyy,DAT* Ezz,DAT* Exy,DAT* Exz,DAT* Eyz,DAT* txx,DAT* tyy,DAT* tzz,DAT* txy,DAT* txz,DAT* tyz,DAT* Exyn,DAT* Exzn,DAT* Eyzn, const DAT eta_b,const int nx, const int ny, const int nz)
{

    def_sizes(Exx  , nx   , ny   , nz   );
    def_sizes(Eyy  , nx   , ny   , nz   );
    def_sizes(Ezz  , nx   , ny   , nz   );
    def_sizes(Exy  , nx-1 , ny-1 , nz-2 );
    def_sizes(Exz  , nx-1 , ny-2 , nz-1 );
    def_sizes(Eyz  , nx-2 , ny-1 , nz-1 );
    def_sizes(txx  , nx   , ny   , nz   );
    def_sizes(tyy  , nx   , ny   , nz   );
    def_sizes(tzz  , nx   , ny   , nz   );
    def_sizes(txy  , nx-1 , ny-1 , nz-2 );
    def_sizes(txz  , nx-1 , ny-2 , nz-1 );
    def_sizes(tyz  , nx-2 , ny-1 , nz-1 );
    def_sizes(etan , nx   , ny   , nz   );
    def_sizes(divV , nx   , ny   , nz   );
    def_sizes(eta_xz , nx-1 , ny-2 , nz-1 );
    def_sizes(eta_yz , nx-2 , ny-1 , nz-1 );
    def_sizes(Exyn , nx   , ny   , nz   );
    def_sizes(Exzn , nx   , ny   , nz   );
    def_sizes(Eyzn , nx   , ny   , nz   );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_a(txx))   all(txx)  = (DAT)2.0*all(etan)*(all(Exx) + eta_b*all(divV));
    if (participate_a(tyy))   all(tyy)  = (DAT)2.0*all(etan)*(all(Eyy) + eta_b*all(divV));
    if (participate_a(txx))   all(tzz)  = (DAT)2.0*all(etan)*(all(Ezz) + eta_b*all(divV));

    if (participate_a(txy))   all(txy)  = (DAT)2.0*av_xyi(etan)*all(Exy);
    if (participate_a(txz))   all(txz)  = (DAT)2.0*av_xzi(etan)*all(Exz);
    if (participate_a(tyz))   all(tyz)  = (DAT)2.0*av_yzi(etan)*all(Eyz);

    if (participate_a(eta_xz))  all(eta_xz) = av_xzi(etan);
    if (participate_a(eta_yz))  all(eta_yz) = av_yzi(etan);

    if (participate_i(Exyn))  inn(Exyn) = av_xya(Exy);
    if (participate_i(Exzn))  inn(Exzn) = av_xza(Exz);
    if (participate_i(Eyzn))  inn(Eyzn) = av_yza(Eyz);

}

__global__ void BC_En(DAT* Exyn, DAT* Exzn, DAT* Eyzn, const int nx, const int ny, const int nz)
{
    def_sizes(Exyn , nx   , ny   , nz   );
    def_sizes(Exzn , nx   , ny   , nz   );
    def_sizes(Eyzn , nx   , ny   , nz   );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (iz==0 && ix<size(Exyn,1) && iy<size(Exyn,2))  Exyn[ix + iy*size(Exyn,1)] =  Exyn[ix + iy*size(Exyn,1) + size(Exyn,1)*size(Exyn,2)];
    if (iz==0 && ix<size(Exzn,1) && iy<size(Exzn,2))  Exzn[ix + iy*size(Exzn,1)] =  Exzn[ix + iy*size(Exzn,1) + size(Exzn,1)*size(Exzn,2)];
    if (iz==0 && ix<size(Eyzn,1) && iy<size(Eyzn,2))  Eyzn[ix + iy*size(Eyzn,1)] =  Eyzn[ix + iy*size(Eyzn,1) + size(Eyzn,1)*size(Eyzn,2)];  

}

__global__ void ResV_etait(DAT* dVxdt,DAT* dVydt,DAT* dVzdt,DAT* P,DAT* EII2,DAT* etait,DAT* Exx,DAT* Eyy,DAT* Ezz, DAT* Exyn, DAT* Exzn, DAT* Eyzn, DAT* txx,DAT* tyy,DAT* tzz,DAT* txy,DAT* txz,DAT* tyz,const DAT mpow,const DAT alph, const DAT dampX, const DAT dampY, const DAT dampZ, const DAT DX, const DAT DY, const DAT DZ, const int nx, const int ny, const int nz)
{
  
    def_sizes(Exx  , nx   , ny   , nz   );
    def_sizes(Eyy  , nx   , ny   , nz   );
    def_sizes(Ezz  , nx   , ny   , nz   );
    def_sizes(Exyn , nx   , ny   , nz   );
    def_sizes(Exzn , nx   , ny   , nz   );
    def_sizes(Eyzn , nx   , ny   , nz   );
    def_sizes(txx  , nx   , ny   , nz   );
    def_sizes(tyy  , nx   , ny   , nz   );
    def_sizes(tzz  , nx   , ny   , nz   );
    def_sizes(txy  , nx-1 , ny-1 , nz-2 );
    def_sizes(txz  , nx-1 , ny-2 , nz-1 );
    def_sizes(tyz  , nx-2 , ny-1 , nz-1 );
    def_sizes(P    , nx   , ny   , nz   );
    def_sizes(EII2 , nx   , ny   , nz   );
    def_sizes(etait, nx   , ny   , nz   );
    def_sizes(dVxdt , nx-1 , ny-2 , nz-2 );
    def_sizes(dVydt , nx-2 , ny-1 , nz-2 );
    def_sizes(dVzdt , nx-2 , ny-2 , nz-1 );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_a(dVxdt))  all(dVxdt) = all(dVxdt)*dampX - DX*d_xi(P)   + DX*d_xi(txx) + DY*d_ya(txy) + DZ*d_za(txz) + (DAT) 1.0; 
    if (participate_a(dVydt))  all(dVydt) = all(dVydt)*dampY + DX*d_xa(txy) - DY*d_yi(P)   + DY*d_yi(tyy) + DZ*d_za(tyz);
    if (participate_a(dVzdt))  all(dVzdt) = all(dVzdt)*dampZ + DX*d_xa(txz) + DY*d_ya(tyz) - DZ*d_zi(P)   + DZ*d_zi(tzz) - cos(alph)/sin(alph);

    if (participate_a(EII2))   all(EII2)  = (DAT)0.5*(all(Exx)*all(Exx) + all(Eyy)*all(Eyy) + all(Ezz)*all(Ezz)) + all(Exyn)*all(Exyn) + all(Exzn)*all(Exzn) + all(Eyzn)*all(Eyzn);
    if (participate_a(etait))  all(etait) = pow(all(EII2),mpow);

}

__global__ void update_V_eta(DAT* Vx,DAT* Vy,DAT* Vz,DAT* dVxdt,DAT* dVydt,DAT* dVzdt,DAT* dtVx,DAT* dtVy,DAT* dtVz,DAT* etait,DAT* etan,const DAT eta0,const DAT rele,const int nx,const int ny,const int nz)
{

    def_sizes(Vx    , nx+1 , ny   , nz   );
    def_sizes(Vy    , nx   , ny+1 , nz   );
    def_sizes(Vz    , nx   , ny   , nz+1 );
    def_sizes(dVxdt , nx-1 , ny-2 , nz-2 );
    def_sizes(dVydt , nx-2 , ny-1 , nz-2 );
    def_sizes(dVzdt , nx-2 , ny-2 , nz-1 );
    def_sizes(dtVx  , nx-1 , ny-2 , nz-2 );
    def_sizes(dtVy  , nx-2 , ny-1 , nz-2 );
    def_sizes(dtVz  , nx-2 , ny-2 , nz-1 );
    def_sizes(etan  , nx   , ny   , nz   );
    def_sizes(etait , nx   , ny   , nz   );;

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    int ixi = ix+1; 
    int iyi = iy+1;
    int izi = iz+1;

    if (participate_i(Vx))  inn(Vx) = inn(Vx) +  all(dVxdt)*all(dtVx); 
    if (participate_i(Vy))  inn(Vy) = inn(Vy) +  all(dVydt)*all(dtVy);
    if (participate_i(Vz))  inn(Vz) = inn(Vz) +  all(dVzdt)*all(dtVz);
    if (participate_a(etan))  all(etan) = min(exp(rele*log(all(etait)) + ((DAT)1.0 - rele)*log(all(etan)) ),eta0); 

}

__global__ void BC_Visc(DAT* etan,const int nx, const int ny, const int nz)
  {  

    def_sizes(etan , nx   , ny   , nz   );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;
    
    if (iz==size(etan,3)-1 && ix<size(etan,1) && iy<size(etan,2))   etan[ix + iy*size(etan,1) + (size(etan,3)-1)*size(etan,1)*size(etan,2)] = etan[ix + iy*size(etan,1) + (size(etan,3)-2)*size(etan,1)*size(etan,2)];

  }

__global__ void BC_V_1(DAT* Vx,DAT* Vy,DAT* Vz,const int nx, const int ny, const int nz)
  {  

    def_sizes(Vx    , nx+1 , ny   , nz   );
    def_sizes(Vy    , nx   , ny+1 , nz   );
    def_sizes(Vz    , nx   , ny   , nz+1 );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (ix==0 && iy< (size(Vx,2)) && iz< (size(Vx,3))) Vx[iy*size(Vx,1) + iz*size(Vx,1)*size(Vx,2)] = Vx[(size(Vx,1)-2) + iy*size(Vx,1) + iz*size(Vx,1)*size(Vx,2)];
	if (ix==0 && iy< (size(Vy,2)) && iz< (size(Vy,3))) Vy[iy*size(Vy,1) + iz*size(Vy,1)*size(Vy,2)] = (DAT) 0.5*(Vy[(size(Vy,1)-2) + iy*size(Vy,1) + iz*size(Vy,1)*size(Vy,2)] + Vy[1 + iy*size(Vy,1) + iz*size(Vy,1)*size(Vy,2)]);
	if (ix==0 && iy< (size(Vz,2)) && iz< (size(Vz,3))) Vz[iy*size(Vz,1) + iz*size(Vz,1)*size(Vz,2)] = (DAT) 0.5*(Vz[(size(Vz,1)-2) + iy*size(Vz,1) + iz*size(Vz,1)*size(Vz,2)] + Vz[1 + iy*size(Vz,1) + iz*size(Vz,1)*size(Vz,2)]);    
 
  }

__global__ void BC_V_2(DAT* Vx,DAT* Vy,DAT* Vz,const int nx, const int ny, const int nz)
  { 

    def_sizes(Vx    , nx+1 , ny   , nz   );
    def_sizes(Vy    , nx   , ny+1 , nz   );
    def_sizes(Vz    , nx   , ny   , nz+1 );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (ix==(size(Vx,1)-1) && iy<(size(Vx,2)) && iz<(size(Vx,3))) Vx[(size(Vx,1)-1) + iy*size(Vx,1) + iz*size(Vx,1)*size(Vx,2)] = Vx[1 + iy*size(Vx,1) + iz*size(Vx,1)*size(Vx,2)];
    if (ix==(size(Vy,1)-1) && iy<(size(Vy,2)) && iz<(size(Vy,3))) Vy[(size(Vy,1)-1) + iy*size(Vy,1) + iz*size(Vy,1)*size(Vy,2)] = Vy[iy*size(Vy,1) + iz*size(Vy,1)*size(Vy,2)];
    if (ix==(size(Vz,1)-1) && iy<(size(Vz,2)) && iz<(size(Vz,3))) Vz[(size(Vz,1)-1) + iy*size(Vz,1) + iz*size(Vz,1)*size(Vz,2)] = Vz[iy*size(Vz,1) + iz*size(Vz,1)*size(Vz,2)];
    
  }

__global__ void BC_V_3(DAT* Vx,DAT* Vy,DAT* Vz,const int nx, const int ny, const int nz)
  {  
    def_sizes(Vx    , nx+1 , ny   , nz   );
    def_sizes(Vy    , nx   , ny+1 , nz   );
    def_sizes(Vz    , nx   , ny   , nz+1 );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (iy==0 && ix<(size(Vx,1)) && iz<(size(Vx,3)))  Vx[ix + iz*size(Vx,1)*size(Vx,2)] = 0.5*(Vx[ix + (size(Vx,2)-2)*size(Vx,1) + iz*size(Vx,1)*size(Vx,2)] + Vx[ix + size(Vx,1) + iz*size(Vx,1)*size(Vx,2)]);
	if (iy==0 && ix<(size(Vy,1)) && iz<(size(Vy,3)))  Vy[ix + iz*size(Vy,1)*size(Vy,2)] = Vy[ix + (size(Vy,2)-2)*size(Vy,1) + iz*size(Vy,1)*size(Vy,2)];
	if (iy==0 && ix<(size(Vz,1)) && iz<(size(Vz,3)))  Vz[ix + iz*size(Vz,1)*size(Vz,2)] = 0.5*(Vz[ix + (size(Vz,2)-2)*size(Vz,1) + iz*size(Vz,1)*size(Vz,2)] + Vz[ix + size(Vz,1) + iz*size(Vz,1)*size(Vz,2)]);
    
  }

__global__ void BC_V_4(DAT* Vx,DAT* Vy,DAT* Vz,const int nx, const int ny, const int nz)
  {  
    def_sizes(Vx    , nx+1 , ny   , nz   );
    def_sizes(Vy    , nx   , ny+1 , nz   );
    def_sizes(Vz    , nx   , ny   , nz+1 );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (iy==(size(Vx,2)-1) && ix< (size(Vx,1)) && iz< (size(Vx,3))) Vx[ix + (size(Vx,2)-1)*size(Vx,1) + iz*size(Vx,1)*size(Vx,2)] = Vx[ix + iz*size(Vx,1)*size(Vx,2)];
    if (iy==(size(Vy,2)-1) && ix< (size(Vy,1)) && iz< (size(Vy,3))) Vy[ix + (size(Vy,2)-1)*size(Vy,1) + iz*size(Vy,1)*size(Vy,2)] = Vy[ix + size(Vy,1) + iz*size(Vy,1)*size(Vy,2)];
    if (iy==(size(Vz,2)-1) && ix< (size(Vz,1)) && iz< (size(Vz,3))) Vz[ix + (size(Vz,2)-1)*size(Vz,1) + iz*size(Vz,1)*size(Vz,2)] = Vz[ix + iz*size(Vz,1)*size(Vz,2)];
    
  }

__global__ void BC_V_5(DAT* Vx,DAT* Vy,DAT* Vz,DAT* txz,DAT* tyz,DAT* etan,DAT* eta_xz,DAT* eta_yz,DAT* beta_x,DAT* beta_y,const DAT p_dzdx,const DAT p_dzdy,const DAT p_dz3,const DAT p_dz23,const int nx, const int ny, const int nz)
  {  

    def_sizes(Vx     , nx+1 , ny   , nz   );
    def_sizes(Vy     , nx   , ny+1 , nz   );
    def_sizes(Vz     , nx   , ny   , nz+1 );
    def_sizes(txz    , nx-1 , ny-2 , nz-1 );
    def_sizes(tyz    , nx-2 , ny-1 , nz-1 );
    def_sizes(eta_xz , nx-1 , ny-2 , nz-1 );
    def_sizes(eta_yz , nx-2 , ny-1 , nz-1 );
    def_sizes(beta_x , nx-1 , ny   , 1    );
    def_sizes(beta_y , nx   , ny-1 , 1    );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (iz==0 && ix>0 && ix<(size(Vx,1)-1) && iy>0 && iy< (size(Vx,2)-1))  Vx[ix + iy*size(Vx,1)] = (Vx[ix + iy*size(Vx,1) + size(Vx,1)*size(Vx,2)] + p_dzdx*(Vz[ix + iy*size(Vz,1) + size(Vz,1)*size(Vz,2)]-Vz[ix-1 + iy*size(Vz,1) + size(Vz,1)*size(Vz,2)])  - p_dz3*txz[ix-1 + (iy-1)*size(txz,1) + size(txz,1)*size(txz,2)]/eta_xz[ix-1 + (iy-1)*size(eta_xz,1)])/((DAT)1.0 + p_dz23*beta_x[ix-1 + iy*size(beta_x,1)]/eta_xz[ix-1 + (iy-1)*size(eta_xz,1)]);
	if (iz==0 && ix>0 && ix<(size(Vy,1)-1) && iy>0 && iy< (size(Vy,2)-1))  Vy[ix + iy*size(Vy,1)] = (Vy[ix + iy*size(Vy,1) + size(Vy,1)*size(Vy,2)] + p_dzdy*(Vz[ix + iy*size(Vz,1) + size(Vz,1)*size(Vz,2)]-Vz[ix + (iy-1)*size(Vz,1) + size(Vz,1)*size(Vz,2)])- p_dz3*tyz[ix-1 + (iy-1)*size(tyz,1) + size(tyz,1)*size(tyz,2)]/eta_yz[ix-1 + (iy-1)*size(eta_yz,1)])/((DAT)1.0 + p_dz23*beta_y[ix + (iy-1)*size(beta_y,1)]/eta_yz[ix-1 + (iy-1)*size(eta_yz,1)]); 
    if (iz==0 && ix<(size(Vz,1)) && iy< (size(Vz,2)))  Vz[ix + iy*size(Vz,1)] = -Vz[ix + iy*size(Vz,1) + size(Vz,1)*size(Vz,2)];
  
  }

__global__ void BC_V_6(DAT* Vx,DAT* Vy,DAT* Vz,DAT* P,DAT* divV,DAT* txz,DAT* tyz,DAT* etan,DAT* eta_xz,DAT* eta_yz,const DAT p_dz2,const DAT p_dz3,const DAT p_dzdx,const DAT p_dzdy,const DAT p_dzeta,const int nx, const int ny, const int nz)
  {  

    def_sizes(Vx     , nx+1 , ny   , nz   );
    def_sizes(Vy     , nx   , ny+1 , nz   );
    def_sizes(Vz     , nx   , ny   , nz+1 );
    def_sizes(P      , nx   , ny   , nz   );
    def_sizes(divV   , nx   , ny   , nz   );
    def_sizes(etan   , nx   , ny   , nz   );
    def_sizes(txz    , nx-1 , ny-2 , nz-1 );
    def_sizes(tyz    , nx-2 , ny-1 , nz-1 );
    def_sizes(eta_xz , nx-1 , ny-2 , nz-1 );
    def_sizes(eta_yz , nx-2 , ny-1 , nz-1 );

    int ix  = blockIdx.x*blockDim.x + threadIdx.x; 
    int iy  = blockIdx.y*blockDim.y + threadIdx.y;
    int iz  = blockIdx.z*blockDim.z + threadIdx.z;

    if (iz==(size(Vz,3)-1) && ix< size(Vz,1) && iy< size(Vz,2))                         Vz[ix + iy*size(Vz,1) + (size(Vz,3)-1)*size(Vz,1)*size(Vz,2)] = Vz[ix + iy*size(Vz,1) + (size(Vz,3)-2)*size(Vz,1)*size(Vz,2)] + p_dz2*(P[ix + iy*size(P,1) + (size(P,3)-1)*size(P,1)*size(P,2)]/etan[ix + iy*size(etan,1) + (size(etan,3)-1)*size(etan,1)*size(etan,2)]) - p_dzeta*divV[ix + iy*size(divV,1) + (size(divV,3)-1)*size(divV,1)*size(divV,2)];
    if (iz==(size(Vx,3)-1) && ix>0 && ix< (size(Vx,1)-1) && iy>0 && iy< (size(Vx,2)-1)) Vx[ix + iy*size(Vx,1) + (size(Vx,3)-1)*size(Vx,1)*size(Vx,2)] = Vx[ix + iy*size(Vx,1) + (size(Vx,3)-2)*size(Vx,1)*size(Vx,2)] - p_dzdx*(Vz[ix + iy*size(Vz,1) + (size(Vz,3)-2)*size(Vz,1)*size(Vz,2)] - Vz[ix-1 + iy*size(Vz,1) + (size(Vz,3)-2)*size(Vz,1)*size(Vz,2)])   + p_dz3*txz[ix-1 + (iy-1)*size(txz,1) + (size(txz,3)-2)*size(txz,1)*size(txz,2)]/eta_xz[ix-1 + (iy-1)*size(eta_xz,1) + (size(eta_xz,3)-1)*size(eta_xz,1)*size(eta_xz,2)];
    if (iz==(size(Vy,3)-1) && ix>0 && ix< (size(Vy,1)-1) && iy>0 && iy< (size(Vy,2)-1)) Vy[ix + iy*size(Vy,1) + (size(Vy,3)-1)*size(Vy,1)*size(Vy,2)] = Vy[ix + iy*size(Vy,1) + (size(Vy,3)-2)*size(Vy,1)*size(Vy,2)] - p_dzdy*(Vz[ix + iy*size(Vz,1) + (size(Vz,3)-2)*size(Vz,1)*size(Vz,2)] - Vz[ix + (iy-1)*size(Vz,1) + (size(Vz,3)-2)*size(Vz,1)*size(Vz,2)]) + p_dz3*tyz[ix-1 + (iy-1)*size(tyz,1) + (size(tyz,3)-2)*size(tyz,1)*size(tyz,2)]/eta_yz[ix-1 + (iy-1)*size(eta_yz,1) + (size(eta_yz,3)-1)*size(eta_yz,1)*size(eta_yz,2)];
  
  }

int main(){	
 int run_id,iter;
 size_t i;

 double mem = (double)1e-9*(double)nx*(double)ny*(double)nz*sizeof(DAT);
 
 DAT nIO   = 10.0;
 run_id    = 1;

 DAT  alph_deg = 0.1; 
 DAT  Lz       = 1.0;
 DAT  npow     = 3.0;   
 DAT  aspect_x = 10.0;
 DAT  aspect_y = 10.0;
 DAT  eta0     = 5e1;
 DAT  beta_0   = 0.194207089894401;
  // preproc     
 DAT Lx    = Lz*aspect_x;
 DAT Ly    = Lz*aspect_y;
 DAT alph  = alph_deg*PI/(DAT)180.0;
 DAT dx    = Lx/(nx-1);
 DAT dy    = Ly/(ny-1);
 DAT dz    = Lz/(nz-1);

 DAT iterror     = (DAT)0.0;
 DAT device_NORM = (DAT)0.0;
 DAT err_Vx_NORM = (DAT)0.0;
 DAT err_Vy_NORM = (DAT)0.0;
 DAT err_Vz_NORM = (DAT)0.0;
 DAT err_P_NORM  = (DAT)0.0;

 DAT damp  = 3.0;
 DAT relV  = 1.0/1.0; 
 DAT relP  = 1.0/3.0; 
 DAT eta_b = 1.0/3.0;
 DAT rele  = 1e-1;
 int niter = 5e6;
 DAT dampX = ((DAT)1.0 -damp/((DAT)nx));
 DAT dampY = ((DAT)1.0 -damp/((DAT)ny));
 DAT dampZ = ((DAT)1.0 -damp/((DAT)nz));
 DAT mpow  = -(1.0-1.0/npow)/2.0;
 DAT DX    = 1.0/dx;
 DAT DY    = 1.0/dy;
 DAT DZ    = 1.0/dz;
 DAT dxyz2 =  pow(min(min(dx,dy),dz),2);
 DAT nxyz  =  max(nx,max(ny,nz));
 DAT pxyz  = (DAT)1.0/(DAT)3.0;
 DAT p_dz2   = dz/(DAT)2.0;
 DAT p_dzdx  = dz/dx;
 DAT p_dzdy  = dz/dy;
 DAT p_dz3   = dz/(DAT)3.0;
 DAT p_dz23  = dz*(DAT)2.0/(DAT)3.0;
 DAT p_dzeta = dz*((DAT)1.0/(DAT)3.0 - eta_b);

 set_up_gpu();  
 printf("Local size: %dx%dx%d, %d iterations ...\n", nx, ny, nz, niter);
 printf("Launching (%dx%dx%d) grid of (%dx%dx%d) blocks.\n\n", grid.x, grid.y, grid.z, block.x, block.y, block.z);

 zeros(__device_normval,grid.x,grid.y,grid.z);
 zeros(xc    , nx   , 1    , 1    );
 zeros(yc    , 1    , ny   , 1    );
 zeros(zc    , 1    , 1    , nz   );
 zeros(xn    , nx+1 , 1    , 1    );   
 zeros(yn    , 1    , ny+1 , 1    );
 zeros(zn    , 1    , 1    , nz+1 );
 zeros(dtP   , nx   , ny   , nz   );
 zeros(dtVx  , nx-1 , ny-2 , nz-2 );
 zeros(dtVy  , nx-2 , ny-1 , nz-2 );
 zeros(dtVz  , nx-2 , ny-2 , nz-1 );
 ones(etan   , nx   , ny   , nz   );
 zeros(Vx    , nx+1 , ny   , nz   );
 zeros(Vy    , nx   , ny+1 , nz   );
 zeros(Vz    , nx   , ny   , nz+1 );
 zeros(P     , nx   , ny   , nz   );
 zeros(divV  , nx   , ny   , nz   );
 zeros(Exx   , nx   , ny   , nz   );
 zeros(Eyy   , nx   , ny   , nz   );
 zeros(Ezz   , nx   , ny   , nz   );
 zeros(Exy   , nx-1 , ny-1 , nz-2 );
 zeros(Exz   , nx-1 , ny-2 , nz-1 );
 zeros(Eyz   , nx-2 , ny-1 , nz-1 );
 zeros(txx   , nx   , ny   , nz   );
 zeros(tyy   , nx   , ny   , nz   );
 zeros(tzz   , nx   , ny   , nz   );
 zeros(txy   , nx-1 , ny-1 , nz-2 );
 zeros(txz   , nx-1 , ny-2 , nz-1 );
 zeros(tyz   , nx-2 , ny-1 , nz-1 );
 zeros(Exyn  , nx   , ny   , nz   );
 zeros(Exzn  , nx   , ny   , nz   );
 zeros(Eyzn  , nx   , ny   , nz   );
 zeros(eta_xz, nx-1 , ny-2 , nz-1 );
 zeros(eta_yz, nx-2 , ny-1 , nz-1 );
 zeros(EII2  , nx   , ny   , nz   );
 zeros(etait , nx   , ny   , nz   );
 zeros(dVxdt , nx-1 , ny-2 , nz-2 );
 zeros(dVydt , nx-2 , ny-1 , nz-2 );
 zeros(dVzdt , nx-2 , ny-2 , nz-1 );
 zeros(beta_x, nx-1 , ny   , 1    );
 zeros(beta_y, nx   , ny-1 , 1    );

 initialize <<<grid,block>>>(xc_d,xn_d,yc_d,yn_d,zc_d,zn_d,beta_x_d,beta_y_d,beta_0,dx,dy,dz,nx,ny,nz,Lx,Ly);  cudaDeviceSynchronize();
 SaveArray(xc,"xc",1); SaveArray(xn,"xn",1);
 SaveArray(yc,"yc",1); SaveArray(yn,"yn",1);
 SaveArray(zc,"zc",1); SaveArray(zn,"zn",1);
 // action
  for (iter=0; iter<niter; iter++){
    if (iter==3){ tic(); }// start time counter
    timesteps     <<<grid,block>>>(etan_d, dtVx_d, dtVy_d, dtVz_d, dtP_d, relV, relP, eta_b, nxyz, dxyz2, nx, ny, nz); cudaDeviceSynchronize();
    strain_rates_P<<<grid,block>>>(P_d, Vx_d, Vy_d, Vz_d, divV_d, Exx_d, Eyy_d,Ezz_d, Exy_d, Exz_d, Eyz_d, dtP_d, pxyz, DX, DY, DZ, nx, ny, nz); cudaDeviceSynchronize();
    stresses      <<<grid,block>>>(etan_d, eta_xz_d, eta_yz_d, divV_d, Exx_d, Eyy_d, Ezz_d, Exy_d, Exz_d, Eyz_d, txx_d, tyy_d, tzz_d, txy_d, txz_d, tyz_d, Exyn_d, Exzn_d, Eyzn_d, eta_b, nx, ny, nz); cudaDeviceSynchronize();
 
    BC_En       <<<grid,block>>>(Exyn_d,Exzn_d,Eyzn_d, nx, ny, nz); cudaDeviceSynchronize(); // BC

    ResV_etait    <<<grid,block>>>(dVxdt_d,dVydt_d,dVzdt_d,P_d,EII2_d,etait_d,Exx_d,Eyy_d,Ezz_d,Exyn_d,Exzn_d,Eyzn_d,txx_d,tyy_d,tzz_d,txy_d,txz_d,tyz_d,mpow,alph,dampX,dampY,dampZ,DX,DY,DZ,nx,ny,nz); cudaDeviceSynchronize();
    update_V_eta  <<<grid,block>>>(Vx_d,Vy_d,Vz_d,dVxdt_d,dVydt_d,dVzdt_d,dtVx_d,dtVy_d,dtVz_d,etait_d,etan_d,eta0,rele,nx,ny,nz); cudaDeviceSynchronize();
      
    BC_Visc       <<<grid,block>>>(etan_d, nx, ny, nz);           cudaDeviceSynchronize(); // BC
    BC_V_1        <<<grid,block>>>(Vx_d, Vy_d, Vz_d, nx, ny, nz); cudaDeviceSynchronize(); // BC
    BC_V_2        <<<grid,block>>>(Vx_d, Vy_d, Vz_d, nx, ny, nz); cudaDeviceSynchronize(); // BC
    BC_V_3        <<<grid,block>>>(Vx_d, Vy_d, Vz_d, nx, ny, nz); cudaDeviceSynchronize(); // BC
    BC_V_4        <<<grid,block>>>(Vx_d, Vy_d, Vz_d, nx, ny, nz); cudaDeviceSynchronize(); // BC
    BC_V_5        <<<grid,block>>>(Vx_d, Vy_d, Vz_d,txz_d, tyz_d,etan_d, eta_xz_d, eta_yz_d, beta_x_d, beta_y_d, p_dzdx , p_dzdy, p_dz3, p_dz23, nx, ny, nz); cudaDeviceSynchronize(); // BC
    BC_V_6        <<<grid,block>>>(Vx_d, Vy_d, Vz_d, P_d, divV_d, txz_d, tyz_d, etan_d, eta_xz_d, eta_yz_d, p_dz2, p_dz3, p_dzdx, p_dzdy, p_dzeta, nx, ny, nz); cudaDeviceSynchronize(); // BC

    if (iter%100==1 && iter>2){
      __MPI_norm(dVxdt);   err_Vx_NORM = device_NORM;
      __MPI_norm(dVydt);   err_Vy_NORM = device_NORM;
      __MPI_norm(dVzdt);   err_Vz_NORM = device_NORM;
      __MPI_norm(divV);    err_P_NORM  = device_NORM;
      iterror = max(max(err_Vx_NORM,err_Vy_NORM),max(err_Vz_NORM,err_P_NORM));
      if (iterror<eps_stop && iter>1) {break;}
    }
    if (iter%1000==0){ printf(">> iter=%4.d , err=%1.3e \n", iter, iterror);}

 }//end iter
  tim("> Performance", mem*(iter-3)*nIO); // timer test
  printf("> Tot iters=%d \n",iter);
  SaveArray(Vx,"Vx",1); 
  SaveArray(Vy,"Vy",1); 
  SaveArray(Vz,"Vz",1); 
 // clear host memory & clear device memory
 free_all(xc); 
 free_all(xn); 
 free_all(yc); 
 free_all(yn); 
 free_all(zc); 
 free_all(zn);
 free_all(dtP); 
 free_all(dtVx); 
 free_all(dtVy); 
 free_all(dtVz); 
 free_all(etan); 
 free_all(Vx);
 free_all(Vy);
 free_all(Vz);
 free_all(P);
 free_all(divV);
 free_all(Exx);
 free_all(Eyy);
 free_all(Ezz);
 free_all(Exy);
 free_all(Exz);
 free_all(Eyz);
 free_all(txx);
 free_all(tyy);
 free_all(tzz);
 free_all(txy);
 free_all(txz);
 free_all(tyz);
 free_all(Exyn);
 free_all(Exzn);
 free_all(Eyzn);
 free_all(eta_xz);
 free_all(eta_yz);
 free_all(EII2);
 free_all(etait);
 free_all(dVxdt);
 free_all(dVydt);
 free_all(dVzdt);
 free_all(beta_x);
 free_all(beta_y);
 free_all(__device_normval);

 clean_cuda();
 return 0;
}

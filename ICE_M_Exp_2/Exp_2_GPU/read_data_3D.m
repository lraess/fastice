function  A  = read_data_3D(var_name,run_id,it,nx,ny,nz,PRECIS)

    fid  = fopen([int2str(run_id) '.' int2str(it) '.' var_name],'rb');
    A1D  = fread(fid, PRECIS, 'n');  
    fclose(fid);

    for k=1:nz
        for j=1:ny
            for i=1:nx
                ijk=i +  (j-1)*nx +  (k-1)*nx*ny;
                A(i,j,k)=A1D(ijk);
            end
        end
    end

end

clear
run_id = 1;
it     = 1;
PRECIS = 'double';

nx = 511;
ny = 127;

Pr  = read_data('P'  ,run_id,it,nx  ,ny  ,PRECIS);
txx = read_data('txx',run_id,it,nx  ,ny  ,PRECIS);
txy = read_data('txy',run_id,it,nx-1,ny-1,PRECIS);
tyy = read_data('tyy',run_id,it,nx  ,ny  ,PRECIS);
Vx  = read_data('Vx' ,run_id,it,nx+1,ny  ,PRECIS);
Vy  = read_data('Vy' ,run_id,it,nx  ,ny+1,PRECIS);

figure(1),clf,set(gcf,'color','w')
subplot(321),imagesc(Pr'),axis image xy; colorbar,title('Pressure')
subplot(323),imagesc(Vx'),axis image xy; colorbar,title('X-velocity')
subplot(325),imagesc(Vy'),axis image xy; colorbar,title('Y-velocity')
subplot(322),imagesc(txx'),axis image xy; colorbar,title('\tau_{xx}')
subplot(324),imagesc(tyy'),axis image xy; colorbar,title('\tau_{yy}')
subplot(326),imagesc(txy'),axis image xy; colorbar,title('\tau_{xy}')
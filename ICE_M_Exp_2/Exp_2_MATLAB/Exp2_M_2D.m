% FastICE: Thermomechanical ice deformation models 2D
%
% Copyright (C) 2019  Ludovic Raess, Aleksandar Licul et al.
%
% FastICE is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% FastICE is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with FastICE. If not, see <http://www.gnu.org/licenses/>.

clear
tic
% Physics 
Lz      = 1;
npow    = 3;
alpha   = 0.1;
aspect  = 10;
beta_0  = 0.194207089894401;
eta0    = 5e2;
% Numerics
nx     = 127;
nz     = 31;
damp   = 2; 
relV   = 1/1;
relP   = 1/2;
eta_b  = 1/2;
rele   = 1e-1;
niter  = 5e6;
epsi   = 1e-8;  
nout_iter = 1000;
% Pre-processing
Lx          = aspect*Lz;
mpow        = -(1-1/npow)/2;
dx          = Lx/(nx-1); 
dz          = Lz/(nz-1);
xn          = -dx/2:dx:Lx+dx/2; 
zn          = -dz/2:dz:Lz+dz/2;
xc          = 0:dx:Lx;
zc          = 0:dz:Lz;
[xc2,  zc2] = ndgrid(xc,zc);
Vx          = zeros(nx+1,nz);
Vz          = zeros(nx,nz+1);
P           = zeros(nx,nz);
Exzn        = zeros(nx,nz);
etan        = ones(nx,nz);
etas        = zeros(nx-1,nz-1);
dVxdt       = zeros(nx-1,nz-2);
dVzdt       = zeros(nx-2,nz-1); 
beta        = beta_0*(1+sin(2*pi*xn(1,2:end-1)'/Lx));
for iter = 1:niter % Pseudo-Transient cycles
    % Timesteps - GPU KERNEL 1
    etax     =  0.5*(etan(2:end  ,2:end-1) + etan(1:end-1,2:end-1));
    etaz     =  0.5*(etan(2:end-1,2:end  ) + etan(2:end-1,1:end-1));
    dtP      = relP*4.1/max(nx,nz)*etan*(1+eta_b);
    dtVx     = relV*min(dx,dz)^2./etax/(1+eta_b)/4.1;
    dtVz     = relV*min(dx,dz)^2./etaz/(1+eta_b)/4.1;
    % Pressure update and strain rates - GPU KERNEL 2
    divV     = diff(Vx,1,1)/dx + diff(Vz,1,2)/dz;
    P        = P - divV.*dtP;
    Exx      = diff(Vx,1,1)/dx - 1/2*divV;
    Ezz      = diff(Vz,1,2)/dz - 1/2*divV;
    Exz      = 0.5*(diff(Vx(2:end-1,:),1,2)/dz+diff(Vz(:,2:end-1),1,1)/dx);                  
    % Stresses - GPU KERNEL 3
    txx       =  2*etan.*(Exx + eta_b*divV);
    tzz       =  2*etan.*(Ezz + eta_b*divV);
    txz       =  2*etas.*Exz;  
    etas     = 0.25*(etan(1:end-1,1:end-1)+etan(2:end  ,1:end-1)+etan(1:end-1,2:end  ) + etan(2:end  ,2:end  ));
    Exzn(2:end-1,2:end-1) = 0.25*(Exz(1:end-1,1:end-1)+Exz(2:end,1:end-1)+Exz(1:end-1,2:end  )+Exz(2:end,2:end  ));
    % GPU KERNEL 4
    Exzn(:,1) = Exzn(:,2);
    % GPU KERNEL 5
    ResVx    = diff(txz,1,2)/dz - diff(P(:,2:end-1),1,1)/dx + diff(txx(:,2:end-1),1,1)/dx + 1;
    ResVz    = diff(txz,1,1)/dx - diff(P(2:end-1,:),1,2)/dz + diff(tzz(2:end-1,:),1,2)/dz - cotd(alpha);
    dVxdt    = dVxdt.*(1-damp/nx) + ResVx;
    dVzdt    = dVzdt.*(1-damp/nz) + ResVz;   
    EII2     = (Exx.^2 + Ezz.^2)/2 + Exzn.^2;
    etait    = (EII2).^mpow;
    % GPU KERNEL 6
    Vx(2:end-1,2:end-1) = Vx(2:end-1,2:end-1) + dVxdt.*dtVx;
    Vz(2:end-1,2:end-1) = Vz(2:end-1,2:end-1) + dVzdt.*dtVz;
    etan     = min(exp(rele*log(etait) + (1-rele)*log(etan)),eta0);
    %etan     = 1.0./(1.0./(exp(rele*log(etait) + (1-rele)*log(etan))) + 1.0/eta0);
    % GPU KERNEL 7
    etan(:,end) = etan(:,end-1);
    %etan(:,[1 end]) = etan(:,[2 end-1]);
    % GPU KERNEL 8
    %etan([1 end],:) = etan([2 end-1],:);
    % GPU KERNEL 9
    % Boundary conditions
    % BC left
    Vx(1,:)   =  Vx(end-1,:);
    Vz(1,:)   =  0.5*(Vz(end-1,:) + Vz(2,:));
    % BC right
    Vx(end,:) =  Vx(2,:);
    Vz(end,:) =  Vz(1,:);
    % GPU KERNEL 10
    % BC bottom
    Vx(2:end-1,1)   = (Vx(2:end-1,2) + dz/dx*(Vz(2:end,2)-Vz(1:end-1,2))-dz/3*txz(:,2)./etas(:,1))./(2/3*beta*dz./etas(:,1)+1); %Vx(2:end-1,1)*beta = (3*txz(:,1) - txz(:,2))/2
    Vz(:,1)         = -Vz(:,2);
    % BC top             
    Vz(:,end) = Vz(:,end-1) + dz*P(:,end)./etan(:,end)/2 + dz*(1/2-eta_b)*divV(:,end);
    Vx(2:end-1,end) = Vx(2:end-1,end-1) - dz/dx*(Vz(2:end,end-1)-Vz(1:end-1,end-1)) + dz/3*txz(:,end-1)./etas(:,end); 
    %% errors
    norm_X  = sqrt(sum(dVxdt(:).*dVxdt(:)))/(nx*nz);
    norm_Z  = sqrt(sum(dVzdt(:).*dVzdt(:)))/(nx*nz);
    norm_P  = sqrt(sum(divV(:).*divV(:)))/(nx*nz);
    % Monitoring
    if (mod(iter,100)==1) 
    %iterror              = max(norm_X,max(norm_Z,norm_P));
    iterror              = max(norm_X,norm_Z);
    if ((iterror < epsi) && (iter > 2)),break; end
    end
    if (mod(iter,nout_iter)==1) 
        fprintf('iter=%d, err=%1.3e \n',iter,iterror)
        figure(1)
        clf
        colormap jet
        subplot(2,3,1)
        imagesc(flipud(Vx'))
        colorbar
        subplot(2,3,2)
        imagesc(flipud(Vz'))
        colorbar
        subplot(2,3,3)
        imagesc(flipud(etan'))
        colorbar
        subplot(2,3,4)
        imagesc(flipud(log10(abs(dVxdt))'))
        colorbar
        subplot(2,3,5)
        imagesc(flipud(log10(abs(dVzdt))'))
        colorbar
        subplot(2,3,6)
        imagesc(flipud(log10(abs(divV))'))
        colorbar
        drawnow
    end
end
CPU_time=toc;
MTP_eff = (nx*nz)*iter*(2*3+2)*8/1e9/CPU_time;
fprintf('iter=%d, err=%1.3e \n',iter,iterror)
fprintf('time=%f, MTP_eff=%f \n',CPU_time,MTP_eff)

Vx_MAT   = Vx;
Vz_MAT   = Vz;
P_MAT    = P;
txx_MAT  = txx;
tzz_MAT  = tzz;
txz_MAT  = txz;
xc_MAT   = xc;
xn_MAT   = xn;
save('data_MAT_1.mat','xc_MAT','xn_MAT','Vx_MAT','Vz_MAT','P_MAT','txx_MAT','tzz_MAT','txz_MAT')
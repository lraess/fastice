function [] = vizme(iter_plot_count,iterror_M,Vx,Vz,P,txx,tzz,txz,EII2,etan,divV)
     figure(1)
     clf,colormap jet   
     subplot(221),semilogy(iter_plot_count,iterror_M,'k'),axis tight,title('errors vs iterations')
     xlabel(max(max(max(Vx))))
     subplot(222),imagesc(flipud(Vx')),colorbar,title('u')
     subplot(223),imagesc(flipud(Vz')),colorbar,title('v')
     subplot(224),imagesc(flipud(log10(etan'))),colorbar,title('etan')
     drawnow
     figure(2)
     clf,colormap jet   
     subplot(331),imagesc(flipud(etan')),colorbar,title('etan')
     subplot(332),imagesc(flipud(P')),colorbar,title('p')
     subplot(333),imagesc(flipud(txx')),colorbar,title('txx')
     subplot(334),imagesc(flipud(txx'-P')),colorbar,title('Sxx')
     subplot(335),imagesc(flipud(tzz')),colorbar,title('tzz')
     subplot(336),imagesc(flipud(tzz'-P')),colorbar,title('Szz')
     subplot(337),imagesc(flipud(txz')),colorbar,title('txz')
     subplot(338),imagesc(flipud(divV')),colorbar,title('divV')
     subplot(339),imagesc(flipud(log10(EII2'))),colorbar,title('EII2')
     drawnow
end


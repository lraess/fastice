/* FastICE: cuda_scientific support functions 2D GPU

Copyright (C) 2019  Ludovic Raess, Aleksandar Licul et al.
 
FastICE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
FastICE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with FastICE. If not, see <http://www.gnu.org/licenses/>. */

#define  def_sizes(A,nx,ny)  const int sizes_##A[] = {nx,ny};                            
#define  size(A,dim)        (sizes_##A[dim-1])
#define  numel(A)           (size(A,1)*size(A,2))
#define  end(A,dim)         (size(A,dim)-1)
#define  zeros(A,nx,ny)     def_sizes(A,nx,ny);                                            \
                            DAT *A##_d,*A##_h; A##_h = (DAT*)malloc(numel(A)*sizeof(DAT)); \
                            for(i=0; i < (nx)*(ny); i++){ A##_h[i]=(DAT)0.0; }             \
                            cudaMalloc(&A##_d      ,numel(A)*sizeof(DAT));                 \
                            cudaMemcpy( A##_d,A##_h,numel(A)*sizeof(DAT),cudaMemcpyHostToDevice);
#define  ones(A,nx,ny)      def_sizes(A,nx,ny);                                            \
                            DAT *A##_d,*A##_h; A##_h = (DAT*)malloc(numel(A)*sizeof(DAT)); \
                            for(i=0; i < (nx)*(ny); i++){ A##_h[i]=(DAT)1.0; }             \
                            cudaMalloc(&A##_d      ,numel(A)*sizeof(DAT));                 \
                            cudaMemcpy( A##_d,A##_h,numel(A)*sizeof(DAT),cudaMemcpyHostToDevice);
#define  gather(A)          cudaMemcpy( A##_h,A##_d,numel(A)*sizeof(DAT),cudaMemcpyDeviceToHost);
#define  free_all(A)        free(A##_h);cudaFree(A##_d);

#define  select(A,ix,iy)   ( A[ix + iy*size(A,1)] )
#define  participate_a(A)  (ix< size(A,1)         && iy< size(A,2))
#define  participate_i(A)  (ix<(size(A,1)-(int)2) && iy<(size(A,2)-(int)2) )
#define  participate_ix(A) (ix<(size(A,1)-(int)2) && iy< size(A,2))
#define  participate_iy(A) (ix< size(A,1)         && iy<(size(A,2)-(int)2))

#define  all(A)     ( A[ ix    +  iy   *size(A,1) ] )
#define  inn(A)     ( A[ ixi   +  iyi  *size(A,1) ] )
#define  in_x(A)    ( A[ ixi   +  iy   *size(A,1) ] )
#define  in_y(A)    ( A[ ix    +  iyi  *size(A,1) ] )
#define  d_xa(A)    ( A[(ix+1) +  iy   *size(A,1) ] - A[(ix) +  iy *size(A,1)] )
#define  d_ya(A)    ( A[ ix    + (iy+1)*size(A,1) ] - A[ ix  + (iy)*size(A,1)] )
#define  d_xi(A)    ( A[(ix+1) +  iyi  *size(A,1) ] - A[(ix) +  iyi*size(A,1)] )
#define  d_yi(A)    ( A[ ixi   + (iy+1)*size(A,1) ] - A[ ixi + (iy)*size(A,1)] )
#define  av_xya(A) (( A[ ix    +  iy   *size(A,1) ] \
   +                  A[(ix+1) +  iy   *size(A,1) ] \
   +                  A[ ix    + (iy+1)*size(A,1) ] \
   +                  A[(ix+1) + (iy+1)*size(A,1) ] )*(DAT)0.25)
#define av_xyi(A)  (( A[ ix    +  iy   *size(A,1) ] \
   +                  A[(ix+1) +  iy   *size(A,1) ] \
   +                  A[ ix    + (iy+1)*size(A,1) ] \
   +                  A[(ix+1) + (iy+1)*size(A,1) ] )*(DAT)0.25)
#define  av_xi(A)  (( A[ ix    +  iyi  *size(A,1) ] \
   +                  A[(ix+1) +  iyi  *size(A,1) ] )*(DAT)0.5)
#define  av_yi(A)  (( A[ ixi   +  iy   *size(A,1) ] \
   +                  A[ ixi   + (iy+1)*size(A,1) ] )*(DAT)0.5)
void save_array(DAT* A, size_t nb_elems, const char* A_name,int it,int run_id)
{  // The name must be of size 2!
  char* fname = (char*) malloc(50*sizeof(char));
  char* rm    = (char*) malloc(50*sizeof(char));
  FILE* fid;
  sprintf(fname,"%.1d.%.1d.%s",run_id,it,A_name); 
  sprintf(rm   ,"rm -f %s",fname    );
  system(rm); fid=fopen(fname          , "wb"); fwrite(A, PRECIS, nb_elems, fid);    fclose(fid);
}
#define SaveArray(A,A_name,it)  gather(A); save_array(A##_h, numel(A), A_name, it, run_id+1);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CUDA __MPI_max __MPI_min & __MPI_mean subroutine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                        
#define blockId        (blockIdx.x  +  blockIdx.y *gridDim.x)
#define threadId       (threadIdx.x + threadIdx.y*blockDim.x)
#define isBlockMaster  (threadIdx.x==0 && threadIdx.y==0)
// maxval //
#define block_max_init() DAT __thread_maxval;
#define __thread_max(A)   __thread_maxval=0;                                                                        \
                          if (participate_a(A)){ __thread_maxval = max(abs(__thread_maxval),abs(select(A,ix ,iy))); }

__shared__ volatile DAT __block_maxval;
#define __block_max(A)    __thread_max(A);                                                            \
                          if (isBlockMaster){ __block_maxval=0; }                                     \
                          __syncthreads();                                                            \
                          for (int i=0; i < (NB_THREADS); i++){                                       \
                            if (i==threadId){ __block_maxval = max(__block_maxval,__thread_maxval); } \
                            __syncthreads();                                                          \
                          }

__global__ void __device_max_d(DAT*A, const int nx_A,const int ny_A, DAT*__device_maxval){
  // CUDA specific
  def_sizes(A,nx_A,ny_A);
  block_max_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  // find the maxval for each block
  __block_max(A);
  __device_maxval[blockId] = __block_maxval;
}
// minval //
#define block_min_init() DAT __thread_minval;
#define __thread_min(A)   __thread_minval=999999;                                                                   \
                          if (participate_a(A)){ __thread_minval = min(abs(__thread_minval),abs(select(A,ix ,iy))); }

__shared__ volatile DAT __block_minval;
#define __block_min(A)    __thread_min(A);                                                            \
                          if (isBlockMaster){ __block_minval=999999; }                                \
                          __syncthreads();                                                            \
                          for (int i=0; i < (NB_THREADS); i++){                                       \
                            if (i==threadId){ __block_minval = min(__block_minval,__thread_minval); } \
                            __syncthreads();                                                          \
                          }

__global__ void __device_min_d(DAT*A, const int nx_A,const int ny_A, DAT*__device_minval){
  // CUDA specific
  def_sizes(A,nx_A,ny_A);
  block_min_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  // find the minval for each block
  __block_min(A);
  __device_minval[blockId] = __block_minval;
}
// meanval //
#define block_mean_init() DAT __thread_meanval;
#define __thread_mean(A)   __thread_meanval=0.0;                                       \
                           if (participate_a(A)){ __thread_meanval = select(A,ix ,iy); }

__shared__ volatile DAT __block_meanval;
#define __block_mean(A)    __thread_mean(A);                                                                                     \
                           if (isBlockMaster){ __block_meanval=0.0; }                                                            \
                           __syncthreads();                                                                                      \
                           for (int i=0; i < (NB_THREADS); i++){                                                                 \
                             if (i==threadId){ __block_meanval = __block_meanval + ((__thread_meanval) / ((DAT)(NB_THREADS))); } \
                             __syncthreads();                                                                                    \
                           }                                                                                                     \

__global__ void __device_mean_d(DAT*A, const int nx_A,const int ny_A, DAT*__device_meanval){
  // CUDA specific
  def_sizes(A,nx_A,ny_A);
  block_mean_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  // find the meaval for each block
  __block_mean(A);
  __device_meanval[blockId] = __block_meanval;
}
// normL2 //
#define block_norm_init() DAT __thread_normval;
#define __thread_norm(A)   __thread_normval=0.0;                                                        \
                           if (participate_a(A)){ __thread_normval = (select(A,ix,iy)*select(A,ix,iy)); }

__shared__ volatile DAT __block_normval;
#define __block_norm(A)    __thread_norm(A);                                                           \
                           if (isBlockMaster){ __block_normval=0.0; }                                  \
                           __syncthreads();                                                            \
                           for (int i=0; i < (NB_THREADS); i++){                                       \
                             if (i==threadId){ __block_normval = __block_normval + __thread_normval; } \
                             __syncthreads();                                                          \
                           }                                                                           \

__global__ void __device_norm_d(DAT*A, const int nx_A,const int ny_A, DAT*__device_normval){
  // CUDA specific
  def_sizes(A,nx_A,ny_A);
  block_norm_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  // find the normval for each block
  __block_norm(A);
  __device_normval[blockId] = __block_normval;
}
#define __MPI_max(A)  __device_max_d<<<grid, block>>>(A##_d, size(A,1),size(A,2), __device_maxval_d); \
                      gather(__device_maxval); device_MAX=(DAT)0.0;          \
                      for (int i=0; i < (grid.x*grid.y); i++){               \
                         device_MAX = max(device_MAX,__device_maxval_h[i]);  \
                      }                                                      
#define __MPI_min(A)  __device_min_d<<<grid, block>>>(A##_d, size(A,1),size(A,2), __device_minval_d); \
                      gather(__device_minval); device_MIN=(DAT)999999;       \
                      for (int i=0; i < (grid.x*grid.y); i++){               \
                         device_MIN = min(device_MIN,__device_minval_h[i]);  \
                      }                                                      
#define __MPI_mean(A) __device_mean_d<<<grid, block>>>(A##_d, size(A,1),size(A,2), __device_meanval_d); \
                      gather(__device_meanval); device_MEAN=(DAT)0.0;                                   \
                      for (int i=0; i < (grid.x*grid.y); i++){                                          \
                        device_MEAN = device_MEAN + ((__device_meanval_h[i]) / ((DAT)(grid.x*grid.y*grid.z))); \
                      }                                                                                 
#define __MPI_norm(A) __device_norm_d<<<grid, block>>>(A##_d, size(A,1),size(A,2), __device_normval_d); \
                      gather(__device_normval); device_NORM=(DAT)0.0;                                   \
                      for (int i=0; i < (grid.x*grid.y); i++){                                          \
                        device_NORM = device_NORM + __device_normval_h[i];                              \
                      }                                                                                 \
                      device_NORM = sqrt(device_NORM) / (size(A,1)*size(A,2));                          
////////// CUDA subroutines //////////
dim3  grid, block;
int   gpu_id=-1;
void set_up_gpu(){
  block.x = BLOCK_X; block.y = BLOCK_Y;
  grid.x  = GRID_X;   grid.y = GRID_Y;
  gpu_id  = GPU_ID;
  cudaSetDevice(gpu_id); cudaGetDevice(&gpu_id);
  cudaDeviceReset();                                // Reset the device to avoid problems caused by a crash in a previous run (does still not assure proper working in any case after a crash!).
  cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);  // set L1 to prefered
}

void  clean_cuda(){ 
  cudaError_t ce = cudaGetLastError();
  if(ce != cudaSuccess){ printf("ERROR launching GPU C-CUDA program: %s\n", cudaGetErrorString(ce)); cudaDeviceReset();}
}

// Timer // to add for mpi in each tic and toc at the beginning: MPI_Barrier(MPI_COMM_WORLD);
#include "sys/time.h"
double timer_start = 0;
double cpu_sec(){ struct timeval tp; gettimeofday(&tp,NULL); return tp.tv_sec+1e-6*tp.tv_usec; }
void   tic(){ timer_start = cpu_sec(); }
double toc(){ return cpu_sec()-timer_start; }
void   tim(const char *what, double n){ double s=toc(); printf("%s: %8.3f seconds",what,s);if(n>0)printf(", %8.3f GB/s", n/s); printf("\n"); }

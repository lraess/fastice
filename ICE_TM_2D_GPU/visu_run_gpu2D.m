clear

get_res = 0;

Lz   = 300;
T_sc = 28.7536;
T0   = 263;
T_K  = 273;
t_sc = 0.7387;
L_sc = 9.9117e-4;

nx   = 255;
nz   = 63;
nt   = 6000;
DAT  = 'double';

Lz   = Lz/L_sc;
dz   = Lz/(nz-1);
zc   = 0:dz:Lz;

% extract results
if get_res==1    
    nruns = 120;
    for i=1:nruns    
        irun = (i-1)*50 + 0;
        T   = read_data('T',1,irun,nx,nz,DAT);
        tim = read_data('time_vec',1,irun,nt,1,DAT);
        if i==1, Tmat = zeros(nruns,size(T,2)+1); nx = size(T,1); end
        Tmat(i,1:end-1) = T(fix(nx/2),:);
        Tmat(i,end) = tim(irun+1);
    end
    save Tmat Tmat
else
    load Tmat
end
% plot results
Temp   = Tmat(2:end,1:end-1);
T_phys = Temp*T_sc+(T0-T_K);
Time   = Tmat(2:end,end);
s2y    = 3600*24*365.25;

plot_scale = 1.0294;

cm = load('MyCmap');
figure(9),clf,colormap(cm.mymap),set(gcf,'Color','w'),set(gcf,'position',[440   385   560   413])
% imagesc(Time*t_sc/s2y,zc*L_sc,T_phys'),axis xy
imagesc(Time/1e9,zc./Lz,plot_scale*Temp'),axis xy
cb = colorbar;
yl=ylabel(cb,'$\Delta T$','interpreter','latex','Fontsize',18);
pos = get(cb,'Position');
caxis([0 0.35])
% cb.Label.Position = [pos(1)+.5 pos(2)+.5]; % to change its position
cb.Label.Position = [pos(1)+.8 pos(2)+.275]; % to change its position
cb.Label.Rotation = 0; % to rotate the text
% xlabel('time [yr]','interpreter','latex')
% ylabel('depth [m]','interpreter','latex')
xlabel('time $\times ~10^{-9}$','interpreter','latex')
ylabel('$z/\mathrm{L}_z$','interpreter','latex')
set(gca,'Fontsize',18,'Linewidth',1.1)
% print('T_evol.png','-dpng','-r300')
